//
// Created by Administrator on 8/22/2020.
//

#include <iostream>
#include <string>
#include <memory>

#include <gtest/gtest.h>

#include <cpp-common/ProcessWrapper.h>

TEST(TestProcessWrapper, testcase) {
    auto worker_id = 1;
    auto exe_path = std::string(R"(D:\Code\iie\file-check\build-vs\debug\src\worker\Debug\worker.exe)");
    auto worker_process = std::make_shared<ProcessWrapper>(exe_path);
    worker_process->push_param(worker_id);
    worker_process->startup();
}