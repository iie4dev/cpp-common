//
// Created by limo on 8/22/2018.
//

#include "cpp-common/AcAutomaton.h"

#include <utility>
#include <iostream>
#include <algorithm>

#include <cpp-common/logger.h>


const size_t MAX_BUFF_SIZE = 2 * 1024 * 1024;
const size_t kHitContextSize = 1000;
const size_t kHitContextMaxSize = 10000;
const size_t kHitContextMinSize = 100;

#pragma region AcAutomatonCore

AcAutomatonCore::AcAutomatonCore(std::shared_ptr<State> tree): p_root_(std::move(tree)) {
}

void AcAutomatonCore::Builder::set(std::vector<std::string> const& keywords) {
    clear();
    p_root_ = std::make_shared<State>();

    // append all the keywords into cur trie tree
    for (auto const& keyword : keywords) {
        append(np_limo::str::UTF8, keyword);
    }
}

void AcAutomatonCore::Builder::set(std::vector<
    std::pair<np_limo::str::EStringEncoding, std::string>> const& pairs) {
    
    clear();
    p_root_ = std::make_shared<State>();

    // append all the keywords into cur trie tree
    for (auto const& pair : pairs) {
        append(pair.first, pair.second);
    }
}

void AcAutomatonCore::Builder::append(std::string const& keyword) {
    append(np_limo::str::UTF8, keyword, keyword);
}

void AcAutomatonCore::Builder::append(np_limo::str::EStringEncoding const encoding, 
                                      std::string const& keyword) {
    append(encoding, keyword, keyword);
}

void AcAutomatonCore::Builder::append(std::string const& keyword, std::string const& payload) {
    append(np_limo::str::UTF8, keyword, payload);
}

void AcAutomatonCore::Builder::append(np_limo::str::EStringEncoding encoding,
                                      std::string const& keyword,
                                      std::string const& payload) {
    if (nullptr == p_root_)
        p_root_ = std::make_shared<State>();

    // append the keyword into curr trie tree
    auto p_state = p_root_;
    for (auto const c : keyword) {
        if (nullptr == p_state->next[c]) {
            p_state->next[c] = std::make_shared<State>();
        }
        p_state = p_state->next[c];
    }
    p_state->outs.emplace(encoding, payload);
}

void AcAutomatonCore::Builder::clear() {
    p_root_ = nullptr;
}

std::shared_ptr<AcAutomatonCore> AcAutomatonCore::Builder::build() const {
    if (nullptr == p_root_) return nullptr;

    // make the root fall into itself
    p_root_->p_fail = p_root_;

    // note that all the states in queue have the correct fail pointer
    std::queue<std::shared_ptr<State>> que;
    // all states of depth 1 have failure pointing to root
    for (auto& chr_state : p_root_->next) {
        chr_state.second->p_fail = p_root_;
        que.push(chr_state.second);
    }

    // update the states which have no fail pointer in bfs order
    while (!que.empty()) {
        auto p_curr_state = que.front();
        que.pop();
        for (auto& chr_state : p_curr_state->next) {
            auto const& chr = chr_state.first;
            auto const& p_next_state = chr_state.second;

            // find the first valid failure State following the failure chain
            auto p_fail = p_curr_state->p_fail;
            while (nullptr == p_fail->next[chr] && p_fail != p_root_)
                p_fail = p_fail->p_fail;

            // get the failure pointer of next State
            if (nullptr != p_fail->next[chr])
                p_fail = p_fail->next[chr];
            p_next_state->p_fail = p_fail;

            // merge out keywords
            p_next_state->outs.insert(p_fail->outs.begin(), p_fail->outs.end());

            que.push(p_next_state);
        }
    }

    return std::shared_ptr<AcAutomatonCore>(new AcAutomatonCore(p_root_));
}

#pragma endregion 

#pragma region AcAutomaton

AcAutomaton::AcAutomaton(std::shared_ptr<AcAutomatonCore> tree) 
    : p_core_(std::move(tree))
    , buff_(MAX_BUFF_SIZE, '\0')
    , content_offset_(0)
	, search_limit_(INT_MAX)
	, hit_limit_(INT_MAX)
	, hit_count_(0)
	, context_(kHitContextSize)
    // , hit_context_(kHitContextSize, '\0')
    // , hit_context_curr_(0)
    // , hit_context_full_(false)
    , is_searching_(false) {
}

void AcAutomaton::ignore_chars(std::string const& chars) {
    ignore_chars_.clear();
    for (auto const c : chars)
        ignore_chars_.insert(c);
}

void AcAutomaton::set_context_size(size_t size) {
    size = std::min(kHitContextMaxSize, size);
    size = std::max(kHitContextMinSize, size);
    context_.resize(size);
    // hit_context_.resize(size);
}

void AcAutomaton::set_hit_limit(size_t const hit_limit)
{
    hit_limit_ = hit_limit ? hit_limit : INT_MAX;
}

void AcAutomaton::set_search_limit(size_t const search_limit)
{
    search_limit_ = search_limit ? search_limit : INT_MAX;
}

void AcAutomaton::set_core(std::shared_ptr<AcAutomatonCore> core) {
    p_core_ = std::move(core);
}

/**
 * @brief
 * @param text_in   text extractor used to extract text block
 * @param callback  this will be called if there is any match
 */
bool AcAutomaton::search_in(text_in_callback_t const& text_in, match_callback_t const& callback) {
    if (!before_search(callback)) return false;
    buff_.resize(MAX_BUFF_SIZE);
    while (text_in(buff_)) {
        if (!search_in_buff(buff_.c_str(), buff_.size()))
            break;
        buff_.resize(MAX_BUFF_SIZE);
    }
    after_search();
    return true;
}

/**
 * Search in buff content if there is a match, notify caller by callback
 *
 * @param content   pointing to the content which is going to be searched
 * @param size      size of buff <content>
 * @param callback  this will be called if there is any match
 */
bool AcAutomaton::search_in(char const* content, size_t const size, match_callback_t const& callback) {
    if (!before_search(callback)) return false;
    search_in_buff(content, size);
    after_search();
    return true;
}

/**
 * Search in string content if there is a match, notify caller by callback
 *
 * @param content   pointing to the content which is going to be searched
 * @param callback  this will be called if there is any match
 */
bool AcAutomaton::search_in(std::string const& content, match_callback_t const& callback) {
    return search_in(content.c_str(), content.size(), callback);
}

bool AcAutomaton::search_in(HANDLE h_content, match_callback_t const& callback) {
    auto const text_in = [&h_content](std::string& buff) {
        DWORD read_bytes;
        ReadFile(h_content, &buff[0], buff.size(), &read_bytes, nullptr);
        buff.resize(read_bytes);
        return buff.size();
    };

    return search_in(text_in, callback);
}

/**
 * Search in stream content if there is a match, notify caller by callback
 *
 * @param content   pointing to the content which is going to be searched
 * @param callback  this will be called if there is any match
 */
bool AcAutomaton::search_in(std::istream& content, match_callback_t const& callback) {
    auto const text_in = [&content](std::string& buff) {
        content.read(&buff[0], buff.size());
        buff.resize(static_cast<size_t>(content.gcount()));
        return buff.size();
    };

    return search_in(text_in, callback);
}

bool AcAutomaton::before_search(match_callback_t const& callback) {
    if (!is_ready()) return false;
    content_offset_ = 0;
    hit_count_ = 0;
    hit_callback_ = callback;
    while (!hits_cache_.empty()) hits_cache_.pop();
    context_.reset();
    is_searching_ = true;
    return true;
}

bool AcAutomaton::pop_hit(size_t const last_hit_offset) {
    auto const hit_context = context_.to_string();
    if (!hit_callback_(last_hit_offset, hit_context, hits_cache_.front().second->outs)) {
        // has been break
        return false;
    }
    hits_cache_.pop();
    return true;
}

/**
* Search pattern in buff by using ac-automaton algorithm
*
* @param content         pointing to the content which going to be searched
* @param size            size of content
*/
bool AcAutomaton::search_in_buff(char const* content, size_t const size) {
    auto const half_context_size = context_.size() >> 1u;

    auto p_state = p_core_->p_root_;
    for (size_t offset = 0; offset < size; ++offset) {
        auto const global_offset = content_offset_ + offset;

        auto const chr = content[offset];
        context_.push(chr);
    	
        if (ignore_chars_.count(chr)) continue;

        // pop out the hit that has collect enough context
        while (!hits_cache_.empty() && hits_cache_.front().first + half_context_size <= global_offset) {
            if (!pop_hit(hits_cache_.front().first)) // early break
                return false;
        }

        // meet the limitation, there is no need to scan the remain stream
        if (global_offset >= search_limit_ || hit_count_ >= hit_limit_) {
        	if (hits_cache_.empty())
				return true;
            continue;
        }

        // find the next State
        while (nullptr == p_state->next[chr] && p_state != p_core_->p_root_)
            p_state = p_state->p_fail;

        if (nullptr != p_state->next[chr])
            p_state = p_state->next[chr];

        if (p_state->outs.empty())
            continue;

        // push 
        hits_cache_.emplace(global_offset, p_state);
        ++hit_count_;
    }

    content_offset_ += size;
    return true;
}

void AcAutomaton::after_search() {
    // pop out all the hit context
    while (!hits_cache_.empty()) {
        if (!pop_hit(hits_cache_.front().first)) // has been break;
            return;
    }
    is_searching_ = false;

    np_limo::log::trace(kGlobalLogger, "Have scan text with size: {}", content_offset_);
}

bool AcAutomaton::is_ready() const {
    return !is_searching_ && nullptr != p_core_;
}

#pragma endregion 
