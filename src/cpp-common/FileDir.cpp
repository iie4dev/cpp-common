#include "cpp-common/FileDir.h"

#include <Windows.h>
#include <ShlObj.h>

#include <vector>
#include <functional>
#include <iostream>

#include <cpp-common/FileName.h>
#include <cpp-common/FileFind.h>
#include <cpp-common/logger.h>

#pragma comment( lib, "shell32.lib")

namespace np_limo
{
    namespace file
    {
        namespace dir
        {
            TempFile::TempFile(bool const auto_remove_file, string_t prefix,
                               string_t suffix): auto_remove_file_(auto_remove_file)
                                                  , prefix_(std::move(prefix))
                                                  , suffix_(std::move(suffix)) {
            }

            TempFile::~TempFile() {
                if (auto_remove_file_) {
                    try {
                        RemoveFile(temp_file_.c_str());
                    } catch (...) {
                    }
                }
            }

            string_t TempFile::create_temp_file() {
                string_t temp_folder;
                if (dir::GetTempFolder(temp_folder)) {
                    return create_temp_file(temp_folder.c_str());
                }
                return TEXT("");
            }

            string_t TempFile::create_temp_file(CFSTR const temp_folder) {
                char_t temp_file[MAX_PATH + 2];
                GetTempFileName(temp_folder, prefix_.c_str(), 0, temp_file);
                auto named_temp_file = string_t(temp_file);
                if (!suffix_.empty()) {
                    if (suffix_[0] != TEXT('.'))
                        named_temp_file.push_back(TEXT('.'));
                    named_temp_file.append(suffix_);
                }

                MoveFile(temp_file, named_temp_file.c_str());
                return temp_file_ = named_temp_file;
            }

            string_t const& TempFile::get_temp_file() const {
                return temp_file_;
            }

            bool GetHomeDir(string_t& dir) {
                char_t home_path[1024] = { 0 };

                auto const path_size = GetEnvironmentVariable(TEXT("USERPROFILE"), home_path, 1024);
                if (path_size <= 0 || path_size > 1024) {
                    return false;
                }

                dir = string_t(home_path);
                return true;
            }

            bool GetExeDir(string_t& dir) {
                char_t currExeFullPath[MAX_PATH + 2] = { 0 };
                auto const len = ::GetModuleFileName(nullptr, currExeFullPath, MAX_PATH + 1);
                if (len <= 0 || len > MAX_PATH) {
                    return false;
                }

                auto const lastSeparIndex = string_t(currExeFullPath).find_last_of(PATH_SEPARATOR);
                dir.assign(currExeFullPath, lastSeparIndex + 1);  // dir ends with path separator

                if (dir.empty())
                    dir = string_t(TEXT("." STR_PATH_SEPARATOR));
                return true;
            }

            bool GetWindowsDir(string_t& dir) {
                char_t exePath[MAX_PATH + 2] = { 0 };
                auto const len = ::GetWindowsDirectory(exePath, MAX_PATH + 1);
                if (len <= 0 || len > MAX_PATH) {
                    return false;
                }
                dir.assign(exePath, len);
                return true;
            }

            bool GetSystemDir(string_t& dir) {
                char_t exePath[MAX_PATH + 2] = { 0 };
                auto const len = ::GetSystemDirectory(exePath, MAX_PATH + 1);
                if (len <= 0 || len > MAX_PATH) {
                    return false;
                }
                dir.assign(exePath, len);
                return true;
            }

            bool TryUsePath(CFSTR const path, std::function<bool(CFSTR)> const& handlePath) {
                auto const useType = name::CheckSuperPathType(path);
                if (name::kUseOnlySuper != useType) {
                    // try main style first if we can
                    if (handlePath(path))
                        return true;
                }
                if (name::kUseOnlyMain != useType) {
                    // try super style if in need
                    string_t superPath;
                    if (name::SuperPath(superPath, path)) {
                        return superPath.empty() ? handlePath(path) :
                            handlePath(superPath.c_str());
                    }
                }
                return false;
            }

            bool CreateTempFolder(string_t& folder, CFSTR const prefix) {
                string_t temp_path;
                GetTempFolder(temp_path);
                temp_path.append(prefix);

                auto const d = (GetTickCount() << 12u) ^ (GetCurrentThreadId() << 14u) ^ GetCurrentProcessId();
                for (auto i = 0; i < 100; ++i) {  // try 100 times
                    auto random_val = d;
                    string_t random_str;
                    for (auto j = 0; j < 8; ++j) {
                        auto const c = random_val & 0xFu;
                        random_val >>= 4u;
                        random_str.push_back(static_cast<char>(c < 10 ? ('0' + c) : ('A' + (c - 10))));
                    }

                    folder.assign(temp_path).append(random_str);
                    if (!find::DoesFileOrDirExist(folder.c_str())) {
                        if (CreateDir(folder.c_str())) {
                            return true;
                        }
                    }
                }
                return false;
            }

            bool RemoveFile(CFSTR const path) {
                /* If alt stream, we also need to clear READ-ONLY attribute of main file before delete.
                 * SetFileAttrib("name:stream", ) changes attributes of main file.
                 */
                {
                    auto const attr = ::GetFileAttributes(path);
                    if (attr != INVALID_FILE_ATTRIBUTES
                        && (attr & FILE_ATTRIBUTE_DIRECTORY) == 0
                        && (attr & FILE_ATTRIBUTE_READONLY) != 0) {
                        if (!find::SetFileAttrib(path, attr & ~FILE_ATTRIBUTE_READONLY))
                            return false;
                    }
                }

                /* DeleteFile("name::$DATA") deletes all alt streams (same as delete DeleteFile("name")).
                   Maybe it's better to open "name::$DATA" and clear data for unnamed stream? */
                auto ret = false;
                TryUsePath(path, [&ret](CFSTR const path)->bool {
                    return ret = (TRUE == ::DeleteFile(path));
                });
                return ret;
            }

            bool CreateFileIfNotExist(CFSTR const path) {
                auto const h_file = ::CreateFile(path,
                                                 GENERIC_WRITE,
                                                 FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
                                                 nullptr,
                                                 CREATE_NEW,
                                                 FILE_ATTRIBUTE_NORMAL,
                                                 nullptr);
                if (INVALID_HANDLE_VALUE != h_file) {
                    ::CloseHandle(h_file);
                    return true;
                }
                return false;
            }

            /**
             * Create dir recurrently. return true if dir exists after this call
             */
            bool CreateDirRecurrently(CFSTR const path) {
                string_t dir = path;
                name::TrimEndSeparator(dir);

                std::vector<string_t> folder_stack = {dir};
                while (true) {
                    find::CFileInfo fi;
                    if (fi.Find(folder_stack.back().c_str())) {
                        if (!fi.IsDir())
                            return false;
                        folder_stack.pop_back();
                        break;
                    }

                    string_t folder;
                    name::SplitFolderFromPath(folder, folder_stack.back().c_str());
                    name::TrimEndSeparator(folder);

                    if (folder.empty())
                        return false;

                    folder_stack.push_back(folder);
                }

                while (!folder_stack.empty()) {
                    if (!CreateDir(folder_stack.back().c_str()))
                        return false;
//                    std::cout << "Created: " << folder_stack.back() << std::endl;
                    folder_stack.pop_back();
                }
                return true;
            }

            /**
             * Remove dir if the dir is empty recurrently. Always return true
             */
            bool RemoveDirRecurrently(CFSTR const path) {
                string_t folder = path;
                name::TrimEndSeparator(folder);

                while (true) {
                    if (!RemoveDir(folder.c_str()))
                        break;

                    name::SplitFolderFromPath(folder, folder.c_str());
                    name::TrimEndSeparator(folder);
                }
                return true;
            }

            /**
             * Remove dir and all the items of it
             */
            bool RemoveDirWithSubItems(CFSTR const name) {
                return RemoveFilesUnderDir(name) && RemoveDir(name);
            }

            /**
             * Remove file. Additionally, if the folder becomes empty, remove it
             */
            bool RemoveFileWithDirIfEmpty(CFSTR const path) {
                if (!RemoveFile(path)) return false;

                string_t folder = path;
                name::SplitFolderFromPath(folder, folder.c_str());
                name::TrimEndSeparator(folder);

                return RemoveDirRecurrently(folder.c_str());
            }

            bool RemoveFilesUnderDir(CFSTR path) {
                auto need_remove_sub_items = true;

                {
                    find::CFileInfo fi;
                    if (!fi.Find(path) || !fi.IsDir())
                        return false;

                    if (fi.HasReParsePoint())
                        need_remove_sub_items = false;
                }

                auto ret = true;
                if (need_remove_sub_items) {
                    string_t curr_path(path);
                    name::NormalizeDirPathPrefix(curr_path);

                    find::CEnumerator enumerator;
                    enumerator.SetDirPrefix(curr_path);

                    find::CFileInfo fi;
                    while (enumerator.Next(fi)) {
                        auto const full_file_path = curr_path + fi.Name;
                        if (fi.IsDir()) {
                            if (!RemoveDirWithSubItems(full_file_path.c_str()))
                                ret = false;
                        } else if (!RemoveFile(full_file_path.c_str()))
                            ret = false;
                    }
                }

                return ret && find::SetFileAttrib(path, 0);
            }

            bool CreateFileWithDirIfNotExist(CFSTR const path) {
                string_t folder;
                name::SplitFolderFromPath(folder, path);
                name::TrimEndSeparator(folder);

                if (CreateDirRecurrently(folder.c_str())) {
                    return CreateFileIfNotExist(path);
                }
                return false;
            }

            bool DoesDirEmpty(CFSTR const path) {
                find::CEnumerator enumerator;
                enumerator.SetDirPrefix(path);

                // dir is empty if has no items
                find::CFileInfo fi;
                return !enumerator.Next(fi);
            }

            bool TravelFilesUnderDir(string_t const &dir,
                                     std::function<bool(string_t const&)> const &cb,
                                     std::function<bool(string_t const&)> const &filter) {
//                std::cout << "enter travel files under dir: " << dir << std::endl;
                if (!filter(dir)) return false;

                auto path = dir;
                name::NormalizeDirPathPrefix(path);

                find::CEnumerator enumerator;
                enumerator.SetDirPrefix(path);

                find::CFileInfo fi;
                while (enumerator.Next(fi)) {
//                    log::trace(kGlobalLogger, "Find next: {}", fi.Name);
                    if (!fi.Name.empty()) {
                        auto const full_path = path + fi.Name;
                        if (fi.IsDir()) {
                            if (!TravelFilesUnderDir(full_path, cb, filter))
                                return false;
                        } else if (!cb(full_path))
                            return false;
                    }
                }
//                log::trace(kGlobalLogger, "exit travel files under dir: {}", fi.Name);
                return true;
            }

            bool TravelFilesUnderDir(string_t const &dir, std::function<bool(string_t const&)> const& cb) {
                auto path = dir;
                name::NormalizeDirPathPrefix(path);

                find::CEnumerator enumerator;
                enumerator.SetDirPrefix(path);

                find::CFileInfo fi;
                while (enumerator.Next(fi)) {
                    auto full_path = path + fi.Name;
                    if (fi.IsDir()) {
                        full_path.push_back(PATH_SEPARATOR);
                        if (!TravelFilesUnderDir(full_path, cb))
                            return false;
                    } else if (!cb(full_path)) return false;
                }
                return true;
            }

            bool GetTempFolder(string_t& path) {
                char_t s[MAX_PATH + 2] = {0};
                auto const need_length = ::GetTempPath(MAX_PATH + 1, s);
                path = s;
                return (need_length > 0 && need_length <= MAX_PATH);
            }

            bool CreateRandomFolder(string_t& folder, string_t const& parent) {
                if (CreateDirRecurrently(parent.c_str())) {
                    auto const d = (GetTickCount() << 12u) ^ (GetCurrentThreadId() << 14u) ^ GetCurrentProcessId();
                    for (auto i = 0; i < 100; ++i) {  // try 100 times
                        auto random_val = d;
                        string_t random_str;
                        for (auto j = 0; j < 8; ++j) {
                            auto const c = random_val & 0xFu;
                            random_val >>= 4u;
                            random_str.push_back(static_cast<char>(c < 10 ? ('0' + c) : ('A' + (c - 10))));
                        }

                        folder.assign(parent).append(STR_PATH_SEPARATOR).append(random_str);
                        if (!find::DoesFileOrDirExist(folder.c_str())) {
                            if (CreateDir(folder.c_str())) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }

            bool RemoveDir(CFSTR const path) {
                return TryUsePath(path, [](CFSTR const tryPath) { return TRUE == ::RemoveDirectory(tryPath); });
            }

            /**
             * Create dir if the folder of given path exists. Return true
             * if the dir exists after this call
             */
            bool CreateDir(CFSTR const path) {
                find::CFileInfo fi;
                if (fi.Find(path))
                    // if the path exists, return true if it is a dir
                    return fi.IsDir();

                return TryUsePath(path, [](CFSTR const tryPath) {
                    return TRUE == ::CreateDirectory(tryPath, nullptr);
                });
            }

            bool GetCurrentDir(string_t& dir) {
                dir.clear();
                char_t path[MAX_PATH + 2];
                auto const len = ::GetCurrentDirectory(MAX_PATH + 1, path);
                dir.assign(path);
                return len > 0 && len <= MAX_PATH;
            }

            bool SetCurrentDir(CFSTR const dir) {
                return TRUE == ::SetCurrentDirectory(dir);
            }

            bool GetLocalAppDataPath(string_t& path) {
                char_t app_data_path[MAX_PATH];
                char_t short_app_data_path[MAX_PATH] = { 0 };
                memset(app_data_path, 0, _MAX_PATH);

                LPITEMIDLIST id_list = nullptr;
                SHGetSpecialFolderLocation(nullptr, CSIDL_LOCAL_APPDATA, &id_list);
                if (id_list && SHGetPathFromIDList(id_list, short_app_data_path)) {
                    GetShortPathName(short_app_data_path, app_data_path, _MAX_PATH);
                    path.assign(short_app_data_path);
                    return true;
                }
                return false;
            }
        }
    }
}
