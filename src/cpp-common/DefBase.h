#pragma once

#define CODE_BLOCK_NO_IMPLEMENT throw std::runtime_error("No Implementation");

#pragma region enum helper macros for convertiong enum to string
/*
 * These macros will be used to create enum class that has a full list of the
 * enum value and string name and even a map between value and name. For example, 
 * suppose that we have a enum named color with values of black, white, blue and red.
 * Then you need do like below:
 * 
 * ~~~ c++ Color.h
 *  ENUM_DEFINE(Color, black, white, blue, red)
 *  ENUM_DECLARE_VARS(Color)
 * ~~~
 * 
 * ~~~ c++ Color.cpp
 * ENUM_MAP(Color, "black", "white", "blue", "red")
 * ~~~
 * 
 * Code above is equal to:
 * 
 * ~~~ c++ Color.h
 * enum class EColor { black, white, blue, red };
 * extern std::vector<EColor> gAllColor;
 * extern std::vector<const char*> gAllColorName;
 * extern size_t gColorSize;
 * extern std::map<EColor, const char *> gColorNameMap;
 * ~~~
 * 
 * ~~~ c++ Color.cpp
 * std::vector<EColor> gAllColor = { black, white, blue, red };
 * std::vector<const char*> gAllColorName = { "black", "white", "blue", "red" };
 * extern size_t gColorSize = gAllColor.size();
 * std::map<EColor, const char *> gColorNameMap = []() {
 *     std::map<EColor, const char *> typeNameMap; \
 *     for (int i = 0, len = sizeof(gAllColor) / sizeof(EColor); i < len; ++i) { \
 *         typeNameMap[gAllColor[i]] = gAllColorName[i]; \
 *     }
 *     return typeNameMap;
 * }();
 * ~~~
 */
#define ENUM_DEFINE(TypeName, ...) enum E##TypeName { \
    __VA_ARGS__ \
}; \
ENUM_INIT_VAL_LIST_FUNC(TypeName, __VA_ARGS__) \

#define ENUM_MAP(TypeName, ...) \
    ENUM_INIT_NAME_LIST_FUNC(TypeName, __VA_ARGS__) \
    ENUM_INIT_MAP_FUNC(TypeName) \
    ENUM_DEFINE_VARS(TypeName)

#define ENUM_DEF_LIST(TypeName, ...) ENUM_DEFINE(TypeName, __VA_ARGS__) \
    ENUM_LIST(TypeName, __VA_ARGS__)

#define ENUM_DECLARE_VARS(TypeName) \
    extern const std::vector<E##TypeName> gAll##TypeName; \
    extern const std::vector<CFSTR> gAll##TypeName##Name; \
    extern const size_t g##TypeName##Size; \
    extern const std::unordered_map<E##TypeName, CFSTR> g##TypeName##NameMap;

#define ENUM_DEFINE_VARS(TypeName) \
    std::vector<E##TypeName> const gAll##TypeName = InitAll##TypeName##List(); \
    std::vector<CFSTR> const gAll##TypeName##Name = InitAll##TypeName##Name(); \
    size_t const g##TypeName##Size = gAll##TypeName.size(); \
    std::unordered_map<E##TypeName, CFSTR> const g##TypeName##NameMap = Init##TypeName##NameMap();

#define ENUM_INIT_VAL_LIST_FUNC(TypeName, ...) inline std::vector<E##TypeName> InitAll##TypeName##List() { \
    return std::vector<E##TypeName> { \
        __VA_ARGS__ \
    }; \
}

#define ENUM_INIT_NAME_LIST_FUNC(TypeName, ...) std::vector<CFSTR> InitAll##TypeName##Name() { \
    return std::vector<CFSTR> { \
        __VA_ARGS__ \
    }; \
}

#define ENUM_INIT_MAP_FUNC(TypeName, ...) std::unordered_map<E##TypeName, CFSTR> Init##TypeName##NameMap() { \
    std::unordered_map<E##TypeName, CFSTR> typeNameMap; \
    for (size_t i = 0; i < gAll##TypeName.size(); ++i) { \
        typeNameMap[gAll##TypeName[i]] = gAll##TypeName##Name[i]; \
    } \
    return typeNameMap; \
};

#pragma endregion 

 // return if not ok
#define RET_IF_NOT_OK(exp) { HRESULT __result__ = (exp); if (S_OK != __result__) return __result__; }

template <class T>
int CompareVal(T const &a, T const &b) {
    return a == b ? 0 : (a < b ? -1 : 1);
}

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

#define CLEAR_HANDLE(handle) ::CloseHandle(handle); handle = nullptr;
#define IS_VALID_HANDLE(handle) (nullptr != handle && INVALID_HANDLE_VALUE != handle)

#define CP_GBK 936

#define DELETE_COPY_CONSTRUCT_AND_OPERATOR_FUNCTIONS(CLS) \
CLS(CLS const& rhv) = delete; \
CLS& operator=(CLS const& rhv) = delete;

#define DELETE_MOVE_CONSTRUCT_AND_OPERATOR_FUNCTIONS(CLS) \
CLS(CLS && rhv) = delete; \
CLS& operator=(CLS && rhv) = delete;

#define UNCOPYABLE_UNMOVEABLE(CLS) \
DELETE_COPY_CONSTRUCT_AND_OPERATOR_FUNCTIONS(CLS) \
DELETE_MOVE_CONSTRUCT_AND_OPERATOR_FUNCTIONS(CLS)