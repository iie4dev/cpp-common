//
// Created by limo on 8/19/2018.
//

#include "ProcessWrapper.h"

#include <utility>
#include <Windows.h>

ProcessWrapper::ProcessWrapper(string_t process_name) :
        process_name_(std::move(process_name)),
        status_(STATIC),
        break_away_job_(false) {
}

ProcessWrapper::~ProcessWrapper()
{
    shutdown();
}

ProcessWrapper::ProcessWrapper(ProcessWrapper&& rhv)
{
    process_name_ = std::move(rhv.process_name_);
    params_ = std::move(rhv.params_);
    status_ = rhv.status_;
}

ProcessWrapper& ProcessWrapper::operator=(ProcessWrapper&& rhv)
{
    process_name_ = std::move(rhv.process_name_);
    params_ = std::move(rhv.params_);
    status_ = rhv.status_;
    return *this;
}

void ProcessWrapper::push_path(string_t const& path)
{
    params_.push_back(TEXT(R"(")") + path + TEXT(R"(")"));
}

void ProcessWrapper::push_param(string_t const& param)
{
    params_.push_back(param);
}

void ProcessWrapper::push_param(const char_t* param)
{
    params_.emplace_back(param);
}

string_t ProcessWrapper::get_process_name() const {
    return process_name_;
}

size_t ProcessWrapper::get_param_num() const {
    return params_.size();
}

std::vector<string_t> ProcessWrapper::get_params() const {
    return params_;
}

void ProcessWrapper::startup() {
    startup(false, false, false, nullptr, nullptr, nullptr);
}

void ProcessWrapper::startup(HANDLE h_read, HANDLE h_write, HANDLE h_error) {
    startup(false, false, true, h_read, h_write, h_error);
}

void ProcessWrapper::startup_new_console(bool const show_window) {
    startup(true, show_window, false, nullptr, nullptr, nullptr);
}

void ProcessWrapper::startup_new_console(HANDLE h_read_pipe, HANDLE h_write_pipe, HANDLE h_error) {
    startup(true, false, true, h_read_pipe, h_write_pipe, h_error);
}

void ProcessWrapper::startup(bool const create_new_console, bool const show_window,
                             bool const redirect_io, HANDLE h_read_pipe, HANDLE h_write_pipe, HANDLE h_error) {
    if (STATIC != status_) {
        shutdown();
    }

    si_.cb = sizeof(si_);
    if (create_new_console) {
        si_.dwFlags |= STARTF_USESHOWWINDOW;
        si_.wShowWindow = show_window ? SW_SHOWNORMAL : SW_HIDE;
    }

    if (redirect_io) {
        si_.dwFlags |= STARTF_USESTDHANDLES;
        si_.hStdInput = h_read_pipe;
        si_.hStdOutput = h_write_pipe;
        si_.hStdError = h_error;
    }

    auto cmdline = string_t(TEXT(R"(")") + process_name_ + TEXT(R"(")"));
    for (auto const& param : params_) {
        cmdline += TEXT(" ");
        cmdline += param;
    }

    auto dw_creation_flags = 0;
    if (create_new_console)
        dw_creation_flags |= CREATE_NEW_CONSOLE;
    if (break_away_job_)
        dw_creation_flags |= CREATE_BREAKAWAY_FROM_JOB;

    auto const ret = ::CreateProcess(
            nullptr,
            const_cast<str_t>(cmdline.c_str()),
            nullptr,
            nullptr,
            redirect_io ? TRUE : FALSE,
            dw_creation_flags,
            nullptr,
            nullptr,
            &si_,
            &pi_);

    if (ret) {  // if succeed to create the worker process
        status_ = RUNNING;
    }
    else {
        status_ = CRASHED;
    }
}

void ProcessWrapper::shutdown() {
#ifdef WIN32
    if (INVALID_HANDLE_VALUE != pi_.hProcess)
        TerminateProcess(pi_.hProcess, 0);

    CLEAR_HANDLE(pi_.hThread);
    CLEAR_HANDLE(pi_.hProcess);

    memset(&pi_, 0, sizeof(pi_));
    memset(&si_, 0, sizeof(si_));

    status_ = STATIC;
#else
#endif
}

bool ProcessWrapper::running() const {
    return RUNNING == status_;
}

bool ProcessWrapper::wait(DWORD const timeout) const {
    if (running()) {
        return WAIT_OBJECT_0 == ::WaitForSingleObject(pi_.hProcess, timeout);
    }
    return true;
}

bool ProcessWrapper::bind_job(HANDLE h_job) const {
    return TRUE == AssignProcessToJobObject(h_job, pi_.hProcess);
}
