//
// Created by limo on 8/18/2018.
//

#ifndef FILE_CHECK_MQ_MESSAGE_T_H
#define FILE_CHECK_MQ_MESSAGE_T_H

#include <string>

#include <zmq.hpp>

struct mq_message_t {
    enum action_e {
        // 'b': begin
        //      message type: b|task_type|.|.
        // 'e': end
        //      message type: e|task_type|.|.
        // 'c': clear
        //      message type: c|task_type|.|.
        // 'a': add
        //      message type: a|task_type|task_id|task_content
        // 'r': remove
        //      message type: d|task_type|task_id|.
        NOOP,
        BEGIN,
        END,
        CLEAR,
        ADD,
        ADD_LOW,
        REMOVE,

        REQ_TASK,
        REP_TASK,
        REP_NONE,
        REP_CONFIG,

        HEARTBEAT,
        STATUS,
        SINK,

        END_MONITOR,

        // TODO: add for result database refactoring!!!
        // NEED to update Client!!!!!
        CHK_TASK,    // for worker -> queue to check if there is a 
                       // result for the query task
    };

    mq_message_t() {
        data_parts_.emplace_back(sizeof(action_e), '\0');  // action
        data_parts_.emplace_back(sizeof(int), '\0');       // type
        data_parts_.emplace_back(sizeof(int), '\0');       // id
        data_parts_.emplace_back();                        // content
    }

    explicit mq_message_t(zmq::socket_t& socket) {
        recv(socket);
    }

    mq_message_t(mq_message_t const& rhv) {
        *this = rhv;
    }

    mq_message_t& operator=(mq_message_t const& rhv) = default;

    mq_message_t(mq_message_t&& rhv) {
        data_parts_ = std::move(rhv.data_parts_);
    }

    mq_message_t& operator=(mq_message_t&& rhv) {
        data_parts_ = std::move(rhv.data_parts_);
        return *this;
    }

    virtual ~mq_message_t() {
    }

    void clear() {
        set_action(NOOP);
        set_type(0);
        set_id(0);
        content().clear();
    }

    bool recv(zmq::socket_t& socket) {
        data_parts_.clear();

        while (true) {
            zmq::message_t buff;
            try {
                socket.recv(&buff);
            } catch (zmq::error_t &) {
                return false;
            }

            data_parts_.emplace_back(static_cast<char*>(buff.data()), buff.size());
            if (!buff.more()) {
                break;
            }
        }

        return true;
    }

    bool recv(zmq::socket_t& socket, size_t time_out) {
        zmq::pollitem_t item = { socket, 0, ZMQ_POLLIN, 0 };
        data_parts_.clear();

        while (true) {
            zmq::poll(&item, 1, time_out);
            if (ZMQ_POLLIN != item.revents) {
                return false;
            }

            zmq::message_t buff;
            try {
                socket.recv(&buff);
            } catch (zmq::error_t &) {
                return false;
            }

            data_parts_.emplace_back(static_cast<char*>(buff.data()), buff.size());
            if (!buff.more()) {
                break;
            }
        }
        return true;
    }

    bool send(zmq::socket_t& socket) {
        auto last_flag = data_parts_.size();
        for (auto const& part : data_parts_) {
            try {
                socket.send(part.c_str(), part.size(), --last_flag == 0 ? 0 : ZMQ_SNDMORE);
            } catch (zmq::error_t&) {
                return false;
            }
        }
        return true;
    }

    size_t parts_num() const {
        return data_parts_.size();
    }

    action_e action() const {
        return *reinterpret_cast<action_e*>(const_cast<char*>(data_parts_[0].c_str()));
    }

    int type() const {
        return *reinterpret_cast<int*>(const_cast<char*>(data_parts_[1].c_str()));
    }

    int id() const {
        return *reinterpret_cast<int*>(const_cast<char*>(data_parts_[2].c_str()));
    }
    
    std::string& content() {
        return data_parts_[3];
    }

    std::string const& content() const {
        return data_parts_[3];
    }

    void set_action(action_e v) {
        *reinterpret_cast<action_e*>(const_cast<char*>(data_parts_[0].c_str())) = v;
    }

    void set_type(int v) {
        *reinterpret_cast<int*>(const_cast<char*>(data_parts_[1].c_str())) = v;
    }

    void set_id(int v) {
        *reinterpret_cast<int*>(const_cast<char*>(data_parts_[2].c_str())) = v;
    }

    void set_content(std::string const& v) {
        data_parts_[3] = v;
    }

private:
    std::vector<std::string> data_parts_;

};


#endif //FILE_CHECK_MQ_MESSAGE_T_H
