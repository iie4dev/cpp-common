#pragma once

#include <string>

#include <WinSock2.h>

#include <cpp-common-config.h>

enum HttpRequestMethod {
    POST,
    GET
};

enum HttpVersion {
    HTTP_V1_1,
    HTTP_V1_0
};

enum HttpContentType {
    MULTIPART_FORM_DATA,
    APPLICATION_X_WWW_FORM_URLENCODED
};


class EXPORT_LIB HttpMessageBuilder {
public:
    HttpMessageBuilder(std::string host, std::string path, HttpVersion version);

    HttpMessageBuilder& insert_param_in_url(std::string const& key, std::string const& value);

    HttpMessageBuilder& set_request_method(HttpRequestMethod method = POST);

    HttpMessageBuilder& set_host();

    HttpMessageBuilder& set_user_agent();

    HttpMessageBuilder& set_multipart_form_data(std::string const& boundary = "");

    HttpMessageBuilder& set_form_urlencoded();

    HttpMessageBuilder& set_charset(std::string const& charset);

    HttpMessageBuilder& set_connection(std::string const& connect);

    HttpMessageBuilder& set_request_property(std::string const& key, std::string const& value);

    HttpMessageBuilder& insert_file_data(std::string const& key, std::string const& file_path);

    HttpMessageBuilder& append_data(std::string const& data);

    HttpMessageBuilder& boundary_begin();

    HttpMessageBuilder& boundary_end();

    std::string build_message();

private:
    HttpMessageBuilder& set_request_property(std::string& stream, std::string const& key, std::string const& value);

    std::string host_;
    std::string path_;
    bool is_first_to_insert_param_in_path_;
    HttpVersion version_;

    std::string new_line_;
    std::string boundary_prefix_;
    std::string boundary_;
    std::string http_version_;

    std::string head_stream_;
    std::string body_stream_;

    HttpRequestMethod method_;

};


class EXPORT_LIB HttpConnect {
public:
    HttpConnect();

    void send_request(std::string const& host, std::string const& request);

    std::string receive_response() const;

private:
    struct addrinfo hints_;
    int socket_fd_;
    bool valid_;

};
