//
// Created by limo on 2018/8/9.
//

#ifndef FILE_CHECK_THREADSAFEQUEUEINTERFACE_H
#define FILE_CHECK_THREADSAFEQUEUEINTERFACE_H

#include <memory>

#include <cpp-common/QueueInterface.h>
#include <cpp-common/NonCopyable.h>


template <typename T>
class ThreadSafeQueueInterface : 
		public QueueInterface<T>,
        public NonCopyable {
protected:
	virtual ~ThreadSafeQueueInterface() = default;

public:
	virtual std::shared_ptr<T> wait_and_deque() = 0;
	virtual void wait_and_deque(T& data) = 0;
};


#endif // FILE_CHECK_THREADSAFEQUEUEINTERFACE_H