#pragma once
#include <functional>

#include <Windows.h>

#include <cpp-common-config.h>

#include "StringUtils.h"


using fn_notify_callback_t = std::function<void(string_t const&, string_t const&, string_t const&, DWORD, DWORD)>;

class EXPORT_LIB FolderMonitor {
public:
    FolderMonitor();
    ~FolderMonitor();

    bool monitoring(fn_notify_callback_t const& notify_callback);
    bool monitor_on(string_t const& folder);
    bool monitor_on(string_t const& folder, DWORD notify_filter);
    void stop_monitor();

private:
    bool prepared_;

    DWORD notify_filter_;
    bool monitoring_;

    string_t folder_;
    HANDLE folder_handle_;

    string_t pre_file_name_;
    time_t pre_read_time_point_;

};
