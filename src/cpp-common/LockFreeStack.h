//
// Created by limo on 2018/8/8.
//

#ifndef FILE_CHECK_LOCKFREESTACK_H
#define FILE_CHECK_LOCKFREESTACK_H

#include <atomic>

#include "ThreadSafeStackInterface.h"

template <typename T>
class LockFreeStack : public ThreadSafeStackInterface<T> {
	struct Node;

	struct CountedNodePtr {
        Node *ptr{ nullptr };
        int external_count{ 0 };
        int reserved{ 0 };
    };

	struct Node {
		std::shared_ptr<T> p_data;
		std::atomic<int> internal_count;
		CountedNodePtr next;

		Node(T data) :
			p_data(std::make_shared<T>(std::move(data))),
			internal_count(0) {
		}

		Node(std::shared_ptr<T> data) :
			p_data(std::move(data)),
			internal_count(0) {
		}

	};

	void IncreaseHeadCount(CountedNodePtr& old_counter) {
		CountedNodePtr new_counter;
		do {
			new_counter = old_counter;
			++new_counter.external_count;
		} while (!m_head.compare_exchange_strong(
			old_counter,
			new_counter,
			std::memory_order_acquire,
			std::memory_order_relaxed));

		old_counter.external_count = new_counter.external_count;
	}

public:
	~LockFreeStack() {
		while (LockFreeStack<T>::Pop());
	}

	void Push(T data) override {
		CountedNodePtr new_node;
		new_node.ptr = new Node(std::move(data));
		new_node.external_count = 1;
		new_node.ptr->next = m_head.load(std::memory_order_relaxed);
		while (!m_head.compare_exchange_weak(
			new_node.ptr->next,
			new_node,
			std::memory_order_release,
			std::memory_order_relaxed));
	}

	void Push(std::shared_ptr<T> p_data) override {
		CountedNodePtr new_node;
		new_node.ptr = new Node(std::move(p_data));
		new_node.external_count = 1;
		new_node.ptr->next = m_head.load(std::memory_order_relaxed);
		while (!m_head.compare_exchange_weak(
			new_node.ptr->next,
			new_node,
			std::memory_order_release,
			std::memory_order_relaxed));
	}

	std::shared_ptr<T> Pop() override {
		CountedNodePtr old_head = m_head.load(std::memory_order_relaxed);
		while (true) {
			// increase the count of threads own current head
			IncreaseHeadCount(old_head);

			Node* const ptr = old_head.ptr;
			if (nullptr == ptr) {  // return nullptr if stack is empty
				return nullptr;
			}

			if (m_head.compare_exchange_strong(
				old_head,
				ptr->next,
				std::memory_order_relaxed)) {
				// luck one got the head in this turn, delete the node if need be and return p_data

				std::shared_ptr<T> res = nullptr;
				res.swap(ptr->p_data);

				int const count_increase = old_head.external_count - 2;  // count of threads own current head except 
				                                                         // current thread

				// if all of the other threads have failed, no more thread would own current head, just delete it
				if (ptr->internal_count.fetch_add(count_increase, std::memory_order_release) == -count_increase) {
					delete ptr;
				}
				return res;
			} else if (ptr->internal_count.fetch_add(-1, std::memory_order_relaxed) == 1) {
				// did not get the head in this turn and the luck one needs helps to delete the node before next turn.
				// the lucky one needs help in one case that not all of the other threads have failed, it just could
				// not delete it directly. if the condition of this branch matched which means current thread is the 
				// last thread own the old head whose p_data has been taken off by the lucky one, current thread should
				// delete the node cause no other one could do that.

				auto const unused = ptr->internal_count.load(std::memory_order_acquire);
				delete ptr;
			}
		}
	}

	bool Pop(T& data) override {
		CountedNodePtr old_head = m_head.load(std::memory_order_relaxed);
		while (true) {
			// increase the count of threads own current head
			IncreaseHeadCount(old_head);

			Node* const ptr = old_head.ptr;
			if (!ptr) {  // return false if stack is empty
				return false;
			}

			if (m_head.compare_exchange_strong(
				old_head,
				ptr->next,
				std::memory_order_relaxed)) {
				// lucky one got the head in this turn, delete the node if need be and return p_data

				data = std::move(*(ptr->p_data));

				int const count_increase = old_head.external_count - 2;  // count of threads own current head except 
																		 // current thread

																		 // if all of the other threads have failed, no more thread would own current head, just delete it
				if (ptr->internal_count.fetch_add(count_increase, std::memory_order_release) == -count_increase) {
					delete ptr;
				}

				return true;
			} else if (ptr->internal_count.fetch_add(-1, std::memory_order_relaxed) == 1) {
				// did not get the head in this turn and the luck one needs helps to delete the node before next turn.
				// the lucky one needs help in one case that not all of the other threads have failed, it just could
				// not delete it directly. if the condition of this branch matched which means current thread is the 
				// last thread own the old head whose p_data has been taken off by the lucky one, current thread should
				// delete the node cause no other one could do that.

				auto const unused = ptr->internal_count.load(std::memory_order_acquire);
				delete ptr;
			}
		}
	}

	std::shared_ptr<T> WaitAndPop() override {
		// need implement
		throw std::exception();
	}

	void WaitAndPop(T& data) override {
		// need implement
		throw std::exception();
	}

	bool Empty() const override {
		return m_head.load(std::memory_order_relaxed).ptr != nullptr;
	}

private:
	std::atomic<CountedNodePtr> m_head;
	std::atomic<size_t> m_size;

};

#endif // FILE_CHECK_LOCKFREESTACK_H