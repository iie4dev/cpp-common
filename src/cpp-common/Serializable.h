#pragma once

//
// Created by limo on 2018/7/27.
//

#include <string>
#include <fstream>

#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>

#include <cpp-common/StringUtils.h>

struct Serializable {
    virtual ~Serializable() = default;
    
    virtual std::string dump() const = 0;
    virtual void load(std::string const& token) = 0;

};

#define FUNC_DUMP_IMPL std::string dump() const override { \
    std::ostringstream ss; \
    { \
        cereal::BinaryOutputArchive out_archive(ss); \
        out_archive(*this); \
    } \
    return ss.str(); \
}

#define FUNC_LOAD_IMPL void load(std::string const&token) override { \
    std::istringstream ss; \
    ss.str(token); \
    { \
        cereal::BinaryInputArchive in_archive(ss); \
        in_archive(*this); \
    } \
}


template <class Config>
bool load_json(string_t const &config_file, Config& config) {
    std::ifstream config_stream(config_file);
    if (config_stream.is_open()) {
        try {
            cereal::JSONInputArchive in_archive(config_stream);
            in_archive(config);
            return true;
        } catch (std::exception& e) {
            std::cout << e.what() << std::endl;
            return false;
        }
    }
    return false;
}

template <class Config>
bool dump_json(string_t const &config_file, Config const& config) {
    std::ofstream config_stream(config_file);
    if (config_stream.is_open()) {
        try {
            cereal::JSONOutputArchive out_archive(config_stream);
            out_archive(config);
            return true;
        } catch (std::exception &e) {
            std::cout << e.what() << std::endl;
            return false;
        }
    }
    return false;
}
