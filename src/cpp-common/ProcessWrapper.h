//
// Created by limo on 8/19/2018.
//

#ifndef FILE_CHECK_PROCESSWRAPPER_H
#define FILE_CHECK_PROCESSWRAPPER_H

#include <string>
#include <vector>
#include <thread>
#include <memory>

#include <cpp-common/NonCopyable.h>
#include <cpp-common/StringUtils.h>
#include <cpp-common/DefBase.h>

#include <Windows.h>


class ProcessJobHelper
{
public:
    ProcessJobHelper() : job_handle_(nullptr)
    {
    }

    explicit ProcessJobHelper(string_t const& job_name) : job_handle_(nullptr)
    {
        create_job(job_name);
    }

    ~ProcessJobHelper()
    {
        shutdown_job();
    }

    HANDLE create_job(string_t const& job_name)
    {
        job_name_ = job_name;
        job_handle_ = CreateJobObject(nullptr, nullptr);
        if (is_valid_job())
        {
            JOBOBJECT_EXTENDED_LIMIT_INFORMATION job_info;
            memset(&job_info, 0, sizeof(job_info));
            // Configure all child processes associated with the job to terminate when the
            job_info.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
            auto const ret = SetInformationJobObject(job_handle_
                    , JobObjectExtendedLimitInformation
                    , &job_info
                    , sizeof(job_info));
            if (TRUE == ret)
                return job_handle_;
        }

        return job_handle_ = nullptr;;
    }

    HANDLE get_job() const
    {
        return job_handle_;
    }

    void shutdown_job()
    {
        if (IS_VALID_HANDLE(job_handle_))
        {
            TerminateJobObject(job_handle_, 0);
            ::CloseHandle(job_handle_);
        }
        job_handle_ = nullptr;
    }

    bool is_valid_job() const
    {
        return IS_VALID_HANDLE(job_handle_);
    }

private:
    string_t job_name_;
    HANDLE job_handle_;
};


class ProcessWrapper final : public NonCopyable
{
    enum process_status_e
    {
        STATIC,
        RUNNING,
        CRASHED
    };

public:
    explicit ProcessWrapper(string_t process_name);

    ~ProcessWrapper();

    ProcessWrapper(ProcessWrapper&& rhv);

    ProcessWrapper& operator=(ProcessWrapper&& rhv);

    template <typename T>
    void push_param(T const& param)
    {
        params_.push_back(TO_STR(param));
    }

    void push_path(string_t const& path);
    void push_param(string_t const& param);
    void push_param(const char_t* param);

    void break_away_job(bool const break_away) {
        break_away_job_ = break_away;
    }

    string_t get_process_name() const;
    size_t get_param_num() const;
    std::vector<string_t> get_params() const;

    void startup();
    void startup(HANDLE h_read, HANDLE h_write, HANDLE h_error);
    void startup_new_console(bool show_window = true);
    void startup_new_console(HANDLE h_read_pipe, HANDLE h_write_pipe, HANDLE h_error);

    void shutdown();
    bool running() const;
    bool wait(DWORD timeout = INFINITE) const;

    bool bind_job(HANDLE h_job) const;

private:
    void startup(bool create_new_console, bool show_window,
                 bool redirect_io, HANDLE h_read_pipe, HANDLE h_write_pipe, HANDLE h_error);

    string_t process_name_;
    std::vector<string_t> params_;
    process_status_e status_;

    bool break_away_job_;

#ifdef WIN32
    STARTUPINFO si_{};
    PROCESS_INFORMATION pi_{};
#else
#endif
};


#endif //FILE_CHECK_PROCESSWRAPPER_H
