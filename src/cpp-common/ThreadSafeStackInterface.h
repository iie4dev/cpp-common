//
// Created by limo on 2018/8/10.
//

#ifndef FILE_CHECK_THREADSAFESTACK_H
#define FILE_CHECK_THREADSAFESTACK_H

#include <memory>

#include "StackInterface.h"
#include "NonCopyable.h"


template <typename T>
class ThreadSafeStackInterface : 
		public StackInterface<T>,
		public NonCopyable {
protected:
	virtual ~ThreadSafeStackInterface() = default;

public:
	virtual std::shared_ptr<T> WaitAndPop() = 0;
	virtual void WaitAndPop(T& data) = 0;
};


#endif // FILE_CHECK_THREADSAFESTACK_H