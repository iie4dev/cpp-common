#pragma once

#include "StringUtils.h"

#include <functional>

#include <Windows.h>

#include <cpp-common-config.h>

namespace np_limo {
    namespace file {
        namespace dir {
            class EXPORT_LIB TempFile {
            public:
                explicit TempFile(bool auto_remove_file, string_t prefix, string_t suffix);

                ~TempFile();

                string_t create_temp_file();

                string_t create_temp_file(CFSTR temp_folder);

                string_t const &get_temp_file() const;

            private:
                string_t temp_file_;
                bool auto_remove_file_;
                string_t prefix_;
                string_t suffix_;

            };


            EXPORT_LIB bool GetHomeDir(string_t &dir);

            EXPORT_LIB bool GetExeDir(string_t &dir);

            EXPORT_LIB bool GetWindowsDir(string_t &dir);

            EXPORT_LIB bool GetSystemDir(string_t &dir);

            EXPORT_LIB bool GetLocalAppDataPath(string_t &path);

            EXPORT_LIB bool RemoveDir(CFSTR path);

            EXPORT_LIB bool CreateDir(CFSTR path);

            EXPORT_LIB bool RemoveFile(CFSTR path);

            EXPORT_LIB bool CreateFileIfNotExist(CFSTR path);

            EXPORT_LIB bool CreateDirRecurrently(CFSTR path);

            EXPORT_LIB bool RemoveDirRecurrently(CFSTR path);

            EXPORT_LIB bool RemoveFileWithDirIfEmpty(CFSTR path);

            EXPORT_LIB bool CreateFileWithDirIfNotExist(CFSTR path);

            EXPORT_LIB bool RemoveFilesUnderDir(CFSTR path);

            EXPORT_LIB bool GetCurrentDir(string_t &dir);

            EXPORT_LIB bool SetCurrentDir(CFSTR dir);

            EXPORT_LIB bool TryUsePath(CFSTR path, std::function<bool(CFSTR)> const &handlePath);

            EXPORT_LIB bool GetTempFolder(string_t &path);

            EXPORT_LIB bool CreateRandomFolder(string_t &folder, string_t const &parent);

            EXPORT_LIB bool CreateTempFolder(string_t &folder, CFSTR prefix);

            EXPORT_LIB bool RemoveDirWithSubItems(CFSTR name);

            EXPORT_LIB bool DoesDirEmpty(CFSTR path);

            EXPORT_LIB bool TravelFilesUnderDir(string_t const &dir, std::function<bool(string_t const &)> const &cb);

            EXPORT_LIB bool TravelFilesUnderDir(string_t const &dir,
                                                std::function<bool(string_t const &)> const &cb,
                                                std::function<bool(string_t const &)> const &filter);
        }
    }
}
