#include <cpp-common/HttpConnect.h>

#include <iomanip>
#include <sstream>
#include <fstream>

#include <WS2tcpip.h>


#ifdef WIN32
#pragma comment(lib, "ws2_32.lib")
#endif

const auto Default_New_Line = "\r\n";
const auto Default_Boundary_Prefix = "--";
const auto Default_New_Bound = "----WebKitFormBoundary7MA4YWxkTrZu0gW";
const auto Http_V1_0 = "HTTP/1.0";
const auto Http_V1_1 = "HTTP/1.1";


static
std::string trim_response_header(std::string const& buff);

static
std::string get_request_method_string(HttpRequestMethod method);

static
std::string get_http_version_string(HttpVersion version);

static
std::string read_file_in_raw(std::string const& file_path);

static
std::string url_encode(std::string const& value);


HttpMessageBuilder::
HttpMessageBuilder(std::string host, std::string path, HttpVersion const version)
    : host_(std::move(host))
    , path_(std::move(path))
    , is_first_to_insert_param_in_path_(true)
    , version_(version)
    , new_line_(Default_New_Line)
    , boundary_prefix_(Default_Boundary_Prefix)
    , boundary_(Default_New_Bound)
    , http_version_(Http_V1_1) {
    method_ = GET;
    head_stream_.clear();
    body_stream_.clear();
}

HttpMessageBuilder& HttpMessageBuilder::
insert_param_in_url(std::string const& key, std::string const& value) {
    if (is_first_to_insert_param_in_path_) {
        path_.append("?");
        is_first_to_insert_param_in_path_ = false;
    } else {
        path_.append("&");
    }
    path_.append(key).append("=").append(value);
    return *this;
}

HttpMessageBuilder& HttpMessageBuilder::set_request_method(HttpRequestMethod const method) {
    host_ = url_encode(host_);
    path_ = url_encode(path_);
    method_ = method;

    head_stream_.append(get_request_method_string(method)).append(" ")
        .append(path_).append(" ")
        .append(get_http_version_string(version_))
        .append(new_line_);
    return *this;
}

HttpMessageBuilder& HttpMessageBuilder::set_host() {
    return set_request_property("Host", host_);
}

HttpMessageBuilder& HttpMessageBuilder::set_user_agent() {
    return set_request_property("User-Agent",
                                "Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3");
}

HttpMessageBuilder& HttpMessageBuilder::set_multipart_form_data(std::string const& boundary) {
    if (!boundary.empty()) {
        boundary_ = boundary;
    }
    return set_request_property("Content-Type", "multipart/form-data; boundary=" + boundary_);
}

HttpMessageBuilder& HttpMessageBuilder::set_form_urlencoded() {
    return set_request_property("Content-Type", "application/x-www-form-urlencoded");
}

HttpMessageBuilder& HttpMessageBuilder::set_charset(std::string const& charset) {
    return set_request_property("Charset", charset);
}

HttpMessageBuilder& HttpMessageBuilder::set_connection(std::string const& connect) {
    return set_request_property("Connection", connect);
}

HttpMessageBuilder& HttpMessageBuilder::set_request_property(std::string const& key, std::string const& value) {
    return set_request_property(head_stream_, key, value);
}

HttpMessageBuilder& HttpMessageBuilder::insert_file_data(std::string const& key, std::string const& file_path) {
    boundary_begin();

    auto const name_string = std::string("name=\"").append(key).append("\"; ");
    auto const filename_string = std::string("filename=\"").append(file_path).append("\"");
    auto const disposition = std::string("form-data; ").append(name_string).append(filename_string);

    set_request_property(body_stream_, "Content-Disposition", disposition);
    set_request_property(body_stream_, "Content-Type", "application/octet-stream");

    body_stream_
        .append(new_line_)
        .append(new_line_)
        .append(read_file_in_raw(file_path)).append(new_line_);

    return boundary_end();
}

HttpMessageBuilder& HttpMessageBuilder::append_data(std::string const& data) {
    body_stream_.append(data);
    return *this;
}

HttpMessageBuilder& HttpMessageBuilder::boundary_begin() {
    body_stream_.append(boundary_prefix_).append(boundary_).append(new_line_);
    return *this;
}

HttpMessageBuilder& HttpMessageBuilder::boundary_end() {
    body_stream_.append(boundary_prefix_).append(boundary_).append(boundary_prefix_).append(new_line_);
    return *this;
}

std::string HttpMessageBuilder::build_message() {
    std::string content_length_filed;
    if (method_ == POST) {
        set_request_property(content_length_filed, "Content-Length", std::to_string(body_stream_.length()));
    }
    return std::string(head_stream_).append(content_length_filed)
        .append(new_line_)
        .append(body_stream_);
}

HttpMessageBuilder& HttpMessageBuilder::set_request_property(std::string& stream, std::string const& key
                                                           , std::string const& value) {
    stream.append(key).append(": ").append(value).append(new_line_);
    return *this;
}

HttpConnect::HttpConnect() : hints_()
, socket_fd_(-1)
, valid_(false) {
#ifdef WIN32
    WSADATA wsa;
    memset(&wsa, 0, sizeof(WSADATA));
    WSAStartup(MAKEWORD(2, 2), &wsa);
#endif

    memset(&hints_, 0, sizeof(addrinfo));
    hints_.ai_family = AF_INET; /* Allow IPv4 */
    hints_.ai_flags = AI_PASSIVE; /* For wildcard IP address */
    hints_.ai_protocol = 0; /* Any protocol */
    hints_.ai_socktype = SOCK_STREAM;
}

void HttpConnect::send_request(std::string const& host, std::string const& request) {
    struct addrinfo* addrinfo_list;
    auto ret = getaddrinfo(host.c_str(), "http", &hints_, &addrinfo_list);
    if (-1 == ret) {
        throw std::runtime_error("cannot get addrinfo from host !");
    }

    for (auto curr = addrinfo_list; nullptr != curr; curr = curr->ai_next) {
        socket_fd_ = socket(curr->ai_family, curr->ai_socktype, curr->ai_protocol);
        if (socket_fd_ < 0) {
            continue;
        }
        if (-1 == connect(socket_fd_, curr->ai_addr, sizeof(sockaddr))) {
            continue;
        }

#ifdef WIN32
        ret = send(socket_fd_, request.c_str(), request.size(), 0);
#else
        write(socket_fd_, request.c_str(), request.size());
#endif
        if (ret <= 0) {
            throw std::runtime_error("failed to send request with err code " + std::to_string(ret));
        }

        freeaddrinfo(addrinfo_list);
        valid_ = true;
        return;
    }

    throw std::runtime_error("cannot create connection from host !");
}

std::string HttpConnect::receive_response() const {
    if (!valid_) {
        throw std::runtime_error("just not request yet, do request first!");
    }

    char buf[10 * 1024] = { 0 };
    auto offset = 0;
    int rc;

#ifdef WIN32
    while ((rc = recv(socket_fd_, buf + offset, 1024, 0)))
#else
    while (rc = read(socket_fd_, buf + offset, 1024))
#endif
    {
        offset += rc;
    }

#ifdef WIN32
    closesocket(socket_fd_);
#else
    close(sockfd);
#endif

    buf[offset] = 0;
    return trim_response_header(buf);
}


static
std::string trim_response_header(std::string const& buff) {
    auto const idx = buff.find("\r\n\r\n");
    if (idx != std::string::npos && idx + 4 < buff.size()) {
        return buff.substr(idx + 4);
    }
    return "";
}

static
std::string get_request_method_string(HttpRequestMethod const method) {
    std::string method_string;
    switch (method) {
        case POST:
            method_string = "POST"; break;
        case GET:
            method_string = "GET"; break;
        default:
            method_string = "POST";
    }
    return method_string;
}

static
std::string get_http_version_string(HttpVersion const version) {
    switch (version) {
        case HTTP_V1_0: return Http_V1_0;
        case HTTP_V1_1: return Http_V1_1;
        default: return Http_V1_1;
    }
}

static
std::string read_file_in_raw(std::string const& file_path) {
    std::ifstream file_stream(file_path, std::ios::binary);
    if (file_stream.is_open()) {
        return std::string(std::istreambuf_iterator<char>(file_stream),
            std::istreambuf_iterator<char>());
    }
    return "";
}

inline
std::string url_encode(std::string const& value) {
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;

    for (auto c : value) {
        // Keep alphanumeric and other accepted characters intact
        if (c != ' ') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << std::uppercase;
        escaped << '%' << std::setw(2) << int(c);
        escaped << std::nouppercase;
    }

    return escaped.str();
}
