#include "cpp-common/FileName.h"

#include <vector>

#include <Windows.h>

#include "cpp-common/FileDir.h"


static const char_t * const kSuperPathPrefix = TEXT(R"(\\?\)");
static const char_t * const kSuperUncPrefix = TEXT(R"(\\?\UNC\)");

#define IS_SEPAR(c) IS_PATH_SEPARATOR(c)

#define IS_DEVICE_PATH(s)          (IS_SEPAR((s)[0]) && IS_SEPAR((s)[1]) && (s)[2] == '.' && IS_SEPAR((s)[3]))
#define IS_SUPER_PREFIX(s)         (IS_SEPAR((s)[0]) && IS_SEPAR((s)[1]) && (s)[2] == '?' && IS_SEPAR((s)[3]))
#define IS_SUPER_OR_DEVICE_PATH(s) (IS_SEPAR((s)[0]) && IS_SEPAR((s)[1]) && ((s)[2] == '?' || (s)[2] == '.') && IS_SEPAR((s)[3]))

#define IS_UNC_WITH_SLASH(s) ( \
    ((s)[0] == 'U' || (s)[0] == 'u') && \
    ((s)[1] == 'N' || (s)[1] == 'n') && \
    ((s)[2] == 'C' || (s)[2] == 'c') && \
    IS_SEPAR((s)[3]) \
)

namespace np_limo
{
    namespace file
    {
        namespace name
        {
            const unsigned kDevicePathPrefixSize = 4;
            const unsigned kSuperPathPrefixSize = 4;
            const unsigned kSuperUncPathPrefixSize = kSuperPathPrefixSize + 4;
            const unsigned kDrivePrefixSize = 3;  /* c:\ */

            static bool IsThereDotsFolderInPath(CFSTR path);

            bool IsAbsolutePath(CFSTR path) {
                return IS_SEPAR(path[0]) || IsDrivePath2(path);
            }

            bool IsDrivePath(CFSTR const path) {         /* A:\ */
                return isalpha(path[0]) && ':' == path[1] && IS_SEPAR(path[2]);
            }

            bool IsDrivePath2(CFSTR const path) {        /* A: */
                return isalpha(path[0]) && ':' == path[1];
            }

            bool IsDevicePath(CFSTR const path) {
                /* \\.\ */
                if (!IS_DEVICE_PATH(path))
                    return false;

                auto const pathLen = SSTRLEN(path);

                /* *****: */
                if (6 == pathLen && ':' == path[5])
                    return true;

                /* ****PhysicalDrive */
                if (pathLen < 18 || pathLen > 22 ||
                    !str::IsPrefix(path + kDevicePathPrefixSize, "PhysicalDrive"))
                    return false;

                /* *****************d~d*~ */
                for (unsigned i = 17; i < pathLen; ++i)
                    if (path[i] < '0' || path[i] > '9')
                        return false;
                return true;
            }

            bool IsSuperPath(CFSTR const path) {          /* \\?\ */
                return IS_SUPER_PREFIX(path);
            }

            bool IsDeviceOrSuperPath(CFSTR const path) {  /* \\.\ or \\?\ */
                return IS_SUPER_OR_DEVICE_PATH(path);
            }

            bool IsSuperUncPath(CFSTR const path) {       /* \\?\UNC\ */
                return IS_SUPER_PREFIX(path) && IS_UNC_WITH_SLASH(path + kSuperPathPrefixSize);
            }

            bool IsNetworkPath(CFSTR const path) {        /* \\?\UNC\ or \\SERVER */
                if (!IS_SEPAR(path[0]) || !IS_SEPAR(path[1]))
                    return false;

                if (IsSuperUncPath(path))
                    return true;

                // is network path if not device and not super
                return '.' != path[2] && '?' != path[2];
            }

            unsigned GetNetworkServerPrefixSize(CFSTR const path) {
                if (!IS_SEPAR(path[0]) || !IS_SEPAR(path[1]))  // path should start with "\\"
                    return 0;

                unsigned prefixSize = 2;  // size of prefix "\\" in "\\server\root\~"
                if (IsSuperUncPath(path))
                    // size of prefix "\\?\UNC"
                    prefixSize = kSuperUncPathPrefixSize;
                else {
                    if ('.' == path[2] || '?' == path[2]) // path should not be like "\\.\" or "\\?\"
                        return 0;
                }
                // for "\\server\root\~", pos point at the separator behind "server"
                // for "\\?\UNC\server\root\~", pos point at the separator behind "server"
                auto const pos = FindSepar(path + prefixSize);
                return pos < 0 ? 0 : prefixSize + pos + 1;
            }

            int FindSepar(CFSTR const path) {
                for (auto curr = path; ; ++curr) {
                    if (0 == *curr) return -1;
                    if (IS_SEPAR(*curr))
                        return static_cast<int>(curr - path);
                }
            }

            int FindSeparReverse(CFSTR const path) {
                auto lastSeparIndex = -1;
                auto curr = path;
                while (*curr) {
                    if (IS_SEPAR(*curr))
                        lastSeparIndex = static_cast<int>(curr - path);
                    ++curr;
                }
                return lastSeparIndex;
            }

            void NormalizeDirPathPrefix(string_t& path) {
                if (path.empty()) return;
                if (!str::IsPathSeparator(path.back()))
                    path.push_back(PATH_SEPARATOR);
            }

            bool IsThereDotsFolderInPath(CFSTR const path) {
                for (unsigned i = 0; ; ++i) {
                    auto const prevChar = path[i];
                    if (0 == prevChar) return false;
                    if ('.' == prevChar && (0 == i || IS_SEPAR(path[i - 1]))) {
                        auto const nextChar = path[i + 1];
                        if (0 == nextChar ||                             // match '.'
                            IS_SEPAR(nextChar) ||                        // match './'
                            ('.' == nextChar && (0 == path[i + 2] ||     // match '..'
                            IS_SEPAR(path[i + 2])))   // match '../'
                            ) {
                            return true;
                        }
                    }
                }
            }

            unsigned GetRootPrefixSizeOfNetworkPath(CFSTR const path) {
                // Network path: we look "server\path\" as root prefix
                auto const pos1St = FindSepar(path);
                if (pos1St < 0)
                    return 0;
                auto const pos2Nd = FindSepar(path + static_cast<unsigned>(pos1St) + 1);
                if (pos2Nd < 0)
                    return 0;
                return pos1St + pos2Nd + 2;
            }

            unsigned GetRootPrefixSizeOfSimplePath(CFSTR const path) {
                if (IsDrivePath(path))  // path is "C:/~" (windows full local path)
                    return kDrivePrefixSize;

                if (!IS_SEPAR(path[0])) // path is not "/~" 
                    return 0;

                // path is "/~" 

                if (path[1] == 0 || !IS_SEPAR(path[1]))  // path not start with "//"
                    return 1;

                auto const size = GetRootPrefixSizeOfNetworkPath(path + 2);
                return (size == 0) ? 0 : 2 + size;
            }

            unsigned GetRootPrefixSizeOfSuperPath(CFSTR const path) {
                if (IS_UNC_WITH_SLASH(path + kSuperPathPrefixSize)) {
                    auto const size = GetRootPrefixSizeOfNetworkPath(path + kSuperUncPathPrefixSize);
                    return (size == 0) ? 0 : kSuperUncPathPrefixSize + size;
                }

                // we support \\?\c:\ paths and volume GUID paths \\?\Volume{GUID}\"
                auto const pos = FindSepar(path + kSuperPathPrefixSize);
                if (pos < 0)
                    return 0;
                return kSuperPathPrefixSize + pos + 1;
            }

            bool ResolveDotsFolder(string_t &path) {
                std::vector<size_t> concretePaths;
                size_t writeCurr = 0, folderStart = 0, folderLen = 0;

                auto const updatePath = [&]()
                {
                    if (2 == folderLen &&
                        TEXT('.') == path[folderStart] && TEXT('.') == path[folderStart + 1]) {
                        // curr folder is '..', pop back from folder stack
                        if (concretePaths.empty())
                            return false;
                        writeCurr = concretePaths.back();
                        concretePaths.pop_back();
                    } else if (1 == folderLen &&
                               TEXT('.') == path[folderStart]) {
                        // curr folder is '.', do nothing
                    } else {
                        // curr folder is a normal name, push back into folder stack
                        concretePaths.push_back(writeCurr);    // writeCurr points to start of folder
                        for (size_t i = 0; i < folderLen; ++i)
                            path[writeCurr++] = path[folderStart + i];
                        path[writeCurr++] = path[folderStart + folderLen];
                    }
                    folderStart += folderLen + 1;
                    folderLen = -1;
                    return true;
                };

                for (auto const c : path) {
                    if (IS_SEPAR(c)) {
                        if (!updatePath())
                            return false;
                    }
                    ++folderLen;
                }

                if (folderLen > 0) {
                    if (!updatePath())
                        return false;
                    --writeCurr;  // do not need the last '\0'
                }
                path.resize(writeCurr);
                return true;
            }

            unsigned GetRootPrefixSize(CFSTR path) {
                if (IS_DEVICE_PATH(path))
                    return kDevicePathPrefixSize;
                if (IsSuperPath(path))
                    return GetRootPrefixSizeOfSuperPath(path);
                return GetRootPrefixSizeOfSimplePath(path);
            }

            bool GetFullPath(string_t& fullPath, CFSTR const dirPrefix, CFSTR const path) {
                fullPath = string_t(path);
                auto const prefixSize = GetRootPrefixSize(path);

                // if there is root prefix, just keep the prefix and resolve dots folder
                if (prefixSize != 0) {
                    if (!IsThereDotsFolderInPath(path + prefixSize))
                        return true;

                    auto rem = string_t(path + prefixSize);
                    if (!ResolveDotsFolder(rem))
                        return true; // maybe false;

                    fullPath = fullPath.substr(0, prefixSize).append(rem);
                    return true;
                }

                // if there is no root prefix which means <path> is a related path, we need 
                // find the absolute curr directory
                string_t currDirectory;
                if (dirPrefix) {
                    currDirectory = string_t(dirPrefix);
                } else {
                    if (!dir::GetCurrentDir(currDirectory))
                        return false;
                }
                NormalizeDirPathPrefix(currDirectory);

                // find the prefix size
                unsigned fixedSize;
                if (IsSuperPath(currDirectory.c_str())) {
                    // curr directory is super path
                    fixedSize = GetRootPrefixSizeOfSuperPath(currDirectory.c_str());
                    if (0 == fixedSize)
                        return false;
                } else {
                    if (IsDrivePath(currDirectory.c_str())) {
                        // curr directory is drive path
                        fixedSize = kDrivePrefixSize;
                    } else {
                        // curr directory is network path, should start with "//"
                        if (!IS_PATH_SEPARATOR(currDirectory[0]) || IS_PATH_SEPARATOR(currDirectory[1]))
                            return false;
                        fixedSize = GetRootPrefixSizeOfNetworkPath(currDirectory.c_str() + 2);  // 2 is len of "//"
                        if (0 == fixedSize)
                            return false;
                        fixedSize += 2;
                    }
                }

                // get the path part that 
                string_t pathWithoutPrefix;
                if (IS_PATH_SEPARATOR(path[0])) {
                    pathWithoutPrefix = string_t(path + 1);
                } else {
                    pathWithoutPrefix.assign(currDirectory.c_str() + fixedSize)
                        .append(path);
                }

                if (!ResolveDotsFolder(pathWithoutPrefix))
                    return false;

                fullPath.assign(currDirectory.substr(0, fixedSize))  // prefix
                    .append(pathWithoutPrefix);
                return true;
            }

            string_t JoinPath(string_t const& folder, string_t const& name) {
                if (IS_SEPAR(folder.back()))
                    return folder + name;
                return folder + PATH_SEPARATOR + name;
            }

            void TrimEndSeparator(string_t& path) {
                if (!path.empty() && IS_SEPAR(path.back()))
                    path.pop_back();
            }

            /*
             * Most of Windows versions have problems, if some file or dir name
             * contains '.' or ' ' at the end of name (Bad Path).
             * To solve_task that problem, we always use Super Path ("\\?\" prefix and full path)
             * in such cases. Note that "." and ".." are not bad names.

             * There are 3 cases:
             *   1) If the path is already Super Path, we use that path
             *   2) If the path is not Super Path :
             *     2.1) Bad Path;  we use only Super Path.
             *     2.2) Good Path; we use Main Path. If it fails, we use Super Path.

             * NeedToUseOriginalPath returns:
             *   kUseOnlyMain     : Super already
             *   kUseOnlySuper    : not Super, Bad Path
             *   kUseMainAndSuper : not Super, Good Path
             */
            ESuperPathType CheckSuperPathType(CFSTR const path) {
                if (IsDeviceOrSuperPath(path)) {
                    // if is not super and bad path, use only super
                    if ('.' != path[2]) {
                        if (IsThereDotsFolderInPath(path + kSuperPathPrefixSize))
                            return kUseOnlySuper;
                    }
                    // else is already super, just use it
                    return kUseOnlyMain;
                }

                // here the path is not super nor device
                for (unsigned i = 0; ; ++i) {
                    auto const prevChar = path[i];
                    if (0 == prevChar)
                        // good path but not super, use main firstly
                        return kUseMainAndSuper;

                    if ('.' == prevChar || ' ' == prevChar) {
                        auto const nextChar = path[i];
                        if (0 == nextChar || IS_SEPAR(nextChar)) {
                            // there is a folder ends with '.' or ' ' in path

                            if ('.' == prevChar) {
                                if (0 == i || IS_SEPAR(path[i - 1]))
                                    // folder with full name of '.' is good, just continue
                                    continue;
                                if ('.' == path[i - 1])
                                    if (i == 1 || IS_SEPAR(path[i - 2]))
                                        // folder with full name of '..' is good, just continue
                                        continue;
                            }
                            // bad path and not super, need to use super
                            return kUseOnlySuper;
                        }
                    }
                }
            }

            bool SuperPath(string_t& superPath, CFSTR const path) {
                superPath.clear();
                if (0 == path[0]) return true;

                if ('.' == path[0] && (0 == path[1] ||       // path is '.'
                    ('.' == path[1] && 0 == path[2]))    // path is '..'
                    ) {
                    return true;
                }

                if (IsDeviceOrSuperPath(path)) {
                    if ('.' == path[2])
                        // device path can tolerate bad name
                        return true;

                    // here path is super

                    if (!IsThereDotsFolderInPath(path + kSuperPathPrefixSize))
                        // the folder is good name
                        return true;

                    // here path is super with a bad name

                    auto const fixedSize = GetRootPrefixSizeOfSuperPath(path);
                    if (fixedSize == 0)
                        return true;

                    auto remind = string_t(path + fixedSize);
                    if (!ResolveDotsFolder(remind))
                        return true;

                    superPath.append(path, fixedSize);
                    superPath.append(remind);
                    return true;
                }

                if (IS_SEPAR(path[0])) {
                    if (IS_SEPAR(path[1])) {
                        // start with '\\', is a network path
                        auto const fixedSize = GetRootPrefixSizeOfNetworkPath(path + 2);
                        auto remind = string_t(path + 2 + fixedSize);
                        if (!ResolveDotsFolder(remind))
                            return false;
                        superPath.append(kSuperUncPrefix);
                        superPath.append(path, 2, fixedSize);
                        superPath.append(remind);
                        return true;
                    }
                } else {
                    // is drive path
                    if (IsDrivePath2(path)) {
                        unsigned prefixSize = 2;
                        if (IsDrivePath(path)) {
                            prefixSize = kDrivePrefixSize;
                        }
                        auto remind = string_t(path + prefixSize);
                        if (!ResolveDotsFolder(remind))
                            return false;  // return true?
                        superPath.append(kSuperPathPrefix);
                        superPath.append(path, 0, prefixSize);
                        superPath.append(remind);
                    }
                }

                // here the path is a relative path, we need add current directory
                // before the path

                string_t currDir;
                if (!dir::GetCurrentDir(currDir)) return false;

                NormalizeDirPathPrefix(currDir);

                unsigned fixedSize, fixedSizeStart = 0;
                char_t const *superMarker = nullptr;

                // current path will not be a device, so there are three possible
                // type for this path: super, drive and network.
                if (IsSuperPath(currDir.c_str())) {
                    // current path is super
                    fixedSize = GetRootPrefixSizeOfSuperPath(currDir.c_str());
                    if (0 == fixedSize)
                        return false;
                } else {
                    if (IsDrivePath(currDir.c_str())) {
                        // current path is drive
                        superMarker = kSuperPathPrefix;
                        fixedSize = kDrivePrefixSize;
                    } else {
                        // current path is network
                        if (!IS_SEPAR(currDir[0]) || !IS_SEPAR(currDir[1]))
                            return false;

                        fixedSizeStart = 2;
                        fixedSize = GetRootPrefixSizeOfNetworkPath(currDir.c_str() + 2);
                        if (0 == fixedSize)
                            return false;
                        superMarker = kSuperUncPrefix;
                    }
                }

                string_t relPath;
                if (IS_SEPAR(path[0])) {
                    relPath.assign(path + 1);
                } else {
                    relPath.assign(currDir.c_str(), fixedSizeStart + fixedSize);
                    relPath.append(path);
                }

                if (!ResolveDotsFolder(relPath))
                    return false;

                if (superMarker)
                    superPath.append(superMarker);
                superPath.append(currDir, fixedSizeStart, fixedSize);
                superPath.append(relPath);
                return true;
            }

            /**
             * TODO: Current implementation has not considered the prefix like "//?/"
             */
            bool SplitPath(std::vector<string_t> &parts, bool const needFolder, bool const needName, 
                           bool const needNameWithExt, bool const needExt, CFSTR const path) {

                auto const lastSeparIndex = name::FindSeparReverse(path);

                if (needFolder)
                    parts.emplace_back(path, 0, lastSeparIndex + 1);

                if (needName && needNameWithExt)
                    parts.emplace_back(path + lastSeparIndex + 1);

                if (needName || needExt) {
                    auto const lastDotIndex = str::find_last_char(path + lastSeparIndex + 1, '.');

                    if (needName)
                        parts.emplace_back(path, lastSeparIndex + 1, lastDotIndex);
                    
                    if (needExt)
                        parts.emplace_back(lastDotIndex < 0 ? TEXT("") : path + lastSeparIndex + lastDotIndex + 2);
                }
                return true;
            }

            bool SplitFolderFromPath(string_t& folder, CFSTR const path) {
                std::vector<string_t> parts;
                if (SplitPath(parts, true, false, false, false, path)) {
                    folder = std::move(parts[0]);
                    return true;
                }
                return false;
            }

            bool SplitNameWithExtFromPath(string_t& name, CFSTR const path) {
                std::vector<string_t> parts;
                if (SplitPath(parts, false, true, true, false, path)) {
                    name = std::move(parts[0]);
                    return true;
                }
                return false;
            }

            bool SplitNameWithoutExtFromPath(string_t& name, CFSTR const path) {
                std::vector<string_t> parts;
                if (SplitPath(parts, false, true, false, false, path)) {
                    name = std::move(parts[0]);
                    return true;
                }
                return false;
            }

            bool SplitExtensionFromPath(string_t& extension, CFSTR path) {
                std::vector<string_t> parts;
                if (SplitPath(parts, false, false, false, true, path)) {
                    extension = std::move(parts[0]);
                    return true;
                }
                return false;
            }

            bool SplitNameExtFromPath(string_t& name, string_t& ext, CFSTR path) {
                std::vector<string_t> parts;
                if (SplitPath(parts, false, true, false, true, path)) {
                    name = std::move(parts[0]);
                    ext = std::move(parts[1]);
                    return true;
                }
                return false;
            }

            bool SplitFolderNameWithExtFromPath(string_t& folder, string_t& name, CFSTR const path) {
                std::vector<string_t> parts;
                if (SplitPath(parts, true, true, true, false, path)) {
                    folder = std::move(parts[0]);
                    name = std::move(parts[1]);
                    return true;
                }
                return false;
            }

            bool SplitFolderNameWithoutExtFromPath(string_t& folder, string_t& name, CFSTR const path) {
                std::vector<string_t> parts;
                if (SplitPath(parts, true, true, false, false, path)) {
                    folder = std::move(parts[0]);
                    name = std::move(parts[1]);
                    return true;
                }
                return false;
            }

            bool SplitFolderNameExtFromPath(string_t& folder, string_t& name, string_t& ext, CFSTR const path) {
                std::vector<string_t> parts;
                if (SplitPath(parts, true, true, false, true, path)) {
                    folder = std::move(parts[0]);
                    name = std::move(parts[1]);
                    ext = std::move(parts[2]);
                    return true;
                }
                return false;
            }

            void NormalizePathSeparator(string_t& path) {
                for (auto &c : path) {
                    if (IS_SEPAR(c)) {
                        c = PATH_SEPARATOR;
                    }
                }
            }
        }
    }
}
