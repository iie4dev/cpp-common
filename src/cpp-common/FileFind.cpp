#include "cpp-common/FileFind.h"
#include "cpp-common/FileSystemUtils.h"
#include "cpp-common/FileName.h"
#include "cpp-common/FileDir.h"

#include <cpp-common/logger.h>

namespace np_limo
{
    namespace file
    {
        namespace find
        {
        #define CLEAR_FILETIME(ft) (ft).dwLowDateTime = (ft).dwHighDateTime = 0;

            static void ConvertWin32FindDataToFileInfo(WIN32_FIND_DATA const&fd, CFileInfo &fi);

        #pragma region CFileInfoBase Implementation
            void CFileInfoBase::ClearBase() {
                Size = 0;
                CLEAR_FILETIME(CTime);
                CLEAR_FILETIME(ATime);
                CLEAR_FILETIME(MTime);
                Attrib = 0;
                IsAltStream = false;
                IsDevice = false;
            }

            void CFileInfoBase::SetAsDir() { Attrib = FILE_ATTRIBUTE_DIRECTORY; }

            bool CFileInfoBase::IsArchived() const { return MatchesMask(FILE_ATTRIBUTE_ARCHIVE); }

            bool CFileInfoBase::IsCompressed() const { return MatchesMask(FILE_ATTRIBUTE_COMPRESSED); }

            bool CFileInfoBase::IsDir() const { return MatchesMask(FILE_ATTRIBUTE_DIRECTORY); }

            bool CFileInfoBase::IsEncrypted() const { return MatchesMask(FILE_ATTRIBUTE_ENCRYPTED); }

            bool CFileInfoBase::IsHidden() const { return MatchesMask(FILE_ATTRIBUTE_HIDDEN); }

            bool CFileInfoBase::IsNormal() const { return MatchesMask(FILE_ATTRIBUTE_NORMAL); }

            bool CFileInfoBase::IsOffline() const { return MatchesMask(FILE_ATTRIBUTE_OFFLINE); }

            bool CFileInfoBase::IsReadOnly() const { return MatchesMask(FILE_ATTRIBUTE_READONLY); }

            bool CFileInfoBase::HasReParsePoint() const { return MatchesMask(FILE_ATTRIBUTE_REPARSE_POINT); }

            bool CFileInfoBase::IsSparse() const { return MatchesMask(FILE_ATTRIBUTE_SPARSE_FILE); }

            bool CFileInfoBase::IsSystem() const { return MatchesMask(FILE_ATTRIBUTE_SYSTEM); }

            bool CFileInfoBase::IsTemporary() const { return MatchesMask(FILE_ATTRIBUTE_TEMPORARY); }
        #pragma endregion 

        #pragma region CFileInfo Implementation
            bool CFileInfo::IsDots() const {
                if (!IsDir() || Name.empty())
                    return false;
                if ('.' != Name[0])
                    return false;
                return 1 == Name.size() || (2 == Name.size() && '.' == Name[1]);
            }

            bool CFileInfo::Find(CFSTR path) {
                if (name::IsDevicePath(path)) {  // path is "\\.\~"
                    ClearBase();
                    Name.assign(path + name::kDevicePathPrefixSize);
                    IsDevice = true;

                    if (name::IsDrivePath2(Name.c_str()) && 2 == Name.size()) {  // path is "\\.\C:~"
                        char_t drive[] = {
                            Name.front(),  // drive letter
                            ':', '\\', 0
                        };
                        UINT64 clusterSize, totalSize, freeSize;
                        if (system::get_disk_info(drive, clusterSize, totalSize, freeSize)) {
                            Size = totalSize;
                            return true;
                        }
                    }

                    // todo: try with NIO::CInFile
                }

                // todo: try for stream file
                CFindFile finder;

                unsigned rootSize = 0;
                if (name::IsSuperPath(path))
                    rootSize = name::kSuperPathPrefixSize;

                if (name::IsDrivePath(path + rootSize) && 0 == path[rootSize + 3]) {  // path is "C:\"
                    auto const attr = GetFileAttrib(path);
                    if (INVALID_FILE_ATTRIBUTES != attr && 0 != (attr & FILE_ATTRIBUTE_DIRECTORY)) {
                        ClearBase();
                        Attrib = attr;
                        Name.assign(path, rootSize, 2); // we don't need backslash (C:)
                        return true;
                    }
                } else if (IS_PATH_SEPARATOR(path[0])) {
                    if (0 == path[1]) {  // path is "\", like a root in an unix-like os
                        auto const attr = GetFileAttrib(path);
                        if (INVALID_FILE_ATTRIBUTES != attr && 0 != (attr & FILE_ATTRIBUTE_DIRECTORY)) {
                            ClearBase();
                            Attrib = attr;
                            Name.clear(); // we don't need backslash (C:)
                            return true;
                        }
                    } else {
                        auto const prefixSize = name::GetNetworkServerPrefixSize(path);
                        if (prefixSize > 0 && 0 != path[prefixSize]) { // path is "\\server\~" or "\\?\UNC\server\~"
                            if (name::FindSepar(path + prefixSize) < 0) {
                                // path is "\\server\root" or "\\?\UNC\server\root"
                                
                                string_t wildcard = path;
                                wildcard.push_back(PATH_SEPARATOR);
                                wildcard.append(TEXT("*"));

                                // make sure current path is a valid dir by checking if there is 
                                // any files under current path
                                auto currPathIsDir = false;
                                if (finder.FindFirst(wildcard.c_str(), *this)) {
                                    if (TEXT(".") == Name) {
                                        Name.assign(path + prefixSize);
                                        return true;
                                    }
                                    currPathIsDir = true;
                                }

                                // if "\\server\share" maps to root folder "d:\", there is no "." item.
                                // but it's possible that there are another items
                                auto const attr = GetFileAttrib(path);
                                if (currPathIsDir || (INVALID_FILE_ATTRIBUTES != attr && 0 != (attr & FILE_ATTRIBUTE_DIRECTORY))) {
                                    ClearBase();
                                    if (attr != INVALID_FILE_ATTRIBUTES)
                                        Attrib = attr;
                                    else
                                        SetAsDir();
                                    Name.assign(path + prefixSize);
                                    return true;
                                }
                            }
                        }
                    }
                }

                return finder.FindFirst(path, *this);
            }
        #pragma endregion 

        #pragma region CFindFileBase Implementation
            CFindFileBase::CFindFileBase(const CFindFileBase& other) = default;

            CFindFileBase::CFindFileBase(CFindFileBase&& other) : handle_(other.handle_) {}

            CFindFileBase& CFindFileBase::operator=(const CFindFileBase& other) {
                if (this == &other)
                    return *this;
                handle_ = other.handle_;
                return *this;
            }

            CFindFileBase& CFindFileBase::operator=(CFindFileBase&& other) {
                if (this == &other)
                    return *this;
                handle_ = other.handle_;
                return *this;
            }

            bool CFindFileBase::Close() {
                if (INVALID_HANDLE_VALUE == handle_)
                    return true;
                if (!::FindClose(handle_))
                    return false;
                handle_ = INVALID_HANDLE_VALUE;
                return true;
            }
        #pragma endregion 

        #pragma region CFindFile
            /**
             * \brief Find the first item that match <wildcard>. NOTE: <wildcard> **MUST NOT**
             * end with a path separator !!!
             */
            bool CFindFile::FindFirst(CFSTR const wildcard, CFileInfo& fileInfo) {
                if (!Close()) {
                    return false;
                }

//                log::trace(kGlobalLogger, "start to find first file by '{}'", wildcard);
                WIN32_FIND_DATA fd;
                __try {
                    handle_ = ::FindFirstFile(wildcard, &fd);
                } __except (EXCEPTION_EXECUTE_HANDLER) {
                    // std::cerr << "" << std::endl;
                    return false;
                }
                auto const err_code = GetLastError();
                if (INVALID_HANDLE_VALUE == handle_ || nullptr == handle_ 
                    // || ERROR_SUCCESS != GetLastError()
                    ) {
                    return false;
                }
                ConvertWin32FindDataToFileInfo(fd, fileInfo);
                return true;
            }

            bool CFindFile::FindNext(CFileInfo& fileInfo) const {
                WIN32_FIND_DATA fd;
                BOOL ret;
                __try {
                    ret = ::FindNextFile(handle_, &fd);
                } __except (EXCEPTION_EXECUTE_HANDLER) {
                    // std::cerr << "" << std::endl;
                    return false;
                }
                auto const err_code = GetLastError();
                if (FALSE == ret 
                    // || ERROR_SUCCESS != err_code
                    ) {
                    log::trace(kGlobalLogger, "err in find next: '{}'", err_code);
                    return false;
                }
                ConvertWin32FindDataToFileInfo(fd, fileInfo);
                return true;
            }
        #pragma endregion 

            bool DoesFileExist(CFSTR const name) {
                CFileInfo fi;
                return fi.Find(name) && !fi.IsDir();
            }

            /**
             * \brief Check if the dir named <name> exists. NOTE: <name> **MUST NOT**
             * ends with path separator *unless* is the root path
             */
            bool DoesDirExist(CFSTR const name) {
                CFileInfo fi;
                return fi.Find(name) && fi.IsDir();
            }

            /**
             * \brief Check if the dir named <name> exists. NOTE: <name> **MUST NOT**
             * ends with path separator *unless* is the root path
             */
            bool DoesFileOrDirExist(CFSTR const name) {
                CFileInfo fi;
                return fi.Find(name);
            }

            void CEnumerator::SetDirPrefix(string_t const& dirPrefix) {
                wildcard_ = dirPrefix;
                wildcard_ += '*';
            }

            bool CEnumerator::Next(CFileInfo& fileInfo) {
                auto debug_miss_time = 100;
                while (true) {
                    log::trace(kGlobalLogger, "CEnumerator find next");
                    if (!NextAny(fileInfo)) {
                        log::trace(kGlobalLogger, "no more files");
                        return false;
                    }
//                    log::trace(kGlobalLogger, "has found {}", fileInfo.Name);
                    if (!fileInfo.IsDots())
                        return true;
                    if (--debug_miss_time < 0) {
                        log::trace(kGlobalLogger, "miss too much in next!!!");
                        return false;
                    }
                }
            }

            bool CEnumerator::Next(CFileInfo& fileInfo, bool& found) {
                if (Next(fileInfo))
                    return found = true;
                found = false;
                return ERROR_SUCCESS == ::GetLastError();
            }

            bool CEnumerator::NextAny(CFileInfo& fileInfo) {
                if (find_file_.IsHandleAllocated()) {
                    log::trace(kGlobalLogger, "Next any find next");
                    return find_file_.FindNext(fileInfo);
                }
                log::trace(kGlobalLogger, "Next any find first");
                return find_file_.FindFirst(wildcard_.c_str(), fileInfo);
            }

            DWORD GetFileAttrib(CFSTR const path) {
                auto ret = INVALID_FILE_ATTRIBUTES;
                dir::TryUsePath(path, [&ret](CFSTR const path)->bool {
                    ret = ::GetFileAttributes(path);
                    return INVALID_FILE_ATTRIBUTES != ret;
                });
                return ret;
            }

            bool SetFileAttrib(CFSTR const path, DWORD const attrib) {
                auto ret = false;
                dir::TryUsePath(path, [&ret, &attrib](CFSTR const path)->bool {
                    return ret = (TRUE == ::SetFileAttributes(path, attrib));
                });
                return ret;
            }

            static void ConvertWin32FindDataToFileInfo(const WIN32_FIND_DATA &fd, CFileInfo &fi) {
                fi.Attrib = fd.dwFileAttributes;
                fi.CTime = fd.ftCreationTime;
                fi.ATime = fd.ftLastAccessTime;
                fi.MTime = fd.ftLastWriteTime;
                fi.Size = (static_cast<UINT64>(fd.nFileSizeHigh) << 32) + fd.nFileSizeLow;
                fi.IsAltStream = false;
                fi.IsDevice = false;
                fi.Name = fd.cFileName;
            }

        }
    }
}
