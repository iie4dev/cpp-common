﻿#pragma once

#include <Windows.h>
#include <string>

#include <cpp-common-config.h>

EXPORT_LIB extern wchar_t temp_dir[MAX_PATH];

EXPORT_LIB extern std::wstring GetATempFileName();
EXPORT_LIB extern std::wstring GetTmpDir();

class EXPORT_LIB TempFileGuard
{
public:
	TempFileGuard(const std::wstring& suffix, bool create_file = false);
	TempFileGuard(const wchar_t* suffix = nullptr, bool create_file = false);

	TempFileGuard(const TempFileGuard&) = default;
	TempFileGuard(TempFileGuard&& first);

	~TempFileGuard();

	std::wstring& getTmpFilePath();
	static TempFileGuard guardExistFile(const std::wstring& path);

private:
	std::wstring tmp_file_path_;
	mutable bool need_delete_;
};
