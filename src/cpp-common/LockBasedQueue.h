//
// Created by limo on 2018/8/9.
//

#ifndef FILE_CHECK_LOCKBASEDQUEUE_H
#define FILE_CHECK_LOCKBASEDQUEUE_H

#include <memory>
#include <queue>
#include <mutex>
#include <functional>

#include "ThreadSafeQueueInterface.h"

template <typename T>
class LockBasedQueue: public ThreadSafeQueueInterface<T> {
	struct Node {
		std::shared_ptr<T> p_data;
		std::unique_ptr<Node> p_next;
	};

public:
	LockBasedQueue() : mp_head(new Node()), mp_tail(mp_head.get()) {}

	~LockBasedQueue() {
	}

	void enque(T data) override {
		auto p_data = std::make_shared<T>(std::move(data));
        enque(std::move(p_data));
	}

	void enque(std::shared_ptr<T> p_data) override {
		std::unique_ptr<Node> p_new_tail(new Node());

		{
			std::lock_guard<std::mutex> tail_lck(m_tail_mtx);
			mp_tail->p_data = std::move(p_data);
			Node *const p_raw_new_tail = p_new_tail.get();
			mp_tail->p_next = std::move(p_new_tail);
			mp_tail = p_raw_new_tail;
		}

		// m_data_cv.notify_one();
	}

	std::shared_ptr<T> deque() override {
		auto const old_head = try_pop_head();
		return old_head ? old_head->p_data : nullptr;
	}

	bool deque(T& data) override {
		return try_pop_head(data);
	}

	std::shared_ptr<T> wait_and_deque() override {
		auto const old_head = wait_pop_head();
		return old_head->p_data;
	}

	void wait_and_deque(T& data) override {
        wait_pop_head(data);
	}

	bool empty() const override {
		std::lock_guard<std::mutex> head_lck(m_head_mtx);
		return (mp_head.get() == get_tail());
	}

    void filter(std::function<bool(bool&, std::shared_ptr<T> const&)> const& check) {
        std::lock_guard<std::mutex> head_lck(m_head_mtx);
        std::lock_guard<std::mutex> tail_lck(m_tail_mtx);

        auto p_new_que_head = std::make_unique<Node>();
        auto p_new_que_tail = p_new_que_head.get();

        // if continue to check or not; false for checking each item; true for adding
        // item into new queue directly without checking
        auto cont = true;

        while (mp_head.get() != mp_tail) {
            auto p_old_head = std::move(mp_head);
            mp_head = std::move(p_old_head->p_next);
            
            if (!cont || check(cont, p_old_head->p_data)) {
                auto p_new_tail = std::make_unique<Node>();
                p_new_que_tail->p_data = std::move(p_old_head->p_data);
                auto *const p_raw_new_tail = p_new_tail.get();
                p_new_que_tail->p_next = std::move(p_new_tail);
                p_new_que_tail = p_raw_new_tail;
            }
        }

        mp_head = std::move(p_new_que_head);
        mp_tail = p_new_que_tail;
	}

private:
	Node *get_tail() const {
		std::lock_guard<std::mutex> tail_lck(m_tail_mtx);
		return mp_tail;
	}

	std::unique_ptr<Node> pop_head() {
		std::unique_ptr<Node> p_old_head = std::move(mp_head);
		mp_head = std::move(p_old_head->p_next);
		return p_old_head;
	}

	std::unique_lock<std::mutex> wait_for_data() {
		std::unique_lock<std::mutex> head_lck(m_head_mtx);
		m_data_cv.wait(head_lck, [&] { return mp_head.get() != get_tail(); });
		return std::move(head_lck);
	}

	std::unique_ptr<Node> wait_pop_head() {
		auto head_lck(wait_for_data());
		return pop_head();
	}

	void wait_pop_head(T& data) {
		auto head_lck(wait_for_data());
		data = std::move(*mp_head->p_data);
        pop_head();
	}

	std::unique_ptr<Node> try_pop_head() {
		std::lock_guard <std::mutex> head_lck(m_head_mtx);
		if (mp_head.get() == get_tail()) {
			return std::unique_ptr<Node>();
		}
		return pop_head();
	}

	bool try_pop_head(T& data) {
		std::lock_guard<std::mutex> head_lck(m_head_mtx);
		if (mp_head.get() == get_tail())
			return false;
		data = std::move(*mp_head->p_data);
		pop_head();
		return true;
	}

	mutable std::mutex m_head_mtx;
	mutable std::mutex m_tail_mtx;
	std::unique_ptr<Node> mp_head;
	Node* mp_tail;
	
	std::condition_variable m_data_cv;

};

#endif