//
// Created by limo on 8/21/2018.
//

#include <cpp-common/FileType.h>

#include <string>
#include <functional>
#include <algorithm>
#include <map>
#include <regex>
#include <set>

#include <cpp-common/FileName.h>

std::map<string_t, EFileFormat> kExtToTypeMap = {
    { TEXT(""), RAW },
    { TEXT("txt"), TXT },{ TEXT("htm"), HTM },{ TEXT("html"), HTML },{TEXT("xml"), XML},
    { TEXT("pdf"), PDF },
    { TEXT("doc"), DOC },{ TEXT("docx"), DOCX },
    { TEXT("xls"), XLS },{ TEXT("xlsx"), XLSX },
    { TEXT("ppt"), PPT },{ TEXT("pptx"), PPTX },
    { TEXT("et"), ET },{ TEXT("pps"), PPS },{ TEXT("rtf"), RTF },{ TEXT("dps"), DPS },{ TEXT("wps"), WPS },
    { TEXT("png"), PNG },{ TEXT("jpg"), JPG },{ TEXT("jpeg"), JPG },{ TEXT("bmp"), BMP },{ TEXT("gif"), GIF },
    { TEXT("dib"), DIB },{ TEXT("tif"), TIF },{ TEXT("tiff"), TIF },
    { TEXT("zip"), ZIP },{ TEXT("rar"), RAR },
    { TEXT("gz"), GZIP },{ TEXT("gzip"), GZIP },
    { TEXT("tar"), TAR },{ TEXT("7z"), Z7 },{ TEXT("tgz"), TGZ }
};

std::set<EFileFormat> kPictureTypeSet = {
    PNG, JPG, BMP, GIF, DIB, TIF
};

std::set<EFileFormat> kCompressedTypeSet = {
    ZIP, RAR, GZIP, TAR, Z7, TGZ
};

std::set<EFileFormat> kOfficeTypeSet = {
    DOC, DOCX, XLS, XLSX, PPT, PPTX, ET, PPS, RTF, DPS, WPS
};

string_t format_to_ext(EFileFormat const format, string_t const& default_ext) {
    for (auto const &ext_format : kExtToTypeMap) {
        if (format == ext_format.second)
            return ext_format.first;
    }
    return default_ext;
}

EFileFormat ext_to_format(string_t const& ext, EFileFormat const default_format) {
    auto const itr = kExtToTypeMap.find(ext);
    return itr == kExtToTypeMap.end() ? default_format : itr->second;
}

std::set<string_t> format_to_ext(std::set<EFileFormat> const& formats) {
    std::set<string_t> ext_set;
    for (auto const& format : formats)
        ext_set.insert(format_to_ext(format));
    return ext_set;
}

std::set<EFileFormat> ext_to_format(std::set<string_t> const& ext_set) {
    std::set<EFileFormat> formats;
    for (auto const& ext : ext_set)
        formats.emplace(ext_to_format(ext));
    return formats;
}

EFileFormat get_file_format(string_t const& path) {
    string_t suffix;
    np_limo::file::name::SplitExtensionFromPath(suffix, path.c_str());
    std::transform(suffix.begin(), suffix.end(), suffix.begin(), ::tolower);
    return ext_to_format(suffix);
}

bool is_picture(EFileFormat const& fmt) {
    return 0 != kPictureTypeSet.count(fmt);
}

bool is_compressed(EFileFormat const& fmt) {
    return 0 != kCompressedTypeSet.count(fmt);
}

bool is_office(EFileFormat const& fmt) {
    return 0 != kOfficeTypeSet.count(fmt);
}

std::string format_to_ext_utf(EFileFormat format, const std::string &default_ext) {
    for (auto const &ext_format : kExtToTypeMap) {
        if (format == ext_format.second) {
            auto ext = std::string();
            np_limo::str::ConvertAppToUtf(ext, ext_format.first.c_str(), ext_format.first.size());
            return ext;
        }
    }
    return default_ext;
}

std::set<std::string> format_to_ext_utf(const std::set<EFileFormat> &formats) {
    std::set<std::string> ext_set;
    for (auto const& format : formats)
        ext_set.insert(format_to_ext_utf(format));
    return ext_set;
}

EFileFormat ext_utf_to_format(const std::string &ext, EFileFormat default_format) {
    auto ext_app = string_t();
    np_limo::str::ConvertUtfToApp(ext_app, ext.c_str(), ext.size());
    return ext_to_format(ext_app, default_format);
}

std::set<EFileFormat> ext_utf_to_format(const std::set<std::string> &ext_set) {
    std::set<EFileFormat> formats;
    for (auto const& ext : ext_set)
        formats.emplace(ext_utf_to_format(ext));
    return formats;
}
