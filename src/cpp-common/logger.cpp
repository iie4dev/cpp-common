//
// Created by limo on 2018/8/11.
//

#include "logger.h"

#include <vector>
#include <memory>

#include "spdlog/sinks/stdout_color_sinks.h" // or "../stdout_sinks.h" if no colors needed
#include "spdlog/sinks/daily_file_sink.h" // or "../stdout_sinks.h" if no colors needed

std::string kGlobalLogger = "Logger";

bool g_log_enabled = false;

namespace np_limo {

    namespace log {

        bool initialize(std::string const& log_name,
                        bool const need_log_to_console,
                        bool const need_log_to_file, std::string const & log_file,
                        log_level_e const log_level) {

            kGlobalLogger = log_name;

            g_log_enabled = (need_log_to_console || need_log_to_file);
            if (!g_log_enabled) return true;

            std::vector<std::shared_ptr<spdlog::sinks::sink>> sinks;
            if (need_log_to_console) {
                sinks.push_back(std::make_shared<spdlog::sinks::stderr_color_sink_mt>());
            }
            if (need_log_to_file) {
                sinks.push_back(std::make_shared<spdlog::sinks::daily_file_sink_mt>(log_file, 23, 59));
            }

            auto const logger = std::make_shared<spdlog::logger>(kGlobalLogger, sinks.begin(), sinks.end());
            logger->set_level(log_level);
            register_logger(logger);
            
            return true;
        }

    }

}
