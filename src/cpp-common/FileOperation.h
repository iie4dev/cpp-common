//
// Created by limo on 8/21/2018.
//

#ifndef FILE_CHECK_FILEOPERATION_H
#define FILE_CHECK_FILEOPERATION_H


#include <string>

#include <cpp-common/StringUtils.h>


enum EFileFormat {
    RAW,
    TXT, HTM, HTML,
    PDF,
    DOC, DOCX, XLS, XLSX, PPT, PPTX, ET, PPS, RTF, DPS,
    PNG, JPG, BMP, GIF, DIB, TIF,
    ZIP, RAR, GZIP, TAR, Z7, TGZ,
};

EFileFormat get_file_format(string_t const& path);

bool is_picture(EFileFormat const& fmt);

bool is_compressed(EFileFormat const& fmt);

#endif //FILE_CHECK_FILEOPERATION_H
