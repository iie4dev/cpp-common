//
// Created by limo on 2018/7/26.
//

#ifndef FILE_CHECK_NONCOPYABLE_H
#define FILE_CHECK_NONCOPYABLE_H


class NonCopyable {
protected:
    NonCopyable() = default;
    virtual ~NonCopyable() = default;

public:
    NonCopyable(const NonCopyable&) = delete;
    const NonCopyable& operator=(const NonCopyable&) = delete;

};


#endif //FILE_CHECK_NONCOPYABLE_H
