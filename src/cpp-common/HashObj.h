//
// Created by limo on 9/30/19.
//

#ifndef SINKER_HASHOBJ_H
#define SINKER_HASHOBJ_H

#include <string>
#include <vector>
#include <fstream>

#include <cpp-common-config.h>

#include "picosha2.h"

static const size_t BUFFER_SIZE = PICOSHA2_BUFFER_SIZE_FOR_INPUT_ITERATOR;

class EXPORT_LIB HashObj {
    using byte = unsigned char;
    using bytes_t = std::basic_string<byte>;

public:
    explicit HashObj(size_t const buff_size=BUFFER_SIZE) : buff_size_(buff_size) {}

    HashObj& append_hash(std::istream &bytes);
    HashObj& hash(std::istream &bytes);

    template <typename InIter>
    HashObj& append_hash(InIter first, InIter last) {
        return append_hash(first, last,
                typename std::iterator_traits<InIter>::iterator_category());
    }

    template <typename InIter>
    HashObj& hash(InIter first, InIter last) {
        clear();
        return append_hash(first, last);
    }

    void finish() {
        hasher_.finish();
    }

    void clear() {
        hasher_ = picosha2::hash256_one_by_one();
    }

    bytes_t to_bytes() const;
    std::string to_hex_string() const;

private:
    template <typename RaIter>
    HashObj& append_hash(RaIter first, RaIter last, std::random_access_iterator_tag) {
        hasher_.process(first, last);
        return *this;
    }

    template <typename RaIter>
    HashObj& append_hash(RaIter first, RaIter last, std::input_iterator_tag) {
        bytes_t buff(buff_size_, 0);
        size_t read_size = 0;
        while (last != first) {
            for (read_size = 0; read_size < buff_size_; ++first) {
                if (last == first) break;
                buff[read_size++] = *first;
            }
        }
        hasher_.process(buff.begin(), buff.end());
        return *this;
    }

    size_t buff_size_;
    picosha2::hash256_one_by_one hasher_;

};


#endif //SINKER_HASHOBJ_H
