#pragma once

#include <Windows.h>

using hb_time_point_t = DWORD;

static
hb_time_point_t hb_get_curr_time() {
    return GetTickCount();
}
