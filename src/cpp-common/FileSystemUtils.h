#pragma once

#include <unordered_map>

#include <Windows.h>

#include <cpp-common-config.h>

#include "StringUtils.h"
#include "cpp-common/DefBase.h"

namespace np_limo
{
    namespace file
    {
        namespace system
        {
            enum EDataUnit {
                kByte, kKByte, kMByte, kGByte, kTByte,
                kPByte, kEByte, kZByte, kYByte, kBByte,
                kNByte, kDByte, kCByte, kXByte
            }; 
        
            EXPORT_LIB inline std::vector<EDataUnit> InitAllDataUnitList() {
                return std::vector<EDataUnit> { 
                    kByte, kKByte, kMByte, kGByte, kTByte,
                    kPByte, kEByte, kZByte, kYByte, kBByte, 
                    kNByte, kDByte, kCByte, kXByte 
                };
            }

            EXPORT_LIB extern const std::vector<EDataUnit> gAllDataUnit;
            EXPORT_LIB extern const std::vector<CFSTR> gAllDataUnitName;
            EXPORT_LIB extern const size_t gDataUnitSize;
            EXPORT_LIB extern const std::unordered_map<EDataUnit, CFSTR> gDataUnitNameMap;;

            EXPORT_LIB UINT64 get_disk_free_size(CFSTR disk);

            EXPORT_LIB bool get_volume_info(CFSTR root,
                                 string_t & volume_name, string_t & file_system_name,
                                 DWORD& volume_serial_number, 
                                 DWORD& max_component_length, 
                                 DWORD& file_system_flags);

            EXPORT_LIB bool get_disk_info(CFSTR disk, UINT64 &cluster_size, UINT64 &total_size, UINT64 &free_size);

            EXPORT_LIB bool format_file_size(string_t &formatted, UINT64 file_size_in_bytes);
            EXPORT_LIB bool format_file_size_in_units(double &num, EDataUnit &unit, UINT64 file_size_in_bytes);
        }
    }
}
