//
// Created by limo on 8/21/2018.
//

#ifndef FILE_CHECK_FILEOPERATION_H
#define FILE_CHECK_FILEOPERATION_H


#include <set>
#include <string>

#include <cpp-common-config.h>

#include <cpp-common/StringUtils.h>


enum EFileFormat {
    RAW,
    TXT, HTM, HTML, XML,
    PDF,
    DOC, DOCX, XLS, XLSX, PPT, PPTX, ET, PPS, RTF, DPS, WPS,
    PNG, JPG, BMP, GIF, DIB, TIF,
    ZIP, RAR, GZIP, TAR, Z7, TGZ,
};

EXPORT_LIB string_t format_to_ext(EFileFormat format, string_t const& default_ext = TEXT(""));
EXPORT_LIB std::string format_to_ext_utf(EFileFormat format, std::string const& default_ext = "");
EXPORT_LIB EFileFormat ext_to_format(string_t const& ext, EFileFormat default_format = RAW);
EXPORT_LIB EFileFormat ext_utf_to_format(std::string const& ext, EFileFormat default_format = RAW);
EXPORT_LIB std::set<string_t> format_to_ext(std::set<EFileFormat> const& formats);
EXPORT_LIB std::set<std::string> format_to_ext_utf(std::set<EFileFormat> const& formats);
EXPORT_LIB std::set<EFileFormat> ext_to_format(std::set<string_t> const& ext_set);
EXPORT_LIB std::set<EFileFormat> ext_utf_to_format(std::set<std::string> const& ext_set);

EXPORT_LIB EFileFormat get_file_format(string_t const& path);

EXPORT_LIB bool is_picture(EFileFormat const& fmt);
EXPORT_LIB bool is_compressed(EFileFormat const& fmt);
EXPORT_LIB bool is_office(EFileFormat const& fmt);

#endif //FILE_CHECK_FILEOPERATION_H
