#pragma once

#include <zmq/zmq.hpp>

#include <cpp-common-config.h>

EXPORT_LIB unsigned auto_bind_locally(zmq::socket_t &socket);