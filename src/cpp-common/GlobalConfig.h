//
// Created by limo on 8/19/2018.
//

#ifndef FILE_CHECK_GLOBALCONFIG_H
#define FILE_CHECK_GLOBALCONFIG_H

#include <utility>

#include <cpp-common/Singleton.h>
#include <cpp-common/FileName.h>
#include <cpp-common/FileDir.h>
#include <cpp-common/FileFind.h>
#include <cpp-common/Serializable.h>

#include <cpp-common/tools/DecompressArchive.h>
#include <cpp-common/logger.h>
#include "NetworkTools.h"


using np_limo::file::name::JoinPath;

#define APP_NAME TEXT(R"(FileCheckV2.0)")
#define LOG_FOLDER_NAME TEXT("log")
#define CONFIG_FOLDER_NAME TEXT("config")

#define MAIN_CONFIG_FILE_NAME "config.json"
#define GLOBAL_CONFIG_FILE_NAME TEXT(R"(global-config.json)")
#define TOOLS_ARCHIVE_FILE_NAME "tools.7z"

#define DEFAULT_MAIN_CONFIG_PATH ".\\config.json"

#define DEFAULT_PATH_TOOLS_ARCHIVE   "./tools.7z"
#define DEFAULT_FOLDER_TOOLS         "!/tools"

#define DEFAULT_EXE_DAEMON      "!/tools/daemon.exe"
#define DEFAULT_EXE_QUEUER      "!/tools/queuer.exe"
#define DEFAULT_EXE_MASTER      "!/tools/master.exe"
#define DEFAULT_EXE_WORKER      "!/tools/worker.exe"
#define DEFAULT_EXE_SINKER      "!/tools/sinker.exe"

#define DEFAULT_EXE_7Z          "./7z.exe"
#define DEFAULT_EXE_TESSERACT   "!/tools/tesseract.exe"
#define DEFAULT_DIR_TESSERACT   "!/tools"
#define DEFAULT_DIR_RESULT_DB   "!/result.db"

#define DEFAULT_DIR_LOG         "!/log"
#define DEFAULT_DIR_CONFIG      "!/config"

#define VAR_NAME_TOOLS_ARCHIVE  "tools archive"
#define VAR_NAME_TOOLS_FOLDER   "tools folder"

#define VAR_NAME_QUEUER_EXE     "queuer exe"
#define VAR_NAME_WORKER_EXE     "worker exe"
#define VAR_NAME_SINKER_EXE     "sinker exe"

#define VAR_NAME_7Z_EXE         "7z exe"
#define VAR_NAME_TESS_EXE       "tess exe"
#define VAR_NAME_TESS_DIR       "tess dir"

#define VAR_NAME_LOG_DIR        "log dir"
#define VAR_NAME_CONFIG_DIR     "config dir"

#define TMP_DECOMPRESS_PREFIX TEXT("TMP_FC_DECOMPRESS")

//inline
//bool sys_to_utf(string_t& sys) {
//    return np_limo::str::ConvertSysToUtf(sys, sys.c_str(), sys.size());
//}
//
//inline
//bool utf_to_sys(string_t& utf) {
//    return np_limo::str::ConvertUtfToSys(utf, utf.c_str(), utf.size());
//}

inline std::string sys2utf(string_t const& str_app) {
    auto utf = std::string();
    np_limo::str::ConvertAppToUtf(utf, str_app.c_str(), str_app.size());
    return utf;
}

inline string_t utf2sys(std::string const& utf) {
    auto str_app = string_t();
    np_limo::str::ConvertUtfToApp(str_app, utf.c_str(), utf.size());
    return str_app;
}

struct LogConfig {
    bool log_to_console;
    bool log_to_file;
    np_limo::log::log_level_e log_level;

    template <class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(log_to_console),
                CEREAL_NVP(log_to_file),
                CEREAL_NVP(log_level)
        );
    }

};

struct ExeConfig {
    string_t path_exe;
    string_t path_config;
    bool show_in_new_window;

    template <class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(path_exe),
                CEREAL_NVP(path_config),
                CEREAL_NVP(show_in_new_window)
        );
    }

};

class BaseConfig : public Singleton<BaseConfig> {
    friend class MainConfigManager;
    friend class Singleton<BaseConfig>;
public:
    bool initialize() {
        return create_dirs();
    }

    string_t daemon_log_path() const {
        return string_t(log_dir_).append(STR_PATH_SEPARATOR).append(TEXT("daemon.log"));
    }

    string_t client_log_path() const {
        return string_t(log_dir_).append(STR_PATH_SEPARATOR).append(TEXT("client.log"));
    }

    string_t master_log_path(int const patch_id) const {
        return string_t(log_dir_).append(STR_PATH_SEPARATOR)
            .append(TEXT("master")).append(TO_STR(patch_id)).append(TEXT(".log"));
    }

    string_t queuer_log_path() const {
        return string_t(log_dir_).append(STR_PATH_SEPARATOR).append(TEXT("queuer.log"));
    }

    string_t worker_log_path(int const worker_id) const {
        return string_t(log_dir_).append(STR_PATH_SEPARATOR)
            .append(TEXT("worker")).append(TO_STR(worker_id)).append(TEXT(".log"));
    }

    string_t sinker_log_path() const {
        return string_t(log_dir_).append(STR_PATH_SEPARATOR).append(TEXT("sinker.log"));
    }

    string_t log_path() const {
        return string_t(log_dir_);
    }

    string_t global_config_path() const {
        return string_t(config_dir_).append(STR_PATH_SEPARATOR).append(GLOBAL_CONFIG_FILE_NAME);
    }

    string_t temp_decompress_folder(int const patch_id) const {
        string_t temp_dir;
        if (np_limo::file::dir::GetTempFolder(temp_dir)) {
            temp_dir.append(TMP_DECOMPRESS_PREFIX).append(TO_STR(patch_id));
        }
        return temp_dir;
    }

private:
    bool create_dirs() {
        string_t app_data_path;
        if (!np_limo::file::dir::GetLocalAppDataPath(app_data_path)) {
            return false;
        }

        log_dir_
            .assign(app_data_path)
            .append(STR_PATH_SEPARATOR)
            .append(APP_NAME)
            .append(STR_PATH_SEPARATOR)
            .append(LOG_FOLDER_NAME);
        config_dir_
            .assign(app_data_path)
            .append(STR_PATH_SEPARATOR)
            .append(APP_NAME)
            .append(STR_PATH_SEPARATOR)
            .append(CONFIG_FOLDER_NAME);

        return np_limo::file::dir::CreateDirRecurrently(log_dir_.c_str()) &&
            np_limo::file::dir::CreateDirRecurrently(config_dir_.c_str());
    }

    string_t log_dir_;
    string_t config_dir_;

};

class GlobalConfig : public Singleton<GlobalConfig> {
    friend class MainConfigManager;
    friend class Singleton<GlobalConfig>;
    friend class cereal::access;

    std::string break_channel_;
    std::string collect_channel_;
    std::string dispatch_channel_;
    std::string sink_channel_;
    std::string heartbeat_channel_;
    std::string status_channel_;

    std::string path_tools_archive_;
    std::string path_tools_archive_decompress_to_;

    std::string path_daemon_exe_;
    std::string path_master_exe_;
    std::string path_worker_exe_;
    std::string path_queuer_exe_;
    std::string path_sinker_exe_;

    std::string path_7z_exe_;
    std::string path_tess_exe_;
    std::string path_tess_dir_;

    std::string path_result_db_;

    LogConfig log_daemon_{};
    LogConfig log_master_{};
    LogConfig log_queuer_{};
    LogConfig log_worker_{};
    LogConfig log_sinker_{};

    long heartbeat_{ 1000 };

    long heartbeat_timeout_{ 10000 };

    /**
    * \brief there is a situation that master is breaking but no more files found, even
    * the files that will not be checked. it may happen when master goes into dead-lock.
    * so this time out is the max time that a master in idle
    */
    long master_idle_timeout_{ 40000 };

    long system_idle_timeout_{ 1500 };

    /**
    * \brief this is the max time that a worker can work on one task. if a task cost
    * more time than this, the worker most likely has been blocked, just kill it.
    */
    long worker_working_timeout_{ 180000 };

    unsigned worker_num_{ 0 };
    unsigned hash_len_{ 12 };

public:
    bool load(string_t const& config_path) {
        auto const succeed = load_json(config_path, *this);
//            && utf_to_sys(path_tools_archive_)
//            && utf_to_sys(path_tools_archive_decompress_to_)
//            && utf_to_sys(path_daemon_exe_)
//            && utf_to_sys(path_master_exe_)
//            && utf_to_sys(path_worker_exe_)
//            && utf_to_sys(path_queuer_exe_)
//            && utf_to_sys(path_sinker_exe_)
//            && utf_to_sys(path_7z_exe_)
//            && utf_to_sys(path_tess_exe_)
//            && utf_to_sys(path_tess_dir_)
//            && utf_to_sys(path_result_db_);
        return succeed;
    }

    bool dump(string_t const& config_path) {
//        auto const succeed =  sys_to_utf(path_tools_archive_)
//            && sys_to_utf(path_tools_archive_decompress_to_)
//            && sys_to_utf(path_daemon_exe_)
//            && sys_to_utf(path_master_exe_)
//            && sys_to_utf(path_worker_exe_)
//            && sys_to_utf(path_queuer_exe_)
//            && sys_to_utf(path_sinker_exe_)
//            && sys_to_utf(path_7z_exe_)
//            && sys_to_utf(path_tess_exe_)
//            && sys_to_utf(path_tess_dir_)
//            && sys_to_utf(path_result_db_);
//
//        if (!succeed) {
//            std::cout << "Failed to convert to utf" << std::endl;
//            return false;
//        }
        return dump_json(config_path, *this);
    }

    template <class Archive>
    void serialize(Archive & archive) {
        archive(CEREAL_NVP(break_channel_),
                CEREAL_NVP(collect_channel_),
                CEREAL_NVP(dispatch_channel_),
                CEREAL_NVP(sink_channel_),
                CEREAL_NVP(heartbeat_channel_),
                CEREAL_NVP(status_channel_),
                CEREAL_NVP(path_tools_archive_),
                CEREAL_NVP(path_tools_archive_decompress_to_),
                CEREAL_NVP(path_daemon_exe_),
                CEREAL_NVP(path_master_exe_),
                CEREAL_NVP(path_worker_exe_),
                CEREAL_NVP(path_queuer_exe_),
                CEREAL_NVP(path_sinker_exe_),
                CEREAL_NVP(path_7z_exe_),
                CEREAL_NVP(path_tess_exe_),
                CEREAL_NVP(path_tess_dir_),
                CEREAL_NVP(path_result_db_),
                CEREAL_NVP(log_daemon_),
                CEREAL_NVP(log_master_),
                CEREAL_NVP(log_queuer_),
                CEREAL_NVP(log_worker_),
                CEREAL_NVP(log_sinker_),
                CEREAL_NVP(heartbeat_),
                CEREAL_NVP(heartbeat_timeout_),
                CEREAL_NVP(master_idle_timeout_),
                CEREAL_NVP(system_idle_timeout_),
                CEREAL_NVP(worker_working_timeout_),
                CEREAL_NVP(worker_num_),
                CEREAL_NVP(hash_len_)
                );
    }


    std::string const& break_channel() const {
        return break_channel_;
    }

    std::string const& collect_channel() const {
        return collect_channel_;
    }

    std::string const& dispatch_channel() const {
        return dispatch_channel_;
    }

    std::string const& sink_channel() const {
        return sink_channel_;
    }

    std::string const& heartbeat_channel() const {
        return heartbeat_channel_;
    }

    std::string const& status_channel() const {
        return status_channel_;
    }

    string_t path_tools_archive() const {
        return utf2sys(path_tools_archive_);
    }

    string_t path_tools_archive_decompress_to() const {
        return utf2sys(path_tools_archive_decompress_to_);
    }

    string_t path_daemon_exe() const {
        return utf2sys(path_daemon_exe_);
    }

    string_t path_master_exe() const {
        return utf2sys(path_master_exe_);
    }

    string_t path_worker_exe() const {
        return utf2sys(path_worker_exe_);
    }

    string_t path_queuer_exe() const {
        return utf2sys(path_queuer_exe_);
    }

    string_t path_sinker_exe() const {
        return utf2sys(path_sinker_exe_);
    }

    string_t path_7z_exe() const {
        return utf2sys(path_7z_exe_);
    }

    string_t path_tess_exe() const {
        return utf2sys(path_tess_exe_);
    }

    string_t path_tess_dir() const {
        return utf2sys(path_tess_dir_);
    }

    string_t path_result_db() const {
        return utf2sys(path_result_db_);
    }

    LogConfig const& log_daemon() const {
        return log_daemon_;
    }

    LogConfig const& log_master() const {
        return log_master_;
    }

    LogConfig const& log_queuer() const {
        return log_queuer_;
    }

    LogConfig const& log_worker() const {
        return log_worker_;
    }

    LogConfig const& log_sinker() const {
        return log_sinker_;
    }

    long heartbeat() const {
        return heartbeat_;
    }

    long heartbeat_timeout() const {
        return heartbeat_timeout_;
    }

    long master_idle_timeout() const {
        return master_idle_timeout_;
    }

    long system_idle_timeout() const {
        return system_idle_timeout_;
    }

    long worker_working_timeout() const {
        return worker_working_timeout_;
    }

    unsigned worker_num() const {
        return worker_num_;
    }

    unsigned hash_len() const {
        return hash_len_;
    }

};

inline
bool find_file_or_dir(bool const is_dir, string_t const& path) {
    if (is_dir) return np_limo::file::find::DoesDirExist(path.c_str());
    return np_limo::file::find::DoesFileExist(path.c_str());
}

/**
 * \brief Make sure the file/dir with path of <sub_path> under <work_paths> exist, if the
 *        file/dir is found under any folder, replace the <sub_path> with <full-path>
 */
inline
bool find_under_work_paths(bool const is_dir, std::string& sub_path, std::vector<string_t> const& paths) {
    auto sub_path_sys = utf2sys(sub_path);
    if (find_file_or_dir(is_dir, sub_path_sys))
        return true;
    for (auto const &path : paths) {
        auto const full_path = JoinPath(path, sub_path_sys);
        if (find_file_or_dir(is_dir, full_path)) {
            sub_path = sys2utf(full_path);
            return true;
        }
    }

    return false;
}

class MainConfigManager {
public:
    explicit MainConfigManager(string_t config_path)
        : path_main_config_(std::move(config_path)) {
    }

    bool parse_and_dump(string_t const& to_path) {
        has_free_port = true;
        return load_and_resolve() &&
            dump_global_config(to_path);
    }

    bool has_free_port {};

private:
    bool load_and_resolve() {
        // init useful path
        np_limo::file::dir::GetHomeDir(path_home_);
        np_limo::file::dir::GetExeDir(path_exe_);
        np_limo::file::dir::GetLocalAppDataPath(path_app_);
        path_app_ = JoinPath(path_app_, APP_NAME);

        // load main config
        auto path_utf = sys2utf(path_main_config_);
        normalize_path(path_utf, DEFAULT_MAIN_CONFIG_PATH);
        path_main_config_ = utf2sys(path_utf);
        if (!load_json(path_main_config_, main_config_)) {
            return false;
        }
        // parse main config params
        normalize_paths();
        return make_sure_paths_exist();
    }

    bool dump_global_config(string_t const& to_path) {
        return main_config_.dump(to_path);
    }

    void normalize_paths() {
        normalize_path(main_config_.path_tools_archive_, DEFAULT_PATH_TOOLS_ARCHIVE);
        normalize_path(main_config_.path_tools_archive_decompress_to_, DEFAULT_FOLDER_TOOLS);

        normalize_path(main_config_.path_daemon_exe_, DEFAULT_EXE_DAEMON);
        normalize_path(main_config_.path_master_exe_, DEFAULT_EXE_MASTER);
        normalize_path(main_config_.path_worker_exe_, DEFAULT_EXE_WORKER);
        normalize_path(main_config_.path_queuer_exe_, DEFAULT_EXE_QUEUER);
        normalize_path(main_config_.path_sinker_exe_, DEFAULT_EXE_SINKER);

        normalize_path(main_config_.path_7z_exe_, DEFAULT_EXE_7Z);
        normalize_path(main_config_.path_tess_exe_, DEFAULT_EXE_TESSERACT);
        normalize_path(main_config_.path_tess_dir_, DEFAULT_DIR_TESSERACT);

        normalize_path(main_config_.path_result_db_, DEFAULT_DIR_RESULT_DB);
    }

    bool make_sure_ports_free() {
//        fixme: zmq socket would block during the initialization in some version of windows
//        auto const test_port_fetchable = std::function<bool()>([]() {
//            try {
//                zmq::context_t ctx(1);
//                zmq::socket_t skt(ctx, ZMQ_PULL);
//            } catch (...) {
//                return false;
//            }
//            return true;
//        });
//        if (!np_limo::thread::do_async(test_port_fetchable, has_free_port, 5000)) {
//            has_free_port = false;
//            return false;
//        }

        auto success = dispatch_free_port(main_config_.break_channel_);
        success = success && dispatch_free_port(main_config_.collect_channel_);
        success = success && dispatch_free_port(main_config_.dispatch_channel_);
        success = success && dispatch_free_port(main_config_.heartbeat_channel_);
        success = success && dispatch_free_port(main_config_.status_channel_);
        return success;
    }

    bool make_sure_paths_exist() {
        std::vector<string_t> const work_paths = {
            path_exe_, main_config_.path_tools_archive_decompress_to()
        };

        if (paths_exist_under_work_paths(work_paths)) {
            // has found all exe we want, no need to extract archives
            return true;
        }

        GlobalConfig::Instance()->path_7z_exe_ = main_config_.path_7z_exe_;
        // decompress tool archive if need. there will be some tools exe or data files
        if (!decompress_archive(main_config_.path_tools_archive(),
                                main_config_.path_tools_archive_decompress_to())) {
            return false;
        }

        std::cout << "Check if exist last time" << std::endl;
        // after decompressing, check paths again
        return paths_exist_under_work_paths(work_paths);
    }

    bool paths_exist_under_work_paths(std::vector<string_t> const& work_paths) {
        return find_under_work_paths(false, main_config_.path_daemon_exe_, work_paths)
            && find_under_work_paths(false, main_config_.path_master_exe_, work_paths)
            && find_under_work_paths(false, main_config_.path_worker_exe_, work_paths)
            && find_under_work_paths(false, main_config_.path_queuer_exe_, work_paths)
            && find_under_work_paths(false, main_config_.path_sinker_exe_, work_paths)
            && find_under_work_paths(false, main_config_.path_7z_exe_, work_paths)
            && find_under_work_paths(false, main_config_.path_tess_exe_, work_paths)
            && find_under_work_paths(true, main_config_.path_tess_dir_, work_paths);
    }

    /**
     * \brief Replace the '.', '~', '!' in the <path> with <exe_path>, <home_path> and <app_path>.
     *        If the <path> is empty, set it with default value
     */
    void normalize_path(std::string& path, std::string const& default_path) const {
        if (path.empty()) {
            // np_limo::log::info(kGlobalLogger,
                               // "MainConfigManager: param {} is empty, set as default value '{}'",
                               // path_name, default_path);
            path = default_path;
        }

        // replace prefix function
        auto const replace_prefix = [&](string_t const& prefix) {
            auto path_sys = utf2sys(path);
            if (1 == path.size()) {
                path_sys.assign(prefix);
            } else if (IS_PATH_SEPARATOR(path[1])) {
                path_sys = JoinPath(prefix, path_sys.substr(2));
                path = sys2utf(path_sys);
            }
        };

        // replace '.~!' prefix with a meaningful file prefix
        switch (path[0]) {
            case '.': replace_prefix(path_exe_); break;
            case '!': replace_prefix(path_app_); break;
            case '~': replace_prefix(path_home_); break;
            default: break;
        }

        // np_limo::log::trace(kGlobalLogger,
                            // "MainConfigManager: param {} is normalized as {}",
                            // path_name, path);
    }

    /*void normalize_log_path(string_t& name, string_t const& default_path) const {
        // resolve <name> if it is a relative path starts with ".~!"
        if (!name.empty())
            normalize_path(name, default_path);

        // join log_dir if <name> is a pure relative path who has no ".~!" or root prefix
        if (0 == np_limo::file::name::GetRootPrefixSize(name.c_str()))
            name = JoinPath(main_config_.path_log_dir_, name.empty() ? default_path : name);
    }*/

    string_t path_main_config_;

    GlobalConfig main_config_{};

    string_t path_exe_;
    string_t path_app_;
    string_t path_home_;

};


#endif //FILE_CHECK_GLOBALCONFIG_H
