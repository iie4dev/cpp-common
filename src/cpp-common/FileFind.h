#pragma once

#include <Windows.h>

#include "StringUtils.h"

#include <cpp-common-config.h>

namespace np_limo
{
    namespace file
    {
        namespace find
        {
            class EXPORT_LIB CFileInfoBase {
            public:
                UINT64 Size{};
                DWORD Attrib{};
                FILETIME CTime{};
                FILETIME ATime{};
                FILETIME MTime{};
                bool IsAltStream{};
                bool IsDevice{};

                CFileInfoBase() { ClearBase(); }
                void ClearBase();

                void SetAsDir();
                bool IsArchived() const;
                bool IsCompressed() const;
                bool IsDir() const;
                bool IsEncrypted() const;
                bool IsHidden() const;
                bool IsNormal() const;
                bool IsOffline() const;
                bool IsReadOnly() const;
                bool HasReParsePoint() const;
                bool IsSparse() const;
                bool IsSystem() const;
                bool IsTemporary() const;

            private:
                bool MatchesMask(UINT32 const mask) const { return 0 != (Attrib & mask); }

            };

            struct EXPORT_LIB CFileInfo : CFileInfoBase {
                bool IsDots() const;
                bool Find(CFSTR path);
                string_t Name;
            };

            class EXPORT_LIB CFindFileBase {
            protected:
                HANDLE handle_{};
            public:
                CFindFileBase() : handle_(INVALID_HANDLE_VALUE) {}
                virtual ~CFindFileBase() { Close(); }
                
                CFindFileBase(const CFindFileBase& other);
                CFindFileBase(CFindFileBase&& other);
                
                CFindFileBase& operator=(const CFindFileBase& other);
                CFindFileBase& operator=(CFindFileBase&& other);

                bool Close();
                bool IsHandleAllocated() const { return INVALID_HANDLE_VALUE != handle_ && nullptr != handle_; }
            };

            class EXPORT_LIB CFindFile : public CFindFileBase {
            public:
                bool FindFirst(CFSTR wildcard, CFileInfo &fileInfo);
                bool FindNext(CFileInfo &fileInfo) const;
            };

            EXPORT_LIB bool DoesFileExist(CFSTR name);
            EXPORT_LIB bool DoesDirExist(CFSTR name);
            EXPORT_LIB bool DoesFileOrDirExist(CFSTR name);

            EXPORT_LIB DWORD GetFileAttrib(CFSTR path);
            EXPORT_LIB bool SetFileAttrib(CFSTR const path, DWORD const attrib);

            class EXPORT_LIB CEnumerator {
            public:
                void SetDirPrefix(string_t const &dirPrefix);
                bool Next(CFileInfo &fileInfo);
                bool Next(CFileInfo &fileInfo, bool &found);

            private:
                bool NextAny(CFileInfo &fileInfo);

                CFindFile find_file_;
                string_t wildcard_;

            };

        }
    }
}
