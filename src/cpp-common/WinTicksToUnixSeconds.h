#pragma once

#include <Windows.h>

#define WINDOWS_TICK 10000000
#define SEC_TO_UNIX_EPOCH 11644473600LL

static time_t windows_tick_to_unix_seconds(FILETIME const& windows_ticks) {
    auto ticks = windows_ticks;
	return static_cast<time_t>(*reinterpret_cast<unsigned long long*>(&ticks) / WINDOWS_TICK - SEC_TO_UNIX_EPOCH);
}
