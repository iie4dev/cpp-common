#pragma once
#include <cstring>
#include <Windows.h>

#include "../../CrashRpt/include/CrashRpt.h"
#include "../../CrashHandler/crash_handler.h"
#include <QStandardPaths>

#include <cpp-common-config.h>

#pragma comment (lib,"../../CrashRpt/lib/CrashRpt1403.lib")
#pragma comment (lib,"../../CrashRpt/lib/CrashRptProbe1403.lib")

inline CR_INSTALL_INFO *GetCrInstallInfo(CR_INSTALL_INFO *pstCrInstallInfo) {
	WCHAR szPathName[MAX_PATH] = { 0 };
	if (pstCrInstallInfo) {
		GetModuleFileName(nullptr, szPathName, sizeof(szPathName) / sizeof(szPathName[0]) - 1);
		memset(pstCrInstallInfo, 0, sizeof(*pstCrInstallInfo));
		pstCrInstallInfo->cb = sizeof(CR_INSTALL_INFO);
		pstCrInstallInfo->pszAppName = L"BMJC";                         // Define application name.
		pstCrInstallInfo->pszAppVersion = L"2.1";                       // Define application version.
		pstCrInstallInfo->pszEmailSubject = L"BMJC Error Report";       // Define subject for email.
		//pstCrInstallInfo->pszEmailTo = _T ("test@hotmail.com");       // Define E-mail recipient address.  
		//pstCrInstallInfo->pszSmtpProxy = _T("127.0.0.1");             // Use SMTP proxy.
		//pstCrInstallInfo->pszSmtpLogin = _T("test");                  // SMTP Login
		//pstCrInstallInfo->pszSmtpPassword = _T("test");               // SMTP Password
		pstCrInstallInfo->pszUrl = L"http://10.10.41.46/crashrpt.php";  // URL for sending reports over HTTP.				
		//pstCrInstallInfo->pfnCrashCallback = CrashCallback;           // Define crash callback function.   
		// Define delivery methods priorities. 
		pstCrInstallInfo->uPriorities[CR_HTTP] = 3;                     // Use HTTP the first.
		pstCrInstallInfo->uPriorities[CR_SMTP] = 2;                     // Use SMTP the second.
		pstCrInstallInfo->uPriorities[CR_SMAPI] = 1;                    // Use Simple MAPI the last.  
		pstCrInstallInfo->dwFlags = 0;
		pstCrInstallInfo->dwFlags |= CR_INST_ALL_POSSIBLE_HANDLERS;     // Install all available exception handlers.    
		//pstCrInstallInfo->dwFlags |= CR_INST_APP_RESTART;             // Restart the application on crash.  
		//pstCrInstallInfo->dwFlags |= CR_INST_NO_MINIDUMP;             // Do not include minidump.
		//pstCrInstallInfo->dwFlags |= CR_INST_NO_GUI;                  // Don't display GUI.
		//pstCrInstallInfo->dwFlags |= CR_INST_DONT_SEND_REPORT;        // Don't send report immediately, just queue for later delivery.
		//pstCrInstallInfo->dwFlags |= CR_INST_STORE_ZIP_ARCHIVES;      // Store ZIP archives along with uncompressed files (to be used with CR_INST_DONT_SEND_REPORT)
		//pstCrInstallInfo->dwFlags |= CR_INST_SEND_MANDATORY;          // Remove "Close" and "Other actions..." buttons from Error Report dialog.
		//pstCrInstallInfo->dwFlags |= CR_INST_SHOW_ADDITIONAL_INFO_FIELDS; //!< Make "Your E-mail" and "Describe what you were doing when the problem occurred" fields of Error Report dialog always visible.
		pstCrInstallInfo->dwFlags |= CR_INST_ALLOW_ATTACH_MORE_FILES;   //!< Adds an ability for user to attach more files to crash report by clicking "Attach More File(s)" item from context menu of Error Report Details dialog.
		//pstCrInstallInfo->dwFlags |= CR_INST_SEND_QUEUED_REPORTS;     // Send reports that were failed to send recently.	
		//pstCrInstallInfo->dwFlags |= CR_INST_AUTO_THREAD_HANDLERS; 
		pstCrInstallInfo->pszDebugHelpDLL = nullptr;                    // Search for dbghelp.dll using default search sequence.
		pstCrInstallInfo->uMiniDumpType = MiniDumpNormal;               // Define minidump size.
		// Define privacy policy URL.
		pstCrInstallInfo->pszPrivacyPolicyURL = L"http://code.google.com/p/crashrpt/wiki/PrivacyPolicyTemplate";
		pstCrInstallInfo->pszErrorReportSaveDir = nullptr;              // Save error reports to the default location.
		//pstCrInstallInfo->pszRestartCmdLine = L"/restart";            // Command line for automatic app restart.
		//pstCrInstallInfo->pszLangFilePath = _T("D:\\");               // Specify custom dir or filename for language file.
		wcscat(szPathName, L", 1");
		//_T("C:\\WINDOWS\\System32\\user32.dll, 1"); 
		pstCrInstallInfo->pszCustomSenderIcon = szPathName;             // Specify custom icon for CrashRpt dialogs.
		pstCrInstallInfo->nRestartTimeout = 50;
	}
	return pstCrInstallInfo;
}

inline void register_crash_handle() {
	TCHAR pszBuf[MAX_PATH] = { 0 };
	
    CR_INSTALL_INFO stCrInstallInfo;
	memset(&stCrInstallInfo, 0, sizeof(stCrInstallInfo));

    CrAutoInstallHelper const stCrHelper(GetCrInstallInfo(&stCrInstallInfo));
	if (stCrHelper.m_nInstallStatus) {
		crGetLastErrorMsg(pszBuf, 256);
		MessageBox(nullptr, L"CrashRpt Install Failed", pszBuf, MB_OK);
	}

	crAddScreenshot2(CR_AS_MAIN_WINDOW | CR_AS_PROCESS_WINDOWS | CR_AS_USE_JPEG_FORMAT, 95);
	
    pszBuf[0] = 0;
	GetTempPath(MAX_PATH, pszBuf);
	
    wcscat(pszBuf, '\\' == pszBuf[wcslen(pszBuf) - 1] ? L"operateRecords.txt" : L"\\operateRecords.txt");
	crAddFile2(pszBuf, nullptr, L"Log File", CR_AF_MAKE_FILE_COPY | CR_AF_ALLOW_DELETE);
	Breakpad::CrashHandler::instance()->Init(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
}
