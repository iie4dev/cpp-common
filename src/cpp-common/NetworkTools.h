//
// Created by limo on 2018/8/11.
//

#ifndef FILE_CHECK_NETWORKTOOLS_H
#define FILE_CHECK_NETWORKTOOLS_H

#include <string>

#include <cpp-common-config.h>

EXPORT_LIB bool get_free_port(unsigned short &port);

EXPORT_LIB bool dispatch_free_port(std::string& channel, int try_times = 100);

#endif