#include <cpp-common/HighLighter.h>


Highlighter::Highlighter(QTextCharFormat default_format, QStringList const& keywords, QTextDocument* parent):
	QSyntaxHighlighter(parent)
	, keyword_format_(default_format)
{
	if (keywords.isEmpty()) return;

	default_format.setForeground(Qt::red);

	HighlightingRule rule;
	for (auto const& keyword : keywords)
	{
		if (keyword.isEmpty()) continue;

		QString keyword_wild;
		auto const l = keyword.size() - 1;
		for (auto i = 0; i < l; ++i)
		{
			keyword_wild.append(keyword[i]);
			keyword_wild.append(".*");
		}
		keyword_wild.append(keyword[l]);

		rule.pattern = QRegularExpression(keyword_wild);
		rule.format = default_format;
		high_lighting_rules_.append(rule);
	}
}

void Highlighter::highlightBlock(const QString& text)
{
	foreach(auto const& rule, high_lighting_rules_)
	{
		auto match_iterator = rule.pattern.globalMatch(text);
		while (match_iterator.hasNext())
		{
			auto match = match_iterator.next();
			setFormat(match.capturedStart(), match.capturedLength(), rule.format);
		}
	}
}
