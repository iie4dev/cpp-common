#include "cpp-common/MonitorFolder.h"

#include <iostream>
#include <ctime>


FolderMonitor::FolderMonitor(): prepared_(false)
                            , notify_filter_()
                            , monitoring_(false)
                            , folder_handle_(INVALID_HANDLE_VALUE)
                            , pre_read_time_point_(0) {
}

FolderMonitor::~FolderMonitor() {
    if (INVALID_HANDLE_VALUE != folder_handle_)
        CloseHandle(folder_handle_);
}

bool FolderMonitor::monitoring(fn_notify_callback_t const& notify_callback) {
    if (!prepared_) return false;
    monitoring_ = true;

    std::cout << "Monitor: Start monitoring..." << std::endl;

    size_t const buff_size = 2048;
    char notify[buff_size];
    memset(notify, 0, buff_size);
    auto const p_notify = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(notify);

    while (monitoring_) {
        DWORD cb_bytes;
        if (ReadDirectoryChangesW(folder_handle_, &notify, buff_size, true,
                                  notify_filter_, &cb_bytes, nullptr, nullptr)) {
            if (!monitoring_) break;

            if (nullptr != reinterpret_cast<WCHAR*>(p_notify->FileName)) {
                DWORD next_action = 0;
                string_t file_name, new_file_name;

                np_limo::str::ConvertWideCharToApp(
                    file_name, p_notify->FileName, p_notify->FileNameLength >> 1);

                if (pre_file_name_ != file_name || GetTickCount() > pre_read_time_point_ + 300) {
                    if (FILE_ACTION_RENAMED_OLD_NAME == p_notify->Action) {
                        if (p_notify->NextEntryOffset) {
                            auto const p_new_notify = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(
                                reinterpret_cast<char*>(p_notify) + p_notify->NextEntryOffset);
                            np_limo::str::ConvertWideCharToApp(
                                new_file_name, p_new_notify->FileName, p_new_notify->FileNameLength >> 1);
                            next_action = p_new_notify->Action;
                        }
                    }
                    notify_callback(folder_, file_name, new_file_name, p_notify->Action, next_action);
                }

                pre_file_name_ = file_name;
                pre_read_time_point_ = GetTickCount();
            }
        }
    }

    std::cout << "Monitor: End monitoring" << std::endl;

    CloseHandle(folder_handle_);
    folder_handle_ = INVALID_HANDLE_VALUE;
    return true;
}

bool FolderMonitor::monitor_on(string_t const& folder) {
    return monitor_on(folder, FILE_NOTIFY_CHANGE_FILE_NAME
                      | FILE_NOTIFY_CHANGE_DIR_NAME
                      | FILE_NOTIFY_CHANGE_ATTRIBUTES
                      | FILE_NOTIFY_CHANGE_CREATION
                      | FILE_NOTIFY_CHANGE_LAST_ACCESS
                      | FILE_NOTIFY_CHANGE_LAST_WRITE
                      | FILE_NOTIFY_CHANGE_SIZE
    );
}

bool FolderMonitor::monitor_on(string_t const& folder, DWORD const notify_filter) {
    folder_ = folder;
    notify_filter_ = notify_filter;
    prepared_ = false;
    folder_handle_ = CreateFile(folder_.c_str(),
        GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        nullptr,
        OPEN_EXISTING,
        FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED,
        nullptr);

    if (folder_handle_ == INVALID_HANDLE_VALUE) {
//        std::cout << "Monitor: Fail to open folder - " << folder_ << std::endl;
        return false;
    }
    prepared_ = true;
    return true;
}

void FolderMonitor::stop_monitor() {
    monitoring_ = false;
}
