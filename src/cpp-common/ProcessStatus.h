//
// Created by limo on 2018/8/12.
//

#ifndef FILE_CHECK_PROCESSSTATUS_H
#define FILE_CHECK_PROCESSSTATUS_H

enum ProcessStatus {
    IDLE,
    RUNNING,
    PAUSED,
    END,
    CRASHED
};

#endif
