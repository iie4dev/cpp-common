#include "cpp-common/FileSystemUtils.h"


typedef BOOL(WINAPI * GetDiskFreeSpaceEx_Pointer)(
    CFSTR lp_directory_name,                      // directory name
    PULARGE_INTEGER lp_free_bytes_available,      // bytes available to caller
    PULARGE_INTEGER lp_total_number_of_bytes,     // bytes on disk
    PULARGE_INTEGER lp_total_number_of_free_bytes // free bytes on disk
    );

namespace np_limo
{
    namespace file
    {
        namespace system
        {
            ENUM_MAP(DataUnit,
                     TEXT("B"), TEXT("KB"), TEXT("MB"), TEXT("GB"), TEXT("TB"), TEXT("PB"), TEXT("EB"),
                     TEXT("ZB"), TEXT("YB"), TEXT("BB"), TEXT("NB"), TEXT("DB"), TEXT("CB"), TEXT("XB")
            );

            bool get_volume_info(CFSTR const root,
                                 string_t& volume_name, string_t& file_system_name,
                                 DWORD& volume_serial_number,
                                 DWORD& max_component_length,
                                 DWORD& file_system_flags) {

                char_t volume_name_str[MAX_PATH + 2];
                char_t file_system_name_str[MAX_PATH + 2];
                auto const ret = ::GetVolumeInformation(
                    root,
                    volume_name_str, MAX_PATH,
                    &volume_serial_number,
                    &max_component_length,
                    &file_system_flags,
                    file_system_name_str, MAX_PATH);
                volume_name.assign(volume_name_str);
                file_system_name.assign(file_system_name_str);

                return TRUE == ret;
            }

            UINT64 get_disk_free_size(CFSTR const disk) {
                UINT64 cluster_size, total_size, free_size;
                if (get_disk_info(disk, cluster_size, total_size, free_size))
                    return free_size;
                return 0;
            }

            bool get_disk_info(CFSTR const disk, UINT64 &cluster_size, UINT64 &total_size, UINT64 &free_size) {
                auto const p_get_disk_free_space_ex = reinterpret_cast<GetDiskFreeSpaceEx_Pointer>(
                    GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")), "GetDiskFreeSpaceEx"));

                auto size_is_detected = false;
                if (p_get_disk_free_space_ex) {
                    ULARGE_INTEGER free_bytes_to_caller2, total_size2, free_size2;
                    size_is_detected = TRUE == p_get_disk_free_space_ex(disk, &free_bytes_to_caller2, &total_size2, &free_size2);
                    total_size = total_size2.QuadPart;
                    free_size = free_size2.QuadPart;
                }

                DWORD num_sectors_per_cluster, bytes_per_sector, num_free_clusters, num_clusters;
                if (TRUE != ::GetDiskFreeSpace(disk, &num_sectors_per_cluster, &bytes_per_sector, 
                                               &num_free_clusters, &num_clusters))
                    return false;

                cluster_size = bytes_per_sector * num_sectors_per_cluster;
                if (!size_is_detected) {
                    total_size = cluster_size * static_cast<UINT64>(num_clusters);
                    free_size = cluster_size * static_cast<UINT64>(num_free_clusters);
                }
                return true;
            }

            bool format_file_size(string_t& formatted, UINT64 const file_size_in_bytes) {
                double num;
                EDataUnit unit;
                if (!format_file_size_in_units(num, unit, file_size_in_bytes))
                    return false;

            #ifdef _UNICODE
                formatted.assign(std::to_wstring(num))
                    .append(L" ")
                    .append(gDataUnitNameMap.at(unit));
            #else
                formatted.assign(std::to_string(num))
                    .append(" ")
                    .append(gDataUnitNameMap.at(unit));
            #endif
                return true;
            }

            bool format_file_size_in_units(double &num, EDataUnit &unit, UINT64 file_size_in_bytes) {
                unsigned idx = 0, decimal_part = 0;
                while (file_size_in_bytes > 1024) {
                    decimal_part = file_size_in_bytes % 1024;
                    file_size_in_bytes /= 1024;
                    ++idx;
                }
                if (idx > gDataUnitSize) return false;
                num = file_size_in_bytes + static_cast<double>(decimal_part) / 1024;
                unit = gAllDataUnit[idx];
                return true;
            }
        }
    }
}
