//
// Created by limo on 2018/7/27.
//

/* NOTE!!!: functions defined under this file have been deprected! now we
            use lib <cereal> to support serialization ability.
#ifndef FILE_CHECK_TOKENIZER_H
#define FILE_CHECK_TOKENIZER_H

#include <string>
#include <vector>

// support plant object
// NOTICE: pointer will not be un-reference.
template <typename T>
std::string tokenize(const T& val) {
    // TODO: add code to asset the type T should be raw c type such as int, double, etc
    const size_t size = sizeof(T);
    const auto *p_char = reinterpret_cast<const char *>(&val);

    std::string token(size, 0);
    for (size_t i = 0; i < size; ++i)
        token[i] = *(p_char+i);
    return token;
}

template <>
inline std::string tokenize(const std::string& val) {
    return tokenize(val.size()).append(val);
}

template <typename T>
void untokenize(T& o_val, std::string const &i_token, size_t &io_offset) {
    const size_t size = sizeof(T);
    auto *p_char = reinterpret_cast<char *>(&o_val);

    for (size_t i = 0; i < size; ++i)
        *(p_char+i) = i_token[io_offset+i];
    io_offset += size;
}

template <>
inline void untokenize(std::string& o_val, std::string const&i_token, size_t& io_offset) {
    size_t size;
    untokenize(size, i_token, io_offset);
    o_val = i_token.substr(io_offset, size);
    io_offset += size;
}

template <typename T>
inline std::string tokenize_vec(std::vector<T> const& items) {
    std::string token;
    token.append(tokenize(items.size()));
    for (auto const& item : items) {
        token.append(tokenize(item));
    }
    return token;
}

template <typename T>
inline void untokenize_vec(std::vector<T>& o_items, std::string const& i_token, size_t& io_offset) {
    size_t size;
    untokenize(size, i_token, io_offset);
    for (size_t i = 0; i < size; ++i) {
        T item;
        untokenize(item, i_token, io_offset);
        o_items.push_back(item);
    }
}

template <typename T>
void untokenize(T& o_val, std::string const &i_token) {
    size_t offset = 0; untokenize<T>(o_val, i_token, offset);
}

template <>
inline void untokenize(std::string& o_val, std::string const&i_token) {
    size_t offset = 0; untokenize(o_val, i_token, offset);
}

template <typename T>
inline void untokenize_vec(std::vector<T>& o_items, std::string const& i_token) {
    size_t offset = 0; untokenize_vec(o_items, i_token, offset);
}


#endif //FILE_CHECK_TOKENIZER_H
