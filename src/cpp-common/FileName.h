#pragma once

#include "StringUtils.h"

#include <cpp-common-config.h>

namespace np_limo
{
    namespace file
    {
        namespace name
        {
            EXPORT_LIB extern const unsigned kDevicePathPrefixSize;
            EXPORT_LIB extern const unsigned kSuperPathPrefixSize;
            EXPORT_LIB extern const unsigned kSuperUncPathPrefixSize;
            EXPORT_LIB extern const unsigned kDrivePrefixSize;

            EXPORT_LIB bool IsAbsolutePath(CFSTR path);
            EXPORT_LIB bool IsDrivePath(CFSTR path);         /* A:\ */
            EXPORT_LIB bool IsDrivePath2(CFSTR path);         /* A: */
            EXPORT_LIB bool IsDevicePath(CFSTR path);        /* \\.\ */
            EXPORT_LIB bool IsSuperPath(CFSTR path);         /* \\?\ */
            EXPORT_LIB bool IsDeviceOrSuperPath(CFSTR path); /* \\.\ or \\?\ */
            EXPORT_LIB bool IsSuperUncPath(CFSTR path);      /* \\?\UNC\ */
            EXPORT_LIB bool IsNetworkPath(CFSTR path);       /* \\?\UNC\ or \\SERVER */

            EXPORT_LIB unsigned GetNetworkServerPrefixSize(CFSTR path);

            EXPORT_LIB int FindSepar(CFSTR path);
            EXPORT_LIB int FindSeparReverse(CFSTR path);

            EXPORT_LIB void NormalizeDirPathPrefix(string_t& path);
            EXPORT_LIB bool ResolveDotsFolder(string_t &path);

            EXPORT_LIB unsigned GetRootPrefixSize(CFSTR path);
            EXPORT_LIB unsigned GetRootPrefixSizeOfSuperPath(CFSTR path);
            EXPORT_LIB unsigned GetRootPrefixSizeOfNetworkPath(CFSTR path);
            EXPORT_LIB unsigned GetRootPrefixSizeOfSimplePath(CFSTR path);

            EXPORT_LIB void NormalizePathSeparator(string_t &path);
            EXPORT_LIB bool GetFullPath(string_t &fullPath, CFSTR dirPrefix, CFSTR path);
            EXPORT_LIB string_t JoinPath(string_t const& folder, string_t const& name);
            EXPORT_LIB void TrimEndSeparator(string_t &path);

            enum ESuperPathType {
                kUseOnlyMain,
                kUseOnlySuper,
                kUseMainAndSuper
            };

            EXPORT_LIB ESuperPathType CheckSuperPathType(CFSTR path);
            EXPORT_LIB bool SuperPath(string_t &superPath, CFSTR path);

            EXPORT_LIB bool SplitFolderFromPath(string_t &folder, CFSTR path);
            EXPORT_LIB bool SplitNameWithExtFromPath(string_t &name, CFSTR path);
            EXPORT_LIB bool SplitNameWithoutExtFromPath(string_t &name, CFSTR path);
            EXPORT_LIB bool SplitExtensionFromPath(string_t &extension, CFSTR path);

            EXPORT_LIB bool SplitNameExtFromPath(string_t &name, string_t &ext, CFSTR path);
            EXPORT_LIB bool SplitFolderNameWithExtFromPath(string_t &folder, string_t &name, CFSTR path);
            EXPORT_LIB bool SplitFolderNameWithoutExtFromPath(string_t &folder, string_t &name, CFSTR path);
            EXPORT_LIB bool SplitFolderNameExtFromPath(string_t &folder, string_t &name, string_t &ext, CFSTR path);
        }
    }
}
