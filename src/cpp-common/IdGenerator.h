//
// Created by limo on 2018/7/29.
//

#ifndef FILE_CHECK_IDGENERATOR_H
#define FILE_CHECK_IDGENERATOR_H


#include <atomic>

template <typename, typename T=int>
class IdGenerator {
public:
    static T FreshId() {
        static std::atomic<T> id_base;
        return id_base.fetch_add(1);
    }

};


#endif //FILE_CHECK_IDGENERATOR_H
