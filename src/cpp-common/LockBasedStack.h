//
// Created by limo on 2018/8/8.
//

#ifndef FILE_CHECK_LOCKBASEDSTACK_H
#define FILE_CHECK_LOCKBASEDSTACK_H

#include "ThreadSafeStackInterface.h"

#include <condition_variable>
#include <stack>
#include <mutex>


template <typename T>
class LockBasedStack : public ThreadSafeStackInterface<T> {
public:
	~LockBasedStack() = default;
	
	void Push(T data) override {
		Push(std::make_shared<T>(std::move(data)));
	}

	void Push(std::shared_ptr<T> p_data) override {
		std::lock_guard<std::mutex> lck(m_mtx);
		m_data_stack.push(std::move(p_data));
	}

	std::shared_ptr<T> Pop() override {
		std::lock_guard<std::mutex> lck(m_mtx);
		if (m_data_stack.empty()) 
			return nullptr;

		auto const res = std::move(m_data_stack.top());
		m_data_stack.pop();
		return res;
	}

	bool Pop(T& data) override {
		std::lock_guard<std::mutex> lck(m_mtx);
		if (m_data_stack.empty())
			return false;

		data = std::move(*m_data_stack.top());
		m_data_stack.pop();
		return true;
	}

	bool Empty() const override {
		std::lock_guard<std::mutex> lck(m_mtx);
		return m_data_stack.empty();
	}

	std::shared_ptr<T> WaitAndPop() override {
		std::unique_lock<std::mutex> lck(m_mtx);
		m_data_cv.wait(lck, [&] { return !m_data_stack.empty(); });
		return Pop();
	}

	void WaitAndPop(T& data) override {
		std::unique_lock<std::mutex> lck(m_mtx);
		m_data_cv.wait(lck, [&] { return !m_data_stack.empty(); });
		Pop(data);
	}

    void TravelConst(std::function<bool(std::shared_ptr<T> const&)> const& processor) {
        std::unique_lock<std::mutex> lck(m_mtx);
        auto copy_stack = m_data_stack;
        while (!copy_stack.empty()) {
            if (!processor(copy_stack.top())) break;
            copy_stack.pop();
        }
    }

    void Travel(std::function<bool(std::shared_ptr<T>&)> const& processor) {
        std::unique_lock<std::mutex> lck(m_mtx);
        auto copy_stack = m_data_stack;
        while (!copy_stack.empty()) {
            if (!processor(copy_stack.top())) break;
            copy_stack.pop();
        }
    }

    std::vector<std::shared_ptr<T>> Copy() {
        std::unique_lock<std::mutex> lck(m_mtx);
        std::vector<std::shared_ptr<T>> data_vec;
        auto copy_stack = m_data_stack;
        while (!copy_stack.empty()) {
            data_vec.push_back(copy_stack.top());
            copy_stack.pop();
        }
        return data_vec;
    }

private:
	std::stack<std::shared_ptr<T>> m_data_stack;
	mutable std::mutex m_mtx;
	std::condition_variable m_data_cv;
};


#endif // FILE_CHECK_LOCKBASEDSTACK_H