#pragma once

#include <string>
#include <Windows.h>
#include <cpp-common/DefBase.h>
#include <cpp-common/Singleton.h>

class SingletonProcess : public Singleton<SingletonProcess> {
	friend class Singleton<SingletonProcess>;
public:
	SingletonProcess() : singleton_mutex_handle_(nullptr) {}
	~SingletonProcess() {
        Release();
	}

	bool singleton_check(std::string const& process_name) {
		// no matter call any times in current process, always return true
        // if it get the singleton mutex
	    if (IS_VALID_HANDLE(singleton_mutex_handle_)) {
			return true;
		}

	    SetLastError(NO_ERROR);

		singleton_mutex_handle_= ::CreateMutexA(nullptr, TRUE, process_name.c_str());
		auto const err = GetLastError();
		return err == NO_ERROR;
	}

	void Release() {
        if (IS_VALID_HANDLE(singleton_mutex_handle_)) {
            ::CloseHandle(singleton_mutex_handle_);
        }
	}

private:
	HANDLE singleton_mutex_handle_;

};
