#include "cpp-common/TempFileGuard.h"

#include <tchar.h>
#include <string>


wchar_t temp_dir[MAX_PATH] = {0};

std::wstring GetTmpDir()
{
	if (temp_dir[0] == _T('\0'))
		GetTempPathW(MAX_PATH, temp_dir);
	return temp_dir;
}

std::wstring GetATempFileName()
{
	wchar_t temp_file_name[MAX_PATH];
	if (temp_dir[0] == _T('\0'))
		GetTempPathW(MAX_PATH, temp_dir);
	const auto u_ret_val = GetTempFileNameW(
		temp_dir,
		L"FE_TMP",
		0,
		temp_file_name
	);
	if (u_ret_val == 0)
		return L"";
	return temp_file_name;
}

TempFileGuard::TempFileGuard(const std::wstring& suffix, const bool create_file)
{
	tmp_file_path_ = GetATempFileName();
	need_delete_ = true;

	if (!suffix.empty())
	{
		const auto new_tmp = tmp_file_path_ + suffix;
		if (create_file)
			MoveFileW(tmp_file_path_.c_str(), new_tmp.c_str());
		else
			DeleteFileW(tmp_file_path_.c_str());
		tmp_file_path_ = new_tmp;
	}
	else
	{
		if (!create_file)
			DeleteFileW(tmp_file_path_.c_str());
	}
}

TempFileGuard::TempFileGuard(const wchar_t* suffix, bool const create_file)
	: TempFileGuard(std::wstring(suffix ? suffix : L""), create_file)
{
}

TempFileGuard::~TempFileGuard()
{
	if (need_delete_)
		DeleteFileW(tmp_file_path_.c_str());
}

std::wstring& TempFileGuard::getTmpFilePath()
{
	return tmp_file_path_;
}

TempFileGuard::TempFileGuard(TempFileGuard&& first)
	: tmp_file_path_(std::move(first.tmp_file_path_))
	, need_delete_(true)
{
	first.need_delete_ = false;
}

TempFileGuard TempFileGuard::guardExistFile(const std::wstring& path)
{
	auto a = TempFileGuard(L"", false);
	a.tmp_file_path_ = path;
	return a;
}
