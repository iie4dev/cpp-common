//
// Created by limo on 8/22/2018.
//

#ifndef FILE_CHECK_ACAUTOMATON_H
#define FILE_CHECK_ACAUTOMATON_H


#include <functional>
#include <memory>
#include <queue>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <Windows.h>

#include <cpp-common/StringUtils.h>

#include <cpp-common-config.h>

/**
 * EType of callback which will be called whenever there is a match in the content
 * 
 * @param offset             offset of the matched strings in the content
 * @param matched_strings    there maybe multi matched strings
 * @return to tell automaton to continue search the content or just quit after 
 *          this match
 */
using match_callback_t = std::function<bool(size_t offset, std::string const& context, 
    std::set<std::pair<np_limo::str::EStringEncoding, std::string>> const& matched_strings)>;


/**
 * @brief EType of callback  which is used to extract <text_block> sequentially 
 *         from a text stream. This will be useful if the text stream is too
 *         big to fetch the content at just one time.
 *         
 * @param text_block  the chars read will be store into here
 * @param block_size  the size of text_block, this is the max you can read
 * @return num of chars just read, 0 if there is no more chars to read
 */
using text_in_callback_t = std::function<size_t(std::string &text_block)>;


class EXPORT_LIB CircleBuffer
{
public:
	explicit CircleBuffer(size_t const buffer_size)
		: buff_(buffer_size, 0)
		, buffer_size_(buffer_size)
		, curr_(0)
	{
	}

	void reset()
	{
        std::fill(buff_.begin(), buff_.end(), 0);
        curr_ = 0;
	}

	void push(char const c)
	{
        buff_[curr_++ % buffer_size_] = c;
	}

    size_t size() const
	{
        return buffer_size_;
	}

    void resize(size_t const size)
	{
        buffer_size_ = size;
        buff_.resize(size, 0);
	}
	
	std::string to_string() const
	{
        if (curr_ <= buffer_size_)
            return buff_.substr(0, curr_);
        auto const curr = curr_ % buffer_size_;
        return buff_.substr(curr) + buff_.substr(0, curr);
	}
	
private:
    std::string buff_;
    size_t buffer_size_;
    size_t curr_;
	
};


/**
 * Ac automaton algorithm is used to match a list of keywords in given content efficiently.
 * This implement of ac-automaton algorithm is modified finely for supporting searching
 * chinese character.
 *
 * See https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm for more info
 */

class EXPORT_LIB AcAutomatonCore {
    friend class AcAutomaton;
    
    struct State {
        std::unordered_map<char, std::shared_ptr<State>> next;
        std::shared_ptr<State> p_fail {nullptr};
        std::set<std::pair<np_limo::str::EStringEncoding, std::string>> outs;
    };

    explicit AcAutomatonCore(std::shared_ptr<State> tree);

public:
    struct Builder {
        void set(std::vector<std::string> const& keywords);
        void set(std::vector<std::pair<np_limo::str::EStringEncoding, std::string>> const& pairs);
    	void append(std::string const& keyword);
        void append(np_limo::str::EStringEncoding encoding, std::string const& keyword);
        void append(std::string const& keyword, std::string const& payload);
        void append(np_limo::str::EStringEncoding encoding, std::string const& keyword, std::string const& payload);
        void clear();
        std::shared_ptr<AcAutomatonCore> build() const;

    private:
        std::shared_ptr<State> p_root_;

    };

private:
    std::shared_ptr<State> p_root_;
    
};

class EXPORT_LIB AcAutomaton {
public:
    explicit AcAutomaton(std::shared_ptr<AcAutomatonCore> tree);
    
    bool is_ready() const;

    void ignore_chars(std::string const& chars);
    void set_context_size(size_t size);
	
    void set_hit_limit(size_t hit_limit);
    void set_search_limit(size_t search_limit);

    void set_core(std::shared_ptr<AcAutomatonCore> core);

    bool search_in(text_in_callback_t const& text_in, match_callback_t const& callback);
    bool search_in(char const* content, size_t size, match_callback_t const& callback);

    bool search_in(std::string const& content, match_callback_t const& callback);
    bool search_in(HANDLE h_content, match_callback_t const& callback);
    bool search_in(std::istream& content, match_callback_t const& callback);

    bool before_search(match_callback_t const& callback);
    void after_search();
    bool search_in_buff(char const* content, size_t size);

private:
    bool pop_hit(size_t last_hit_offset);

    std::shared_ptr<AcAutomatonCore> p_core_;
    std::unordered_set<char> ignore_chars_;

	std::string buff_;

    size_t content_offset_;
    size_t search_limit_;
    size_t hit_limit_;
    size_t hit_count_;

    match_callback_t hit_callback_;
	
    /**
     * \brief A queue used to cache the hits which have not collected enough
     * context yet. The cached item is a pair of hit offset and hit payload.
     */
    std::queue<std::pair<size_t, std::shared_ptr<AcAutomatonCore::State>>> hits_cache_;

    CircleBuffer context_;
	
	// std::string hit_context_;
    // size_t hit_context_curr_;
    // bool hit_context_full_;

    bool is_searching_;

};


#endif //FILE_CHECK_ACAUTOMATON_H
