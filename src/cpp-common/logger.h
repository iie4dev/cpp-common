#pragma once
//
// Created by limo on 2018/8/11.
//


#include <spdlog/spdlog.h>

#include <cpp-common-config.h>
#include <cpp-common/StringUtils.h>

extern std::string kGlobalLogger;
extern bool g_log_enabled;

namespace np_limo {

    namespace log {
        using log_level_e = spdlog::level::level_enum;

        // enum ELogLevel {
            // kTrace, kDebug, kInfo, kWarn, kError, kFatal
        // };

        EXPORT_LIB bool initialize(std::string const& log_name,
                        bool need_log_to_console,
                        bool need_log_to_file, std::string const& log_file,
                        log_level_e log_level);

        // see https://stackoverflow.com/questions/2031163/when-to-use-the-different-log-levels
        // for more info about difference among log levels

        inline void flush(std::string const&logger_name) {
            if (g_log_enabled)
                spdlog::get(logger_name)->flush();
        }


        template<typename... Args>
        void trace(std::string const&logger_name, const char *fmt, const Args &... args) {
            if (g_log_enabled) {
                spdlog::get(logger_name)->trace(fmt, args...);
            }
        }

        template<typename... Args>
        void debug(std::string const&logger_name, const char *fmt, const Args &... args) {
            if (g_log_enabled) {
                spdlog::get(logger_name)->debug(fmt, args...);
            }
        }

        template<typename... Args>
        void info(std::string const&logger_name, const char *fmt, const Args &... args) {
            if (g_log_enabled) {
                spdlog::get(logger_name)->info(fmt, args...);
            }
        }

        template<typename... Args>
        void warn(std::string const&logger_name, const char *fmt, const Args &... args) {
            if (g_log_enabled) {
                spdlog::get(logger_name)->warn(fmt, args...);
            }
        }

        template<typename... Args>
        void error(std::string const&logger_name, const char *fmt, const Args &... args) {
            if (g_log_enabled) {
                spdlog::get(logger_name)->error(fmt, args...);
            }
        }

        template<typename... Args>
        void fatal(std::string const&logger_name, const char *fmt, const Args &... args) {
            if (g_log_enabled) {
                spdlog::get(logger_name)->critical(fmt, args...);
            }
        }

    }

}
