#ifndef FILE_CHECK_OBJECTPOOL_H
#define FILE_CHECK_OBJECTPOOL_H

#include <mutex>
#include <memory>
#include <vector>
#include <map>
#include <algorithm>

#include "Singleton.h"
#include "FactoryInterface.h"
#include "Register.h"


template <typename B, typename T>
class ObjectPool :
        public Singleton<ObjectPool<B, T>>,
        public Register<T, int> {

    friend class Singleton<ObjectPool<B, T>>;
    using obj_ptr_t = std::shared_ptr<B>;
    using pool_t = std::vector<obj_ptr_t>;
    using pool_capacity_t = std::pair<pool_t, size_t>;

private:
    ObjectPool() = default;

public:
    virtual ~ObjectPool() = default;

    void Init(std::shared_ptr<FactoryInterface<B>> p_factory_,
              size_t init_capacity,
              size_t max_inc_capacity_) {
        mp_factory = std::move(p_factory_);
        m_init_capacity = init_capacity;
        m_max_inc_capacity = max_inc_capacity_;

        for (const auto &kv : this->m_register_map)
            ResizePool(kv.first, init_capacity);
    }

    void Shrink() {
        for (const auto &kv : this->m_register_map)
            ResizePool(kv.first, m_init_capacity);
    }

    void Clear() {
        for (const auto &kv : this->m_register_map)
            ResizePool(kv.first, 0);
    }

    obj_ptr_t Acquire(const T& type) {
        std::lock_guard<std::mutex> lock_guard(m_mtx);

        auto &pool_capacity = m_container[type];
        std::vector<obj_ptr_t> &pool = pool_capacity.first;
        if (!pool.empty()) {
            auto p_obj = pool.back();
            pool.pop_back();
            return std::move(p_obj);
        }

        if (m_max_inc_capacity == 0) return nullptr;

        // expand pool if pool get empty
        // ResizePool(type, std::min(pool.size() * 2, m_max_inc_capacity));
        return Acquire(type);
    }

    size_t Size(const T& type) const {
        return m_container.at(type).first.size();
    }

private:
    void ResizePool(const T& type, size_t capacity) {
        std::lock_guard<std::mutex> lock_guard(m_mtx);

        auto &pool_capacity = m_container[type];
        std::vector<obj_ptr_t > &pool = pool_capacity.first;
        const size_t curr_capacity = pool_capacity.second;

        if (curr_capacity < capacity) {
            auto tasks = mp_factory->CreateObjsByType(
                    type,
                    capacity-curr_capacity,
                    [this, type](B* p_b){Release(type, p_b);});

            for (auto &task : tasks) {
                pool.push_back(task);
            }
        } else {
            // remove all items that can be removed now, deleter will take care of the remain
            // items cannot deleted
            size_t need_remove_num = curr_capacity-capacity;
            pool.resize(need_remove_num > pool.size() ? need_remove_num-pool.size() : 0);
        }

        pool_capacity.second = capacity;
    }

    void Release(const T &type, B* p_b) {
        auto& pool_capacity = m_container[type];
        auto& pool = pool_capacity.first;
        const auto capacity = pool_capacity.second;

        if (pool.size() < capacity) {
            pool.push_back(WrapPtr(type, p_b));
        } else {  // if pool is full, just destroy it
            std::default_delete<B>()(p_b);
        }
    }

    std::shared_ptr<B> WrapPtr(const T& type, B* b) {
        return std::move(std::shared_ptr<B>(b, [this, type](B* p_b){Release(type, p_b);}));
    }

    size_t m_init_capacity {};
    size_t m_max_inc_capacity {};
    std::shared_ptr<FactoryInterface<B>> mp_factory;
    std::map<T, pool_capacity_t> m_container {};
    std::mutex m_mtx {};

};

#define REGISTER_TASK_TO_POOL(E, B, T) bool __trick_pool_##T = ObjectPool<B,B::EType>::Instance()->\
                           RegisterKeyValue(E, 0);


#endif //FILE_CHECK_OBJECTPOOL_H
