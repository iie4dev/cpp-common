#pragma once
#include <string>

#include <Windows.h>

#include <cpp-common-config.h>

#pragma region type for char and string
using char_a_t = char;
using char_w_t = wchar_t;
using str_a_t = char_a_t * ;
using str_w_t = char_w_t * ;
using c_str_a_t = const char *;
using c_str_w_t = const wchar_t *;
using string_a_t = std::string;
using string_w_t = std::wstring;

#define ASTRLEN(s) strlen((s))
#define WSTRLEN(s) wcslen((s))

#ifdef _UNICODE
using char_t = char_w_t;
using CFSTR = c_str_w_t;
using string_t = string_w_t;
#define TO_STR(x) std::to_wstring((x))
#define SSTRLEN(s) WSTRLEN(s)
#else
using char_t = char_a_t;
using CFSTR = c_str_a_t;
using string_t = string_a_t;
#define TO_STR(x) std::to_string((x))
#define SSTRLEN(s) ASTRLEN(s)
#endif

using p_char_t = char_t * ;
using str_t = p_char_t;
#pragma endregion


#pragma region macro for path separator
#define IS_PATH_SEPARATOR(c) ('\\' == (c) || '/' == (c))

#ifdef _WIN32
#define STR_PATH_SEPARATOR TEXT("\\")
#define PATH_SEPARATOR TEXT('\\')
#define A_PATH_SEPARATOR '\\'
#define W_PATH_SEPARATOR L'\\'
#else
#define STR_PATH_SEPARATOR TEXT("/")
#define PATH_SEPARATOR TEXT('/')
#define A_PATH_SEPARATOR '/'
#define W_PATH_SEPARATOR L'/'
#endif
#pragma endregion


namespace np_limo
{
    namespace str
    {
        enum EStringEncoding {
            UTF8, GBK, UNIC
        };

    #pragma region functions for converting between multi-bytes and wide-char
        EXPORT_LIB bool ConvertMultiBytesToWideChar(std::wstring &w_str, char const* a_str, size_t a_str_size, unsigned codepage);
        EXPORT_LIB bool ConvertWideCharToMultiBytes(std::string &a_str, wchar_t const* w_str, size_t w_str_size, unsigned codepage);

        EXPORT_LIB bool ConvertBetweenMultiBytes(std::string &to, char const* from, size_t from_size,
                                      unsigned to_codepage, unsigned from_codepage);

        EXPORT_LIB bool ConvertSysToUtf(std::string &to, char const* from, size_t from_size);
        EXPORT_LIB bool ConvertUtfToSys(std::string &to, char const* from, size_t from_size);

        EXPORT_LIB bool ConvertAppToUtf(std::string &to, char_t const* from, size_t from_size);
        EXPORT_LIB bool ConvertUtfToApp(string_t &to, char const* from, size_t from_size);

        EXPORT_LIB bool ConvertSysToApp(string_t &to, char const* from, size_t from_size);
        EXPORT_LIB bool ConvertAppToSys(std::string &to, char_t const* from, size_t from_size);

        EXPORT_LIB bool ConvertWideCharToSys(std::string &to, wchar_t const* str, size_t str_len);
        EXPORT_LIB bool ConvertWideCharToApp(string_t &to, wchar_t const* str, size_t str_len);

        EXPORT_LIB bool ConvertGbkToUtf(std::string &to, char const* from, size_t from_size);
        EXPORT_LIB bool ConvertUtfToGbk(std::string &to, char const* from, size_t from_size);

        EXPORT_LIB bool ConvertToUtf(std::string &to, void const* from, size_t from_size, EStringEncoding encoding);
    #pragma endregion
    
    #pragma region functions for path separator
        EXPORT_LIB inline bool IsPathSeparator(char_a_t const c) { return c == A_PATH_SEPARATOR; }
        EXPORT_LIB inline bool IsPathSeparator(char_w_t const c) { return c == W_PATH_SEPARATOR; }
    #pragma endregion

        EXPORT_LIB int find_first_char(CFSTR str, char_t c);
        EXPORT_LIB int find_last_char(CFSTR str, char_t c);

    #pragma region functions for string compare
        /*
         * *NOTE*: all functions that used to compare between multi-byte and wide-char 
         * can only be used when the items of strings are all ascii bytes since encode
         * method of multi-byte is various, they all will use several bytes to present
         * one real char so there may not have no one-to-one map between multi-byte 
         * and wide-char strings.
         */

        /**
        * \brief Check if <ptrContent> has a prefix <ptrPattern>. 
        * \tparam T1 Should be either c_str_a_t or c_str_w_t
        * \tparam T2 Should be either c_str_a_t or c_str_w_t
        */
        template <typename T1, typename T2>
        bool IsPrefix(T1 const *ptrContent, T2 const *ptrPattern) {
            while (*ptrPattern) {
                if (*ptrContent++ != *ptrPattern++) 
                    return false;
            }
            return true;
        }

        template <typename T1, typename T2>
        bool IsPrefixNoCase(T1 const *ptrContent, T2 const *ptrPattern) {
            while (*ptrPattern) {
                if (tolower(*ptrContent++) != tolower(*ptrPattern++))
                    return false;
            }
            return true;
        }

        template <typename T1, typename T2>
        bool are_equal(T1 const* str1, T2 const* str2) {
            while (*str1) {
                if (*str1++ != *str2++)
                    return false;
            }
            return *str1 == *str2;
        }

        template <typename T1, typename T2>
        bool are_equal_no_case(T1 const* str1, T2 const* str2) {
            while (*str1) {
                if (tolower(*str1++) != tolower(*str2++))
                    return false;
            }
            return *str1 == *str2;
        }
    #pragma endregion
    }
}
