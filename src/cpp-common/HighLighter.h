#pragma once

#include <qsyntaxhighlighter.h>
#include <qfuture.h>
#include <qregularexpression.h>


class Highlighter final : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    explicit Highlighter(
        QTextCharFormat default_format,
        QStringList const& keywords,
        QTextDocument* parent = nullptr);

protected:
    void highlightBlock(const QString& text) override;

private:
    struct HighlightingRule
    {
        QRegularExpression pattern;
        QTextCharFormat format;
    };

    QVector<HighlightingRule> high_lighting_rules_;

    QRegularExpression comment_start_expression_;
    QRegularExpression comment_end_expression_;

    QTextCharFormat class_format_;
    QTextCharFormat single_line_comment_format_;
    QTextCharFormat multi_line_comment_format_;
    QTextCharFormat quotation_format_;
    QTextCharFormat function_format_;
    QTextCharFormat keyword_format_;

};
