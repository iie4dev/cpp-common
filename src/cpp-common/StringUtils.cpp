﻿#include "StringUtils.h"

#include <vector>

#include <Windows.h>
#include <locale>
#include <codecvt>

#include "DefBase.h"

namespace np_limo
{
    namespace str
    {
#pragma region functions for converting between multi-bytes and wide-char
        bool ConvertMultiBytesToWideChar(std::wstring& w_str,
                                         char const* a_str, size_t const a_str_size,
                                         unsigned const codepage) {
            if (codepage == CP_UTF8) {
                std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
                w_str = converter.from_bytes(a_str, a_str + a_str_size);
                return true;
            }

            // fetch tgt str size
            auto len = MultiByteToWideChar(
                    codepage,
                    0,
                    a_str,       // src str
                    a_str_size,    // src str len
                    nullptr,    // tgt str, null for test how long is need for tgt str
                    0           // tgt str len, 0 for test how long is need for tgt str
            );


            if (0 == len) {
                return false;
            }

            // convert from src to tgt
            w_str.resize(len);
            len = MultiByteToWideChar(
                    codepage,
                    0,
                    a_str,
                    a_str_size,
                    const_cast<str_w_t>(w_str.c_str()),
                    w_str.size()
            );

            return 0 != len;
        }

        bool ConvertWideCharToMultiBytes(std::string& a_str,
                                         wchar_t const* w_str, size_t const w_str_size,
                                         unsigned const codepage) {
            if (codepage == CP_UTF8) {
                std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
                a_str = converter.to_bytes(w_str, w_str + w_str_size);
                return true;
            }

            // fetch tgt str size
            auto len = WideCharToMultiByte(
                    codepage,
                    0,
                    w_str,      // src str
                    w_str_size,  // src str len
                    nullptr,    // tgt str, null for test how long is need for tgt str
                    0,          // tgt str len, 0 for test how long is need for tgt str
                    nullptr,    // no need in test phase
                    nullptr     // no need in test phase
            );

            if (0 == len) {
                return false;
            }

            auto default_char = '\0';
            auto default_used = FALSE;

            // convert from src to tgt
            a_str.resize(len);
            len = WideCharToMultiByte(
                    codepage,
                    0,
                    w_str,
                    w_str_size,
                    const_cast<str_a_t>(a_str.c_str()),
                    a_str.size(),
                    &default_char,
                    &default_used
            );

            return 0 != len;
        }

        bool ConvertBetweenMultiBytes(std::string& to, char const* from, size_t const from_size,
                                      unsigned const to_codepage, unsigned const from_codepage) {
            std::wstring temp;
            if (ConvertMultiBytesToWideChar(temp, from, from_size, from_codepage)) {
                return ConvertWideCharToMultiBytes(to, temp.c_str(), temp.size(), to_codepage);
            }
            return false;
        }

        bool ConvertSysToUtf(std::string& to, char const* from, size_t from_size) {
            return ConvertBetweenMultiBytes(to, from, from_size, CP_UTF8, GetACP());
        }

        bool ConvertUtfToSys(std::string& to, char const* from, size_t const from_size) {
            return ConvertBetweenMultiBytes(to, from, from_size, GetACP(), CP_UTF8);
        }

        bool ConvertAppToUtf(std::string &to, char_t const*from, size_t from_size) {
#ifdef _UNICODE
            return ConvertWideCharToMultiBytes(to, from, from_size, CP_UTF8);
#else
            return ConvertBetweenMultiBytes(to, from, from_size, CP_UTF8, GetACP());
#endif
        }

        bool ConvertUtfToApp(string_t &to, const char *from, size_t from_size) {
#ifdef _UNICODE
            return ConvertMultiBytesToWideChar(to, from, from_size, CP_UTF8);
#else
            return ConvertBetweenMultiBytes(to, from, from_size, GetACP(), CP_UTF8);
#endif
        }

        bool ConvertSysToApp(string_t &to, const char *from, size_t from_size) {
#ifdef _UNICODE
            return ConvertMultiBytesToWideChar(to, from, from_size, GetACP());
#else
            to.assign(from, from_size);
            return true;
#endif
        }

        bool ConvertAppToSys(std::string &to, char_t const* from, size_t from_size) {
#ifdef _UNICODE
            return ConvertWideCharToMultiBytes(to, from, from_size, GetACP());
#else
            to.assign(from, from_size);
            return true;
#endif
        }


        bool ConvertWideCharToSys(std::string &to, const wchar_t *str, size_t str_len) {
            return ConvertWideCharToMultiBytes(to, str, str_len, GetACP());
        }

        bool ConvertWideCharToApp(string_t &to, const wchar_t *str, size_t str_len) {
#ifdef _UNICODE
            to.assign(str, str_len);
            return true;
#else
            return ConvertWideCharToMultiBytes(to, str, str_len, GetACP());
#endif
        }

        bool ConvertGbkToUtf(std::string& to, char const* from, size_t from_size) {
            return ConvertBetweenMultiBytes(to, from, from_size, CP_UTF8, CP_GBK);
        }

        bool ConvertUtfToGbk(std::string& to, char const* from, size_t from_size) {
            return ConvertBetweenMultiBytes(to, from, from_size, CP_GBK, CP_UTF8);
        }

        bool ConvertToUtf(std::string& to, void const* from, size_t const from_size,
                          EStringEncoding const encoding) {
            switch(encoding) {
                case UTF8:
                    to.assign(static_cast<const char *>(from));
                    return true;
                case GBK:
                    return ConvertGbkToUtf(to, static_cast<const char *>(from), from_size);
                case UNIC:
                    return ConvertWideCharToMultiBytes(to,
                                                       static_cast<const wchar_t *>(from),
                                                       from_size >> 1u,
                                                       CP_UTF8);
                default: ;
            }
            return false;
        }

        int find_first_char(CFSTR const str, char_t const c) {
            auto curr = str;
            while (*curr) {
                if (c == *curr)
                    return static_cast<int>(curr - str);
                ++curr;
            }
            return -1;
        }

        int find_last_char(CFSTR const str, char_t const c) {
            auto curr = str + SSTRLEN(str);
            do {
                --curr;
                if (c == *curr)
                    return static_cast<int>(curr - str);
            } while (str != curr);
            return -1;
        }

#pragma endregion
    }
}