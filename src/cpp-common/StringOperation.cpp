//
// Created by limo on 8/22/2018
//

#include <cpp-common/StringOperation.h>

#include <sstream>
#include <iomanip>

std::string dump_string(char const* str, size_t const len) {
    std::stringstream os;
    os << "bytes [size " << std::dec << std::setw(3)
        << std::setfill('0') << len << "] (";

    if (len >= 1000) {
        os << "... too big to print)";
    } else {
        auto prev_is_ascii = false;
        for (size_t i = 0; i < len; ++i) {
            auto const byte = str[i];
            auto const curr_is_ascii = (byte >= 33 && byte < 127);
            if (prev_is_ascii != curr_is_ascii)
                os << " "; // Separate text/non text

            if (curr_is_ascii) {
                os << byte;
            } else {
                os << std::hex << std::uppercase << std::setw(2)
                    << std::setfill('0') << static_cast<short>(byte);
            }
            prev_is_ascii = curr_is_ascii;
        }
        os << ")";
    }
    return os.str();
}

std::string dump_string(std::string const& str) {
    return dump_string(str.c_str(), str.size());
}