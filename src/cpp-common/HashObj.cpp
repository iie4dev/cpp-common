//
// Created by limo on 9/30/19.
//

#include <cpp-common/HashObj.h>

HashObj& HashObj::append_hash(std::istream &bytes) {
    return append_hash(std::istreambuf_iterator<char>(bytes),
            std::istreambuf_iterator<char>());
}

HashObj& HashObj::hash(std::istream &bytes) {
    clear();
    return append_hash(bytes);
}

HashObj::bytes_t HashObj::to_bytes() const {
    auto bytes = bytes_t(picosha2::k_digest_size, 0);
    hasher_.get_hash_bytes(bytes.begin(), bytes.end());
    return bytes;
}

std::string HashObj::to_hex_string() const {
    auto const bytes = to_bytes();
    return picosha2::bytes_to_hex_string(bytes.begin(), bytes.end());
}
