//
// Created by limo on 2018/8/11.
//

#include "cpp-common/NetworkTools.h"

#include <stdexcept>

#ifdef _WIN32
#include <WinSock2.h>
#else
#include <sys/socket.h>
#include <net/if.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/select.h>
#endif

#include <zmq.hpp>


bool get_free_port(unsigned short &port) {
#ifdef _WIN32
    auto result = true;
    WORD wVersionRequested;
    WSADATA wsaData;

    // initialize socket
    wVersionRequested = MAKEWORD(2, 2);
    if (0 != WSAStartup(wVersionRequested, &wsaData)) {
        return false;
    }

    // 1. create a socket
    auto const sock = socket(AF_INET, SOCK_STREAM, 0);

    // 2. create a sockaddr
    sockaddr_in addr{};
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(ADDR_ANY);
    addr.sin_port = 0;        // 0 for letting system generate an avaliable port

    // 3. bind 
    if (0 != bind(sock, reinterpret_cast<SOCKADDR*>(&addr), sizeof addr)) {
        result = false;
    } else {
        sockaddr_in connAddr{};
        int len = sizeof connAddr;
        if (0 != getsockname(sock, reinterpret_cast<SOCKADDR*>(&connAddr), &len)) {
            result = false;
        } else {
            port = ntohs(connAddr.sin_port); // get port
        }
    }

    if (0 != closesocket(sock))
        result = false;
    return result;
#else
#error In __FILE__ : __LINE__, need implement
#endif
}


bool dispatch_free_port(std::string& channel, int try_times) {
    auto const is_channel_usable = [](std::string const& channel) {
        try {
            zmq::context_t ctx(1);
            zmq::socket_t skt(ctx, ZMQ_PULL);
            skt.bind(channel);
        } catch (...) {
            return false;
        }
        return true;
    };

    auto const idx = channel.find_last_of(':') + 1;
    auto const local_channel_prefix = channel.substr(0, idx);
    while (!is_channel_usable(channel)) {
        if (--try_times < 0) {
            return false;
        }
        unsigned short port;
        try {
            if (get_free_port(port)) {
                channel = local_channel_prefix + std::to_string(port);
            }
        } catch (...) {
        }
    }

    return true;
}