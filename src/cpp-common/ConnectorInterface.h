//
// Created by limo on 2018/8/10.
//

#ifndef FILE_CHECK_CONNECTORINTERFACE_H
#define FILE_CHECK_CONNECTORINTERFACE_H


#include <string>
#include <future>

enum ConnectCode {
    SUCCESS,
    SEND_ERR,
    RECV_ERR,
};

class ConnectorInterface {
public:
    using bytes_stream_t = std::string;
    using reply_t = std::pair<ConnectCode, bytes_stream_t>;

	class Client {
	public:
		virtual ~Client() = default;
		virtual bool ConnectChannel(const std::string& channel) = 0;
        virtual bool Connected() const = 0;
        virtual void Disconnect() = 0;
		virtual std::future<reply_t> Request(bytes_stream_t const& req) = 0;

	protected:
        void UntokenReply(reply_t & reply, char const*token, size_t size) const {
            if (0 == size) return;

            reply.first = static_cast<ConnectCode>(*token);
            reply.second.resize(size-1);
            std::copy(token+1, token+size, reply.second.begin());
        }

	};

	class Server {
	public:
		virtual ~Server() = default;
		virtual std::string BuildChannel() = 0;
        virtual std::string GetChannel() = 0;
		virtual void ReplyLooper(
            std::function<bool(bytes_stream_t&, bytes_stream_t const&)> const&) = 0;

	protected:
	    void TokenReply(char *token, reply_t const& rep) const {
            *token = static_cast<char>(rep.first);
            memcpy(token + 1, rep.second.c_str(), rep.second.size());
	    }
	};
};


#endif