//
// Created by limo on 9/3/2018.
//

#ifndef FILE_CHECK_BUFF_H
#define FILE_CHECK_BUFF_H

#include <cstring>
#include <iostream>

#include <cpp-common-config.h>

class EXPORT_LIB Buff {
public:
	explicit Buff(size_t const buff_capacity) {
		reserve(buff_capacity);
	}

	Buff(Buff const& rhv) {
		*this = rhv;
	}

    Buff& operator=(Buff const& rhv) {
	    if (&rhv == this) return *this;
		if (rhv.get_size() > buff_capacity_) {
			reserve(rhv.get_size());
		}
		memcpy(buff_, rhv.buff_, rhv.get_size());
		return *this;
	}

    Buff(Buff&& rhv) {
		*this = std::move(rhv);
	}

    Buff& operator=(Buff&& rhv) {
		buff_ = rhv.buff_;
		buff_size_ = rhv.buff_size_;
		buff_capacity_ = rhv.buff_capacity_;
		rhv.buff_ = nullptr;
		rhv.reserve(0);
		return *this;
	}

    ~Buff() {
		reserve(0);
	}

    const char *get_buff() const {
		return buff_;
    }

    char *get_buff() {
		return buff_;
	}

    size_t get_size() const {
		return buff_size_;
    }

    size_t get_capacity() const {
		return buff_capacity_;
    }

    void resize(size_t size) {
	    if (size > buff_capacity_) {
			buff_size_ = buff_capacity_;
	    } else {
			buff_size_ = size;
	    }
	}

    bool append(char const* more_buff, size_t const more_size) {
        if (buff_size_ + more_size <= buff_capacity_) {
			memcpy(buff_ + buff_size_, more_buff, more_size);
			buff_size_ += more_size;
			return true;
        }
		return false;
    }

    void reserve(size_t const buff_capacity) {
		delete buff_;
		buff_ = nullptr;
        if (0 != buff_capacity)
		    buff_ = new char[buff_capacity];
		buff_capacity_ = buff_capacity;
	    buff_size_ = 0;
	}

	void clear() {
		buff_size_ = 0;
	}

private:
	char *buff_{};
	size_t buff_size_{};
	size_t buff_capacity_{};

};

#endif
