#pragma once
#include <functional>
#include <thread>
#include <condition_variable>

#include <Windows.h>
#include <future>

#include <cpp-common-config.h>

namespace np_limo {
	namespace thread {
		template <typename T>
		class EXPORT_LIB CancelableThread {
			bool canceled_;
			bool finished_;
			std::shared_ptr<std::thread> p_thread_;

		public:
			explicit CancelableThread(std::function<T()> const& package, T& result)
				: canceled_(false), finished_(false)
				, p_thread_(nullptr) {
				p_thread_ = std::make_shared<std::thread>([&]() {
					auto res = package();
					if (!canceled_)
						result = res;
					finished_ = true;
				});
			}

			~CancelableThread() {
				if (nullptr != p_thread_ && p_thread_->joinable())
					p_thread_->join();
			}

			void cancel() {
				canceled_ = true;
				p_thread_->detach();
				p_thread_ = nullptr;
			}

			bool finished() const {
				return finished_;
			}
		};

		inline
		bool wait_for(std::function<bool()> const& check,
			DWORD const interval,
			DWORD const timeout) {

			auto start_point = GetTickCount();
			while (!check()) {
				if (start_point > GetTickCount()) {
					// in case that GetTickCount clears to 0 after running 24 days
					start_point = GetTickCount();
				}
				if (start_point + timeout < GetTickCount()) {
					return false;
				}
				Sleep(interval);
			}
			return true;
		}

		template <typename T>
		inline bool do_async(std::function<T()> const& package, T& result, DWORD const timeout) {
			auto thd = CancelableThread<T>(package, result);
			auto const finished = wait_for([&thd]() {
				return thd.finished();
			}, 200, timeout);
			if (!finished) {
				thd.cancel();
			}
			return finished;
		}
	}
}
