#include "Image2TextConverter.h"

#include <cpp-common/ProcessWrapper.h>
#include <cpp-common/GlobalConfig.h>
#include <cpp-common/DefBase.h>

np_limo::tools::Image2TextConverter::Image2TextConverter()
    : in_assigned_(false)
    , out_assigned_(false)
    , h_in_image_(nullptr)
    , h_out_txt_(nullptr)
    , h_in_txt_(nullptr) {
}

np_limo::tools::Image2TextConverter::~Image2TextConverter() {
    clear_in();
    clear_out();
}

np_limo::tools::Image2TextConverter::Image2TextConverter(Image2TextConverter&& rhv)
    : in_assigned_(rhv.in_assigned_)
    , out_assigned_(rhv.out_assigned_)
    , h_in_image_(rhv.h_in_image_)
    , h_out_txt_(rhv.h_out_txt_)
    , h_in_txt_(rhv.h_in_txt_)
    , langs_(std::move(rhv.langs_)) {
    rhv.in_assigned_ = false;
    rhv.out_assigned_ = false;
    rhv.h_in_image_ = nullptr;
    rhv.h_out_txt_ = nullptr;
    rhv.h_in_txt_ = nullptr;
}

np_limo::tools::Image2TextConverter& 
np_limo::tools::Image2TextConverter::operator=(Image2TextConverter&& rhv) {
    in_assigned_ = rhv.in_assigned_;
    out_assigned_ = rhv.out_assigned_;
    h_in_image_ = rhv.h_in_image_;
    h_out_txt_ = rhv.h_out_txt_;
    h_in_txt_ = rhv.h_in_txt_;
    langs_ = std::move(rhv.langs_);

    rhv.in_assigned_ = false;
    rhv.out_assigned_ = false;
    rhv.h_in_image_ = nullptr;
    rhv.h_out_txt_ = nullptr;
    rhv.h_in_txt_ = nullptr;

    return *this;
}

bool np_limo::tools::Image2TextConverter::set_in(string_t const& image_file) {
    clear_in();

    SECURITY_ATTRIBUTES sa = {sizeof(SECURITY_ATTRIBUTES), nullptr, TRUE};
    h_in_image_ = ::CreateFile(
        image_file.c_str(),
        GENERIC_READ,
        FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
        &sa,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        nullptr);

    return in_assigned_ = (h_in_image_ && INVALID_HANDLE_VALUE != h_in_image_);
}

bool np_limo::tools::Image2TextConverter::set_out(string_t const& txt_file) {
    clear_out();

    SECURITY_ATTRIBUTES sa = {sizeof(SECURITY_ATTRIBUTES), nullptr, TRUE};
    h_out_txt_ = ::CreateFile(
        txt_file.c_str(),
        GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
        &sa,
        CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        nullptr);

    return out_assigned_ = (h_out_txt_ && INVALID_HANDLE_VALUE != h_out_txt_);
}

HANDLE np_limo::tools::Image2TextConverter::set_out() {
    clear_out();

    SECURITY_ATTRIBUTES sa;
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.lpSecurityDescriptor = nullptr;
    sa.bInheritHandle = TRUE;

    out_assigned_ = CreatePipe(&h_in_txt_, &h_out_txt_, &sa, 0) == TRUE;
    return h_in_txt_;
}

bool np_limo::tools::Image2TextConverter::convert() {
    if (in_assigned_ && out_assigned_) {
        ProcessWrapper tess_exe(GlobalConfig::Instance()->path_tess_exe());
        tess_exe.push_param(TEXT("stdin"));
        tess_exe.push_param(TEXT("stdout"));
        if (!langs_.empty()) {
            tess_exe.push_param(TEXT("-l"));
            tess_exe.push_param(lang_param());
        }
        tess_exe.push_param(TEXT("--tessdata-dir"));
        tess_exe.push_path(GlobalConfig::Instance()->path_tess_dir());
        tess_exe.startup(h_in_image_, h_out_txt_, nullptr);
        
        log::trace(kGlobalLogger, "Image2TextConverter: waiting for converting");
        auto ret = false;
        if (tess_exe.wait()) {
            ret = true;
        }

        CLEAR_HANDLE(h_out_txt_);
        return ret;
    }

    log::error(kGlobalLogger, "Image2TextConverter: image path or out-file path not set");
    return false;
}

bool np_limo::tools::Image2TextConverter::convert(
    string_t const& in_file, string_t const& out_file) const {
    // for version 3.1
    ProcessWrapper tess_exe(GlobalConfig::Instance()->path_tess_exe());
    tess_exe.push_path(in_file);
    tess_exe.push_path(out_file);
    if (!langs_.empty()) {
        tess_exe.push_param(TEXT("-l"));
        tess_exe.push_param(lang_param());
    }
    tess_exe.startup();

    log::trace(kGlobalLogger, "Image2TextConverter: waiting for converting");
    return tess_exe.wait();
}

void np_limo::tools::Image2TextConverter::clear_in() {
    CLEAR_HANDLE(h_in_image_);
    in_assigned_ = false;
}

void np_limo::tools::Image2TextConverter::clear_out() {
    CLEAR_HANDLE(h_out_txt_);
    CLEAR_HANDLE(h_in_txt_);
    out_assigned_ = false;
}

void np_limo::tools::Image2TextConverter::target_lang(string_t const& lang) {
    langs_.push_back(lang);
}

string_t np_limo::tools::Image2TextConverter::lang_param() const {
    if (langs_.empty()) return TEXT("");

    auto const len = langs_.size();
    auto param = langs_.front();
    for (size_t i = 1; i < len; ++i) {
        param.push_back(TEXT('+'));
        param.append(langs_[i]);
    }
    return param;
}
