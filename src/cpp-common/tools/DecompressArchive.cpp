#include "DecompressArchive.h"

#include <cpp-common/ProcessWrapper.h>
#include <cpp-common/logger.h>
#include <cpp-common/GlobalConfig.h>

static const unsigned BUF_SIZE = 1024;

// todo: move into a separated file
class BufPipe {
public:
    BufPipe()
            : writer_(nullptr)
            , reader_(nullptr)
            , initialized_(false) {
        init();
    }

    HANDLE get_write_handle() const {
        return writer_;
    }

    void close_write_handle() {
        ::CloseHandle(writer_);
        writer_ = nullptr;
    }

    std::string read() {
        // Read output from the child process's pipe for STDOUT
        // and write to the parent process's pipe for STDOUT.
        // Stop when there is no more data.
        auto buf = std::string();

        DWORD dwRead;
        CHAR chBuf[BUF_SIZE];
        for (;;)
        {
            auto const bSuccess = ReadFile(reader_, chBuf, BUF_SIZE, &dwRead, nullptr);
            if(!bSuccess || dwRead == 0) break;
            buf.append(chBuf, dwRead);
        }

        reader_ = nullptr;
        return buf;
    }

private:
    void init() {
        SECURITY_ATTRIBUTES saAttr;

        // Set the bInheritHandle flag so pipe handles are inherited.
        saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
        saAttr.bInheritHandle = TRUE;
        saAttr.lpSecurityDescriptor = nullptr;

        // Create a pipe for the child process's STDOUT.
        if (!CreatePipe(&reader_, &writer_, &saAttr, 0))
            // ErrorExit(TEXT("StdoutRd CreatePipe"));
            return;

        // Ensure the read handle to the pipe for STDOUT is not inherited.
        if (!SetHandleInformation(reader_, HANDLE_FLAG_INHERIT, 0))
//        ErrorExit(TEXT("Stdout SetHandleInformation"));
            return;

        initialized_ = true;
    }

    HANDLE reader_;
    HANDLE writer_;
    bool initialized_;

};


decompress_res_e decompress_archive(string_t const& archive_path, string_t const& decompress_to) {
    auto pipe = BufPipe();

    ProcessWrapper process(GlobalConfig::Instance()->path_7z_exe());
    process.push_param(TEXT("e"));
    process.push_path(archive_path);
    process.push_param(string_t(TEXT(R"(-o")")).append(decompress_to).append(TEXT(R"(")")));
    process.push_param(TEXT("-y"));
    process.push_param(TEXT("-p"));
    process.push_param(TEXT("-spf"));
    process.startup(nullptr, nullptr, pipe.get_write_handle());

    pipe.close_write_handle();

    if (process.wait()) {
        auto err = pipe.read();
        if (err.empty()) return SUCCESS;

        if (err.find("Wrong password?") != std::string::npos) {
            return NEED_PASSWORD;
        }
    }

    return CRASHED;
}
