#pragma once

#include <cpp-common/StringUtils.h>

enum decompress_res_e {
    SUCCESS,
    NEED_PASSWORD,
    CRASHED,
};

decompress_res_e decompress_archive(string_t const& archive_path, string_t const& decompress_to);
