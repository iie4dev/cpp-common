#pragma once

#include <vector>

#include <Windows.h>

#include <cpp-common-config.h>
#include <cpp-common/StringUtils.h>

namespace np_limo
{
    namespace tools
    {
        class EXPORT_LIB Image2TextConverter {
        public:
            Image2TextConverter();
            ~Image2TextConverter();

            Image2TextConverter(Image2TextConverter const& rhv) = default;
            Image2TextConverter& operator=(Image2TextConverter const& rhv) = default;
            Image2TextConverter(Image2TextConverter&& rhv);
            Image2TextConverter& operator=(Image2TextConverter&& rhv);

            bool set_in(string_t const& image_file);
            bool set_out(string_t const& txt_file);
            HANDLE set_out();

            bool convert();
            bool convert(string_t const& in_file, string_t const& out_file) const;

            void clear_in();
            void clear_out();

            void target_lang(string_t const& lang);

        private:
            string_t lang_param() const;

            bool in_assigned_;
            bool out_assigned_;

            HANDLE h_in_image_;
            HANDLE h_out_txt_;
            HANDLE h_in_txt_;

            std::vector<string_t> langs_;

        };
    }
}
