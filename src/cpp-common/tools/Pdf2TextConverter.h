#pragma once

#include <poppler/OutputDev.h>

#include <cpp-common/StringUtils.h>


class Pdf2TextConverter {
    using Self = Pdf2TextConverter;
public:
    bool convert(std::string const& pdf_file_utf8);

    Self &set_output_dev(std::shared_ptr<OutputDev> out) {
        output_dev_ = out;
        return *this;
    }

    Self &set_out_text_encoding(std::string const& encoding) {
        text_encoding_ = encoding;
        return *this;
    }

private:
    std::shared_ptr<OutputDev> output_dev_;
    std::string text_encoding_;

};
