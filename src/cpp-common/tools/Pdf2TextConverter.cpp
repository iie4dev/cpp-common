//
// Created by Administrator on 9/6/2020.
//

#include "Pdf2TextConverter.h"

#include <poppler/GlobalParams.h>
#include <poppler/PDFDocFactory.h>
#include <poppler/TextOutputDev.h>

#include <cpp-common/logger.h>


bool Pdf2TextConverter::convert(std::string const& pdf_file_utf8) {
    auto file_name = std::make_shared<GooString>(pdf_file_utf8.c_str());

    // init_queuer params for poppler
    auto config = std::make_shared<GlobalParams>();
    globalParams = config.get();
    globalParams->setTextEncoding(&text_encoding_[0]);
    globalParams->setTextPageBreaks(gTrue);
    globalParams->setErrQuiet(gTrue);

    // construct file name
    auto doc = std::shared_ptr<PDFDoc>(
            PDFDocFactory().createPDFDoc(
                    *file_name, nullptr, nullptr));
    if (! doc->isOk()) {
        return false;
    }

    try {
        auto const first_page = 1;
        auto const last_page = doc->getNumPages();
        // this function will do the real extract stuff and the call chain would be like
        // this: <displayPages> -> <text_out> -> <search_func> -> <search_in> -> <callback>
        doc->displayPages(output_dev_.get(), first_page, last_page,
                72.0,72.0, 0, gTrue, gFalse, gFalse);

    } catch (...) {
        np_limo::log::error(kGlobalLogger, "Something went wrong while converting pdf to text.");
        return false;
    }

    return true;
}