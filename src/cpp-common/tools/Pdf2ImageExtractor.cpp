#include "Pdf2ImageExtractor.h"

#include <memory>

#include <poppler/GlobalParams.h>
#include <poppler/PDFDocFactory.h>
#include <poppler/TextOutputDev.h>


bool Pdf2ImageExtractor::extract(const string_t &pdf_file) {
    auto file_path_sys = std::string();
    if (!np_limo::str::ConvertAppToSys(file_path_sys, pdf_file.c_str(), pdf_file.size())) {
//        np_limo::log::error(kGlobalLogger, "Failed encoding file path into utf.");
        return false;
    }

    auto file_name = std::make_shared<GooString>(file_path_sys.c_str());

    auto config = std::make_shared<GlobalParams>();
    globalParams = config.get();
    globalParams->setErrQuiet(quiet_);

    std::shared_ptr<GooString> owner_password = nullptr;
    std::shared_ptr<GooString> user_password = nullptr;
    if (! owner_password_.empty()) {
        owner_password = std::make_shared<GooString>(owner_password_.c_str());
    }
    if (! user_password_.empty()) {
        user_password = std::make_shared<GooString>(user_password_.c_str());
    }

    auto doc = std::shared_ptr<PDFDoc>(
            PDFDocFactory().createPDFDoc(
                    *file_name, owner_password.get(), user_password.get()));
    if (! doc->isOk()) {
        return false;
    }

    auto const first_page = 1;
    auto const last_page = doc->getNumPages();
    doc->displayPages(out_dev_.get(), first_page, last_page,
            72, 72, 0, gTrue, gFalse, gFalse);

    return true;
}
