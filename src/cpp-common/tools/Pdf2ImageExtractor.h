#pragma once

#include <memory>
#include <poppler/OutputDev.h>

#include <cpp-common/StringUtils.h>


class Pdf2ImageExtractor {
    using Self = Pdf2ImageExtractor;
public:
    Pdf2ImageExtractor() : quiet_(true) {
    }

    bool extract(string_t const& pdf_file);

    Self &setOutDev(std::shared_ptr<OutputDev> const& out_dev) {
        out_dev_ = out_dev;
        return *this;
    }

    Self &set_owner_password(std::string const& password) {
        owner_password_ = password;
        return *this;
    }

    Self &set_user_password(std::string const& password) {
        user_password_ = password;
        return *this;
    }

    Self &enable_quiet(bool const quiet) {
        quiet_ = quiet;
        return *this;
    }

private:
    std::shared_ptr<OutputDev> out_dev_;

    std::string owner_password_;
    std::string user_password_;
    bool quiet_;

};
