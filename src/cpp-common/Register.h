//
// Created by limo on 2018/7/29.
//

#ifndef FILE_CHECK_REGISTER_H
#define FILE_CHECK_REGISTER_H


#include <map>

template <typename K, typename V>
class Register {
public:
    bool RegisterKeyValue(const K& k, const V& v) {
        if (m_register_map.find(k) != m_register_map.end()) return false;
        m_register_map[k] = v;
        return true;
    }

    bool UnregisterKey(const K& k) {
        return m_register_map.erase(k) != 0;
    }

    const V& GetValueByKey(const K& k) const {
        return m_register_map.at(k);
    }

    V& operator[](const K& k) {
        return m_register_map[k];
    }

protected:
    std::map<K, V> m_register_map;

};

#endif //FILE_CHECK_REGISTER_H
