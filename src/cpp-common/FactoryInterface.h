//
// Created by limo on 2018/7/29.
//

#ifndef FILE_CHECK_FACTORYINTERFACE_H
#define FILE_CHECK_FACTORYINTERFACE_H

#include <memory>
#include <vector>
#include <functional>

#include "cpp-common/IdGenerator.h"

template <typename B>
class FactoryInterface : public IdGenerator<B> {
public:
    using deleter_t = const std::function<void(B*)>;

    virtual std::shared_ptr<B> CreateObjByType(const typename B::EType&) const = 0;
    virtual std::vector<std::shared_ptr<B>> CreateObjsByType(const typename B::EType&, size_t num) const = 0;

    virtual std::shared_ptr<B> CreateObjByType(const typename B::EType&, const deleter_t &deleter) const = 0;
    virtual std::vector<std::shared_ptr<B>> CreateObjsByType(const typename B::EType&, size_t num, const deleter_t &deleter) const = 0;
};

#endif //FILE_CHECK_FACTORYINTERFACE_H
