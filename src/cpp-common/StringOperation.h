//
// Created by limo on 8/22/2018
//

#ifndef FILE_CHECK_STRINGOPERATION_H
#define FILE_CHECK_STRINGOPERATION_H

#include <string>

#include <cpp-common-config.h>

EXPORT_LIB std::string dump_string(char const* str, size_t const len);
EXPORT_LIB std::string dump_string(std::string const& str);

#endif