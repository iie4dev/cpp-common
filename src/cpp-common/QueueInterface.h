//
// Created by limo on 2018/8/9.
//

#ifndef FILE_CHECK_QUEUEINTERFACE_H
#define FILE_CHECK_QUEUEINTERFACE_H

#include <memory>

template <typename T>
class QueueInterface {
public:
	virtual ~QueueInterface() {}
	virtual void enque(T data) = 0;
	virtual void enque(std::shared_ptr<T> p_data) = 0;
	virtual std::shared_ptr<T> deque() = 0;
	virtual bool deque(T& data) = 0;
	virtual bool empty() const = 0;

};

#endif