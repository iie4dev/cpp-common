//
// Created by limo on 2018/8/8.
//

#ifndef FILE_CHECK_STACKINTERFACE_H
#define FILE_CHECK_STACKINTERFACE_H

#include <memory>

template <typename T>
class StackInterface {
public:
    virtual ~StackInterface() = default;
    virtual void Push(T data) = 0;
    virtual void Push(std::shared_ptr<T> p_data) = 0;
	virtual std::shared_ptr<T> Pop() = 0;
	virtual bool Pop(T& data) = 0;
    virtual bool Empty() const = 0;

};

#endif