#include "cpp-common/AutoBindZmqSocket.h"
#include <cpp-common/NetworkTools.h>

std::string const kLocalTcpAddrPrefix = "tcp://127.0.0.1:";

unsigned auto_bind_locally(zmq::socket_t &socket) {
    auto try_runs = 100;
    unsigned short free_port = 0;
    while (--try_runs > 0) {
        if (GetAvaliadblePort(free_port)) {
            auto const channel = std::string(kLocalTcpAddrPrefix).append(std::to_string(free_port));
            try {
                socket.bind(channel);
            } catch(...) {
                continue;
            }
            return free_port;
        }
    }
    return 0;
}
