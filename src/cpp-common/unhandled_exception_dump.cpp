#include <Windows.h>
#include <DbgHelp.h>
#include <ShlObj.h>

#include <cpp-common/FileDir.h>

#pragma comment(lib, "dbghelp.lib")


long __stdcall pretend_handle(_EXCEPTION_POINTERS* exception) {
	MINIDUMP_EXCEPTION_INFORMATION exp_param;

	np_limo::file::dir::TempFile tf(false, TEXT("FTD"), TEXT("DMP"));
	auto const file_name = tf.create_temp_file();

	auto* const h_dump_file = CreateFile(
		file_name.c_str(),
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_WRITE | FILE_SHARE_READ,
		nullptr, CREATE_ALWAYS, 0, nullptr);

	exp_param.ThreadId = GetCurrentThreadId();
	exp_param.ExceptionPointers = exception;
	exp_param.ClientPointers = TRUE;

	auto const mini_dump_with_data_segs = static_cast<MINIDUMP_TYPE>(MiniDumpNormal
		| MiniDumpWithHandleData
		| MiniDumpWithUnloadedModules
		| MiniDumpWithIndirectlyReferencedMemory
		| MiniDumpScanMemory
		| MiniDumpWithProcessThreadData
		| MiniDumpWithThreadInfo);

	MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(),
		h_dump_file, mini_dump_with_data_segs, &exp_param, nullptr, nullptr);
	return EXCEPTION_EXECUTE_HANDLER;
}


void register_dump_guard()
{
	SetUnhandledExceptionFilter(pretend_handle);
}
